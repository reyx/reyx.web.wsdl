// service

DataLayerService = {
    url: "http://panasonic.restauranteweb.com.br/servicoapps/datalayerservice.svc",
    reference: "IDataLayerService"
};

// methods

DataLayerService.AddAsFavorite = function (codigoRestaurante, codigoFilial, cep, securityData, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="codigoRestaurante" type="short"></param>
    ///   <param name="codigoFilial" type="short"></param>
    ///   <param name="cep" type="string"></param>
    ///   <param name="securityData" type="SecurityModel"></param>
    ///   <returns type="RestaurantModel" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("codigoRestaurante", codigoRestaurante);
    parameters.add("codigoFilial", codigoFilial);
    parameters.add("cep", cep);
    parameters.add("securityData", securityData);

    return SOAPClient.invoke(this.url, "AddAsFavorite", parameters, true, this.reference, callback);
};

DataLayerService.AddMenuItem = function (addMenuItemModel, securityData, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="addMenuItemModel" type="AddMenuItemModel"></param>
    ///   <param name="securityData" type="SecurityModel"></param>
    ///   <returns type="ErrorModel" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("addMenuItemModel", addMenuItemModel);
    parameters.add("securityData", securityData);

    return SOAPClient.invoke(this.url, "AddMenuItem", parameters, true, this.reference, callback);
};

DataLayerService.ChangeAccountPassword = function (newPassword, confirmNewPassword, reminder, securityData, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="newPassword" type="string"></param>
    ///   <param name="confirmNewPassword" type="string"></param>
    ///   <param name="reminder" type="string"></param>
    ///   <param name="securityData" type="SecurityModel"></param>
    ///   <returns type="MyAccountModel" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("newPassword", newPassword);
    parameters.add("confirmNewPassword", confirmNewPassword);
    parameters.add("reminder", reminder);
    parameters.add("securityData", securityData);

    return SOAPClient.invoke(this.url, "ChangeAccountPassword", parameters, true, this.reference, callback);
};

DataLayerService.EraseAccountAddress = function (cep, securityData, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="cep" type="string"></param>
    ///   <param name="securityData" type="SecurityModel"></param>
    ///   <returns type="MyAccountModel" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("cep", cep);
    parameters.add("securityData", securityData);

    return SOAPClient.invoke(this.url, "EraseAccountAddress", parameters, true, this.reference, callback);
};

DataLayerService.EraseOrderItem = function (orderItemIndex, restaurantData, securityData, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="orderItemIndex" type="int"></param>
    ///   <param name="restaurantData" type="RestaurantMenuSearchModel"></param>
    ///   <param name="securityData" type="SecurityModel"></param>
    ///   <returns type="UserBasicAndOrderModel" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("orderItemIndex", orderItemIndex);
    parameters.add("restaurantData", restaurantData);
    parameters.add("securityData", securityData);

    return SOAPClient.invoke(this.url, "EraseOrderItem", parameters, true, this.reference, callback);
};

DataLayerService.GetApplicationVersion = function (securityData, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="securityData" type="SecurityModel"></param>
    ///   <returns type="GetApplicationVersionResult" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("securityData", securityData);

    return SOAPClient.invoke(this.url, "GetApplicationVersion", parameters, true, this.reference, callback);
};

DataLayerService.GetHomeData = function (cep, securityData, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="cep" type="string"></param>
    ///   <param name="securityData" type="SecurityModel"></param>
    ///   <returns type="HomeModel" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("cep", cep);
    parameters.add("securityData", securityData);

    return SOAPClient.invoke(this.url, "GetHomeData", parameters, true, this.reference, callback);
};

DataLayerService.GetMyAccountData = function (securityData, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="securityData" type="SecurityModel"></param>
    ///   <returns type="MyAccountModel" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("securityData", securityData);

    return SOAPClient.invoke(this.url, "GetMyAccountData", parameters, true, this.reference, callback);
};

DataLayerService.GetNewAccountCEPData = function (newCEP, securityData, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="newCEP" type="string"></param>
    ///   <param name="securityData" type="SecurityModel"></param>
    ///   <returns type="MyAccountModel" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("newCEP", newCEP);
    parameters.add("securityData", securityData);

    return SOAPClient.invoke(this.url, "GetNewAccountCEPData", parameters, true, this.reference, callback);
};

DataLayerService.GetOrderConfirmationData = function (orderNumber, securityData, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="orderNumber" type="int"></param>
    ///   <param name="securityData" type="SecurityModel"></param>
    ///   <returns type="OrderCorfirmationModel" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("orderNumber", orderNumber);
    parameters.add("securityData", securityData);

    return SOAPClient.invoke(this.url, "GetOrderConfirmationData", parameters, true, this.reference, callback);
};

DataLayerService.GetOrderEndData = function (orderEndModel, securityData, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="orderEndModel" type="RestaurantMenuSearchModel"></param>
    ///   <param name="securityData" type="SecurityModel"></param>
    ///   <returns type="OrderEndModel" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("orderEndModel", orderEndModel);
    parameters.add("securityData", securityData);

    return SOAPClient.invoke(this.url, "GetOrderEndData", parameters, true, this.reference, callback);
};

DataLayerService.GetOrderEndNewAddress = function (newCEP, orderEndModel, securityData, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="newCEP" type="string"></param>
    ///   <param name="orderEndModel" type="RestaurantMenuSearchModel"></param>
    ///   <param name="securityData" type="SecurityModel"></param>
    ///   <returns type="OrderEndModel" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("newCEP", newCEP);
    parameters.add("orderEndModel", orderEndModel);
    parameters.add("securityData", securityData);

    return SOAPClient.invoke(this.url, "GetOrderEndNewAddress", parameters, true, this.reference, callback);
};

DataLayerService.GetOrderOnlineState = function (securityData, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="securityData" type="SecurityModel"></param>
    ///   <returns type="GetOrderOnlineStateResult" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("securityData", securityData);

    return SOAPClient.invoke(this.url, "GetOrderOnlineState", parameters, true, this.reference, callback);
};

DataLayerService.GetPushCampaign = function (idCampaign, securityData, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="idCampaign" type="int"></param>
    ///   <param name="securityData" type="SecurityModel"></param>
    ///   <returns type="PushCampaignModel" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("idCampaign", idCampaign);
    parameters.add("securityData", securityData);

    return SOAPClient.invoke(this.url, "GetPushCampaign", parameters, true, this.reference, callback);
};

DataLayerService.GetRegisterUserData = function (securityData, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="securityData" type="SecurityModel"></param>
    ///   <returns type="RegisterUserModel" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("securityData", securityData);

    return SOAPClient.invoke(this.url, "GetRegisterUserData", parameters, true, this.reference, callback);
};

DataLayerService.GetRestaurantData = function (codigoRestaurante, codigoFilial, cep, securityData, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="codigoRestaurante" type="short"></param>
    ///   <param name="codigoFilial" type="short"></param>
    ///   <param name="cep" type="string"></param>
    ///   <param name="securityData" type="SecurityModel"></param>
    ///   <returns type="RestaurantModel" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("codigoRestaurante", codigoRestaurante);
    parameters.add("codigoFilial", codigoFilial);
    parameters.add("cep", cep);
    parameters.add("securityData", securityData);

    return SOAPClient.invoke(this.url, "GetRestaurantData", parameters, true, this.reference, callback);
};

DataLayerService.GetRestaurantMenuData = function (searchModel, securityData, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="searchModel" type="RestaurantMenuSearchModel"></param>
    ///   <param name="securityData" type="SecurityModel"></param>
    ///   <returns type="RestaurantMenuModel" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("searchModel", searchModel);
    parameters.add("securityData", securityData);

    return SOAPClient.invoke(this.url, "GetRestaurantMenuData", parameters, true, this.reference, callback);
};

DataLayerService.GetRestaurantMenuDetailData = function (searchModel, securityData, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="searchModel" type="MenuDetailSearchModel"></param>
    ///   <param name="securityData" type="SecurityModel"></param>
    ///   <returns type="MenuDetailModel" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("searchModel", searchModel);
    parameters.add("securityData", securityData);

    return SOAPClient.invoke(this.url, "GetRestaurantMenuDetailData", parameters, true, this.reference, callback);
};

DataLayerService.GetSERPChangeCEPData = function (newCep, securityData, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="newCep" type="string"></param>
    ///   <param name="securityData" type="SecurityModel"></param>
    ///   <returns type="SERPModel" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("newCep", newCep);
    parameters.add("securityData", securityData);

    return SOAPClient.invoke(this.url, "GetSERPChangeCEPData", parameters, true, this.reference, callback);
};

DataLayerService.GetSERPChangeFiltersData = function (IdTipoCozinha, searchModel, securityData, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="IdTipoCozinha" type="int"></param>
    ///   <param name="searchModel" type="SERPSearchModel"></param>
    ///   <param name="securityData" type="SecurityModel"></param>
    ///   <returns type="SERPModel" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("IdTipoCozinha", IdTipoCozinha);
    parameters.add("searchModel", searchModel);
    parameters.add("securityData", securityData);

    return SOAPClient.invoke(this.url, "GetSERPChangeFiltersData", parameters, true, this.reference, callback);
};

DataLayerService.GetSERPData = function (searchModel, securityData, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="searchModel" type="HomeModelSearch"></param>
    ///   <param name="securityData" type="SecurityModel"></param>
    ///   <returns type="SERPModel" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("searchModel", searchModel);
    parameters.add("securityData", securityData);

    return SOAPClient.invoke(this.url, "GetSERPData", parameters, true, this.reference, callback);
};

DataLayerService.GetUserBasicAndOrder = function (securityData, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="securityData" type="SecurityModel"></param>
    ///   <returns type="UserBasicAndOrderModel" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("securityData", securityData);

    return SOAPClient.invoke(this.url, "GetUserBasicAndOrder", parameters, true, this.reference, callback);
};

DataLayerService.LogIn = function (email, password, securityData, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="email" type="string"></param>
    ///   <param name="password" type="string"></param>
    ///   <param name="securityData" type="SecurityModel"></param>
    ///   <returns type="LogInModel" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("email", email);
    parameters.add("password", password);
    parameters.add("securityData", securityData);

    return SOAPClient.invoke(this.url, "LogIn", parameters, true, this.reference, callback);
};

DataLayerService.LogOut = function (securityData, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="securityData" type="SecurityModel"></param>
    ///   <returns type="WebSession" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("securityData", securityData);

    return SOAPClient.invoke(this.url, "LogOut", parameters, true, this.reference, callback);
};

DataLayerService.OrderEndChangeAddress = function (orderEndModel, securityData, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="orderEndModel" type="RestaurantMenuSearchModel"></param>
    ///   <param name="securityData" type="SecurityModel"></param>
    ///   <returns type="OrderEndModel" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("orderEndModel", orderEndModel);
    parameters.add("securityData", securityData);

    return SOAPClient.invoke(this.url, "OrderEndChangeAddress", parameters, true, this.reference, callback);
};

DataLayerService.OrderEndChangeDate = function (orderEndModel, addressType, addressOperation, searchDate, securityData, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="orderEndModel" type="RestaurantMenuSearchModel"></param>
    ///   <param name="addressType" type="string"></param>
    ///   <param name="addressOperation" type="string"></param>
    ///   <param name="searchDate" type="string"></param>
    ///   <param name="securityData" type="SecurityModel"></param>
    ///   <returns type="OrderEndModel" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("orderEndModel", orderEndModel);
    parameters.add("addressType", addressType);
    parameters.add("addressOperation", addressOperation);
    parameters.add("searchDate", searchDate);
    parameters.add("securityData", securityData);

    return SOAPClient.invoke(this.url, "OrderEndChangeDate", parameters, true, this.reference, callback);
};

DataLayerService.OrderEndSaveAddress = function (restaurantData, addressData, securityData, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="restaurantData" type="RestaurantMenuSearchModel"></param>
    ///   <param name="addressData" type="FinalizaPedidoRestauranteModelDadosEntregaSelecaoEndereco"></param>
    ///   <param name="securityData" type="SecurityModel"></param>
    ///   <returns type="OrderEndModel" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("restaurantData", restaurantData);
    parameters.add("addressData", addressData);
    parameters.add("securityData", securityData);

    return SOAPClient.invoke(this.url, "OrderEndSaveAddress", parameters, true, this.reference, callback);
};

DataLayerService.OrderEndValidateCoupon = function (restaurantData, voucher, orderSubtotal, securityData, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="restaurantData" type="RestaurantMenuSearchModel"></param>
    ///   <param name="voucher" type="string"></param>
    ///   <param name="orderSubtotal" type="string"></param>
    ///   <param name="securityData" type="SecurityModel"></param>
    ///   <returns type="OrderEndModel" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("restaurantData", restaurantData);
    parameters.add("voucher", voucher);
    parameters.add("orderSubtotal", orderSubtotal);
    parameters.add("securityData", securityData);

    return SOAPClient.invoke(this.url, "OrderEndValidateCoupon", parameters, true, this.reference, callback);
};

DataLayerService.RecoverPassword = function (email, securityData, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="email" type="string"></param>
    ///   <param name="securityData" type="SecurityModel"></param>
    ///   <returns type="RecoverPasswordModel" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("email", email);
    parameters.add("securityData", securityData);

    return SOAPClient.invoke(this.url, "RecoverPassword", parameters, true, this.reference, callback);
};

DataLayerService.RegisterUser = function (user, securityData, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="user" type="UserModel"></param>
    ///   <param name="securityData" type="SecurityModel"></param>
    ///   <returns type="RegisterUserModel" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("user", user);
    parameters.add("securityData", securityData);

    return SOAPClient.invoke(this.url, "RegisterUser", parameters, true, this.reference, callback);
};

DataLayerService.RemoveAsFavorite = function (codigoRestaurante, codigoFilial, cep, securityData, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="codigoRestaurante" type="short"></param>
    ///   <param name="codigoFilial" type="short"></param>
    ///   <param name="cep" type="string"></param>
    ///   <param name="securityData" type="SecurityModel"></param>
    ///   <returns type="RestaurantModel" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("codigoRestaurante", codigoRestaurante);
    parameters.add("codigoFilial", codigoFilial);
    parameters.add("cep", cep);
    parameters.add("securityData", securityData);

    return SOAPClient.invoke(this.url, "RemoveAsFavorite", parameters, true, this.reference, callback);
};

DataLayerService.Reorder = function (orderNumber, securityData, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="orderNumber" type="int"></param>
    ///   <param name="securityData" type="SecurityModel"></param>
    ///   <returns type="ReorderModel" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("orderNumber", orderNumber);
    parameters.add("securityData", securityData);

    return SOAPClient.invoke(this.url, "Reorder", parameters, true, this.reference, callback);
};

DataLayerService.RequestEmailChange = function (oldEmail, newEmail, confirmNewEmail, securityData, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="oldEmail" type="string"></param>
    ///   <param name="newEmail" type="string"></param>
    ///   <param name="confirmNewEmail" type="string"></param>
    ///   <param name="securityData" type="SecurityModel"></param>
    ///   <returns type="MyAccountModel" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("oldEmail", oldEmail);
    parameters.add("newEmail", newEmail);
    parameters.add("confirmNewEmail", confirmNewEmail);
    parameters.add("securityData", securityData);

    return SOAPClient.invoke(this.url, "RequestEmailChange", parameters, true, this.reference, callback);
};

DataLayerService.SaveAccountAddress = function (address, isNewAddress, securityData, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="address" type="MyAccountSaveAddressModel"></param>
    ///   <param name="isNewAddress" type="boolean"></param>
    ///   <param name="securityData" type="SecurityModel"></param>
    ///   <returns type="MyAccountModel" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("address", address);
    parameters.add("isNewAddress", isNewAddress);
    parameters.add("securityData", securityData);

    return SOAPClient.invoke(this.url, "SaveAccountAddress", parameters, true, this.reference, callback);
};

DataLayerService.SaveDeviceToken = function (appInternalId, token, deviceType, deviceName, emailUsuario, securityData, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="appInternalId" type="string"></param>
    ///   <param name="token" type="string"></param>
    ///   <param name="deviceType" type="string"></param>
    ///   <param name="deviceName" type="string"></param>
    ///   <param name="emailUsuario" type="string"></param>
    ///   <param name="securityData" type="SecurityModel"></param>
    ///   <returns type="OperationResponseModel" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("appInternalId", appInternalId);
    parameters.add("token", token);
    parameters.add("deviceType", deviceType);
    parameters.add("deviceName", deviceName);
    parameters.add("emailUsuario", emailUsuario);
    parameters.add("securityData", securityData);

    return SOAPClient.invoke(this.url, "SaveDeviceToken", parameters, true, this.reference, callback);
};

DataLayerService.SendCampaignPush = function (certified, certifiedPassword, pushText, badge, emitsSound, customItemKey, customItemValue, tokens, securityData, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="certified" type="base64Binary"></param>
    ///   <param name="certifiedPassword" type="string"></param>
    ///   <param name="pushText" type="string"></param>
    ///   <param name="badge" type="int"></param>
    ///   <param name="emitsSound" type="boolean"></param>
    ///   <param name="customItemKey" type="string"></param>
    ///   <param name="customItemValue" type="string"></param>
    ///   <param name="tokens" type="ArrayOfstring"></param>
    ///   <param name="securityData" type="SecurityModel"></param>
    ///   <returns type="boolean" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("certified", certified);
    parameters.add("certifiedPassword", certifiedPassword);
    parameters.add("pushText", pushText);
    parameters.add("badge", badge);
    parameters.add("emitsSound", emitsSound);
    parameters.add("customItemKey", customItemKey);
    parameters.add("customItemValue", customItemValue);
    parameters.add("tokens", tokens);
    parameters.add("securityData", securityData);

    return SOAPClient.invoke(this.url, "SendCampaignPush", parameters, true, this.reference, callback);
};

DataLayerService.SendContactUsMessage = function (contactUs, securityData, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="contactUs" type="ContactUs"></param>
    ///   <param name="securityData" type="SecurityModel"></param>
    ///   <returns type="ContactUsModel" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("contactUs", contactUs);
    parameters.add("securityData", securityData);

    return SOAPClient.invoke(this.url, "SendContactUsMessage", parameters, true, this.reference, callback);
};

DataLayerService.SendOrder = function (restaurantData, orderEndData, deviceName, securityData, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="restaurantData" type="RestaurantMenuSearchModel"></param>
    ///   <param name="orderEndData" type="OrderEndData"></param>
    ///   <param name="deviceName" type="string"></param>
    ///   <param name="securityData" type="SecurityModel"></param>
    ///   <returns type="OrderEndModel" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("restaurantData", restaurantData);
    parameters.add("orderEndData", orderEndData);
    parameters.add("deviceName", deviceName);
    parameters.add("securityData", securityData);

    return SOAPClient.invoke(this.url, "SendOrder", parameters, true, this.reference, callback);
};

DataLayerService.SendOrderPush = function (orderNumber, pushText, userName, password, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="orderNumber" type="int"></param>
    ///   <param name="pushText" type="string"></param>
    ///   <param name="userName" type="string"></param>
    ///   <param name="password" type="string"></param>
    ///   <returns type="boolean" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("orderNumber", orderNumber);
    parameters.add("pushText", pushText);
    parameters.add("userName", userName);
    parameters.add("password", password);

    return SOAPClient.invoke(this.url, "SendOrderPush", parameters, true, this.reference, callback);
};

DataLayerService.SetAddressAsFavorite = function (cep, securityData, callback) {
    /// <signature>
    ///   <summary></summary>
    ///   <param name="cep" type="string"></param>
    ///   <param name="securityData" type="SecurityModel"></param>
    ///   <returns type="MyAccountModel" />
    /// </signature>

    var parameters = new SOAPClientParameters();
    parameters.add("cep", cep);
    parameters.add("securityData", securityData);

    return SOAPClient.invoke(this.url, "SetAddressAsFavorite", parameters, true, this.reference, callback);
};

// functions

AddMenuItemModel = function (DadosForm, DadosUrl) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="DadosForm" type="AdicionarPratoModelFormData"></param>    
    ///   <param name="DadosUrl" type="MenuDetailSearchModel"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.DadosForm = DadosForm || null;
    self.DadosUrl = DadosUrl || null;
};

AdicionarPratoModelFormData = function (CodigoPizzaMetade1, CodigoPizzaMetade2, Observacao, Quantidade, SelectedValues) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="CodigoPizzaMetade1" type="string"></param>    
    ///   <param name="CodigoPizzaMetade2" type="string"></param>    
    ///   <param name="Observacao" type="string"></param>    
    ///   <param name="Quantidade" type="int"></param>    
    ///   <param name="SelectedValues" type="ArrayOfAdicionarPratoModelFormDataSelectValue"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.CodigoPizzaMetade1 = CodigoPizzaMetade1 || null;
    self.CodigoPizzaMetade2 = CodigoPizzaMetade2 || null;
    self.Observacao = Observacao || null;
    self.Quantidade = Quantidade || null;
    self.SelectedValues = SelectedValues || null;
};

AdicionarPratoModelFormDataSelectValue = function (NomeInput, ValorInput) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="NomeInput" type="string"></param>    
    ///   <param name="ValorInput" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.NomeInput = NomeInput || null;
    self.ValorInput = ValorInput || null;
};

ArrayOfAdicionarPratoModelFormDataSelectValue = function () {
    /// <signature>
    ///   <summary></summary>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
};

ArrayOfCardapioRestauranteModelPrato = function () {
    /// <signature>
    ///   <summary></summary>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
};

ArrayOfDadosUsuarioEndereco = function () {
    /// <signature>
    ///   <summary></summary>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
};

ArrayOfEnderecoSelecao = function () {
    /// <signature>
    ///   <summary></summary>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
};

ArrayOfFinalizaPedidoRestauranteModelDadosEntregaSelecaoEndereco = function () {
    /// <signature>
    ///   <summary></summary>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
};

ArrayOfFinalizaPedidoRestauranteModelDadosPagamentoData = function () {
    /// <signature>
    ///   <summary></summary>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
};

ArrayOfFinalizaPedidoRestauranteModelPrato = function () {
    /// <signature>
    ///   <summary></summary>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
};

ArrayOfGrupoPrato = function () {
    /// <signature>
    ///   <summary></summary>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
};

ArrayOfHomeModelPedidosRefazerItem = function () {
    /// <signature>
    ///   <summary></summary>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
};

ArrayOfHomeModelPedidosRefazerItemOpcao = function () {
    /// <signature>
    ///   <summary></summary>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
};

ArrayOfHomeModelRestaurantes = function () {
    /// <signature>
    ///   <summary></summary>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
};

ArrayOfHorario = function () {
    /// <signature>
    ///   <summary></summary>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
};

ArrayOfHorarioAgendamento = function () {
    /// <signature>
    ///   <summary></summary>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
};

ArrayOfLogInModelEndereco = function () {
    /// <signature>
    ///   <summary></summary>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
};

ArrayOfMeusPedidosPedido = function () {
    /// <signature>
    ///   <summary></summary>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
};

ArrayOfMeusPedidosPedidoItem = function () {
    /// <signature>
    ///   <summary></summary>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
};

ArrayOfMinhaContaModelDataCheck = function () {
    /// <signature>
    ///   <summary></summary>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
};

ArrayOfMinhaContaModelDataEndereco = function () {
    /// <signature>
    ///   <summary></summary>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
};

ArrayOfMinhaContaModelDataSelect = function () {
    /// <signature>
    ///   <summary></summary>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
};

ArrayOfOpcaoAssociada = function () {
    /// <signature>
    ///   <summary></summary>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
};

ArrayOfOpcaoItemPreco = function () {
    /// <signature>
    ///   <summary></summary>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
};

ArrayOfOrderConfirmationMessage = function () {
    /// <signature>
    ///   <summary></summary>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
};

ArrayOfOrderItemModel = function () {
    /// <signature>
    ///   <summary></summary>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
};

ArrayOfPratoOpcoes = function () {
    /// <signature>
    ///   <summary></summary>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
};

ArrayOfPratoOpcoesItem = function () {
    /// <signature>
    ///   <summary></summary>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
};

ArrayOfPratoOpcoesPizza = function () {
    /// <signature>
    ///   <summary></summary>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
};

ArrayOfRestaurantGrupo = function () {
    /// <signature>
    ///   <summary></summary>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
};

ArrayOfRestaurantPrato = function () {
    /// <signature>
    ///   <summary></summary>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
};

ArrayOfRestaurantPratoOpcoes = function () {
    /// <signature>
    ///   <summary></summary>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
};

ArrayOfRestaurantPratoOpcoesItem = function () {
    /// <signature>
    ///   <summary></summary>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
};

ArrayOfRestaurantPratoOpcoesPizza = function () {
    /// <signature>
    ///   <summary></summary>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
};

ArrayOfSERPModelDadosRestaurante = function () {
    /// <signature>
    ///   <summary></summary>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
};

ArrayOfSERPModelDadosRestauranteTipoCozinha = function () {
    /// <signature>
    ///   <summary></summary>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
};

ArrayOfSERPModelDataCombo = function () {
    /// <signature>
    ///   <summary></summary>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
};

ArrayOfSERPModelHoraCombo = function () {
    /// <signature>
    ///   <summary></summary>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
};

ArrayOfSERPModelTipoCozinha = function () {
    /// <signature>
    ///   <summary></summary>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
};

ArrayOfSERPModelTipoCozinhaCombo = function () {
    /// <signature>
    ///   <summary></summary>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
};

CapaRestauranteModelPagamento = function (CARTAO, CODIGOS, DESCRICAO, ICONE, LITERALPAGTOS, LITERALPAGTOSSSL) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="CARTAO" type="ArrayOfstring"></param>    
    ///   <param name="CODIGOS" type="ArrayOfint"></param>    
    ///   <param name="DESCRICAO" type="ArrayOfstring"></param>    
    ///   <param name="ICONE" type="ArrayOfstring"></param>    
    ///   <param name="LITERALPAGTOS" type="string"></param>    
    ///   <param name="LITERALPAGTOSSSL" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.CARTAO = CARTAO || null;
    self.CODIGOS = CODIGOS || null;
    self.DESCRICAO = DESCRICAO || null;
    self.ICONE = ICONE || null;
    self.LITERALPAGTOS = LITERALPAGTOS || null;
    self.LITERALPAGTOSSSL = LITERALPAGTOSSSL || null;
};

CapaRestauranteModelSugestao = function (BOTAO1, BOTAO2, CODIGOGRUPO, CODIGOPRATO, CODINTEGRACAOPRATO, DESCRICAOFULLPRATO, DESCRICAOPRATO, FOTOPRATO, FOTOPRATOGRANDE, GRUPOPRATO, INDICAPIZZAMEIO, INDICAPIZZATAM, PATH, PRATO, QTDEOPCOESPRATO, TEMSUGESTAO, TIPOPRATO, TITLEBOTAO1, TITLEBOTAO2) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="BOTAO1" type="string"></param>    
    ///   <param name="BOTAO2" type="string"></param>    
    ///   <param name="CODIGOGRUPO" type="int"></param>    
    ///   <param name="CODIGOPRATO" type="int"></param>    
    ///   <param name="CODINTEGRACAOPRATO" type="int"></param>    
    ///   <param name="DESCRICAOFULLPRATO" type="string"></param>    
    ///   <param name="DESCRICAOPRATO" type="string"></param>    
    ///   <param name="FOTOPRATO" type="string"></param>    
    ///   <param name="FOTOPRATOGRANDE" type="string"></param>    
    ///   <param name="GRUPOPRATO" type="int"></param>    
    ///   <param name="INDICAPIZZAMEIO" type="string"></param>    
    ///   <param name="INDICAPIZZATAM" type="string"></param>    
    ///   <param name="PATH" type="string"></param>    
    ///   <param name="PRATO" type="Prato"></param>    
    ///   <param name="QTDEOPCOESPRATO" type="string"></param>    
    ///   <param name="TEMSUGESTAO" type="boolean"></param>    
    ///   <param name="TIPOPRATO" type="string"></param>    
    ///   <param name="TITLEBOTAO1" type="string"></param>    
    ///   <param name="TITLEBOTAO2" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.BOTAO1 = BOTAO1 || null;
    self.BOTAO2 = BOTAO2 || null;
    self.CODIGOGRUPO = CODIGOGRUPO || null;
    self.CODIGOPRATO = CODIGOPRATO || null;
    self.CODINTEGRACAOPRATO = CODINTEGRACAOPRATO || null;
    self.DESCRICAOFULLPRATO = DESCRICAOFULLPRATO || null;
    self.DESCRICAOPRATO = DESCRICAOPRATO || null;
    self.FOTOPRATO = FOTOPRATO || null;
    self.FOTOPRATOGRANDE = FOTOPRATOGRANDE || null;
    self.GRUPOPRATO = GRUPOPRATO || null;
    self.INDICAPIZZAMEIO = INDICAPIZZAMEIO || null;
    self.INDICAPIZZATAM = INDICAPIZZATAM || null;
    self.PATH = PATH || null;
    self.PRATO = PRATO || null;
    self.QTDEOPCOESPRATO = QTDEOPCOESPRATO || null;
    self.TEMSUGESTAO = TEMSUGESTAO || null;
    self.TIPOPRATO = TIPOPRATO || null;
    self.TITLEBOTAO1 = TITLEBOTAO1 || null;
    self.TITLEBOTAO2 = TITLEBOTAO2 || null;
};

CardapioRestauranteModelPrato = function (CODIGOPRATO, DESCRDETPRATO, DESCRICAOPRATO, DescricaoDetalhadaFormatada, FOTOPRATO, ImagemFormatada, ListaPrecoPrato, ListaTextoOpcoes, PRECOPRATO, TEMOPCOES, TEXTOOPCOES) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="CODIGOPRATO" type="int"></param>    
    ///   <param name="DESCRDETPRATO" type="string"></param>    
    ///   <param name="DESCRICAOPRATO" type="string"></param>    
    ///   <param name="DescricaoDetalhadaFormatada" type="string"></param>    
    ///   <param name="FOTOPRATO" type="string"></param>    
    ///   <param name="ImagemFormatada" type="string"></param>    
    ///   <param name="ListaPrecoPrato" type="ArrayOfstring"></param>    
    ///   <param name="ListaTextoOpcoes" type="ArrayOfstring"></param>    
    ///   <param name="PRECOPRATO" type="string"></param>    
    ///   <param name="TEMOPCOES" type="string"></param>    
    ///   <param name="TEXTOOPCOES" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.CODIGOPRATO = CODIGOPRATO || null;
    self.DESCRDETPRATO = DESCRDETPRATO || null;
    self.DESCRICAOPRATO = DESCRICAOPRATO || null;
    self.DescricaoDetalhadaFormatada = DescricaoDetalhadaFormatada || null;
    self.FOTOPRATO = FOTOPRATO || null;
    self.ImagemFormatada = ImagemFormatada || null;
    self.ListaPrecoPrato = ListaPrecoPrato || null;
    self.ListaTextoOpcoes = ListaTextoOpcoes || null;
    self.PRECOPRATO = PRECOPRATO || null;
    self.TEMOPCOES = TEMOPCOES || null;
    self.TEXTOOPCOES = TEXTOOPCOES || null;
};

ContactUs = function (Email, Genre, Message, Name, PhoneDDD, PhoneNumber) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="Email" type="string"></param>    
    ///   <param name="Genre" type="GenreEnum"></param>    
    ///   <param name="Message" type="string"></param>    
    ///   <param name="Name" type="string"></param>    
    ///   <param name="PhoneDDD" type="string"></param>    
    ///   <param name="PhoneNumber" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.Email = Email || null;
    self.Genre = Genre || null;
    self.Message = Message || null;
    self.Name = Name || null;
    self.PhoneDDD = PhoneDDD || null;
    self.PhoneNumber = PhoneNumber || null;
};

ContactUsModel = function (EMAIL, ERROCADASTRO, ERROMENS, NOME, SELECT) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="EMAIL" type="string"></param>    
    ///   <param name="ERROCADASTRO" type="boolean"></param>    
    ///   <param name="ERROMENS" type="string"></param>    
    ///   <param name="NOME" type="string"></param>    
    ///   <param name="SELECT" type="boolean"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.EMAIL = EMAIL || null;
    self.ERROCADASTRO = ERROCADASTRO || null;
    self.ERROMENS = ERROMENS || null;
    self.NOME = NOME || null;
    self.SELECT = SELECT || null;
};

DadosUsuario = function (CEPFAVORITO, COD, COZINHAUSUARIO, CPF, DTNASCAA, DTNASCDD, DTNASCMM, EMAIL, EMAILNVREST, EMAILPROMO, ENDERECO, ENVIAPESQUISA, EXIBEPREF, EXIBERES, LSENHA, NOME, SENHA, SEXO, TIME) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="CEPFAVORITO" type="string"></param>    
    ///   <param name="COD" type="int"></param>    
    ///   <param name="COZINHAUSUARIO" type="string"></param>    
    ///   <param name="CPF" type="string"></param>    
    ///   <param name="DTNASCAA" type="string"></param>    
    ///   <param name="DTNASCDD" type="string"></param>    
    ///   <param name="DTNASCMM" type="string"></param>    
    ///   <param name="EMAIL" type="string"></param>    
    ///   <param name="EMAILNVREST" type="string"></param>    
    ///   <param name="EMAILPROMO" type="string"></param>    
    ///   <param name="ENDERECO" type="ArrayOfDadosUsuarioEndereco"></param>    
    ///   <param name="ENVIAPESQUISA" type="string"></param>    
    ///   <param name="EXIBEPREF" type="string"></param>    
    ///   <param name="EXIBERES" type="string"></param>    
    ///   <param name="LSENHA" type="string"></param>    
    ///   <param name="NOME" type="string"></param>    
    ///   <param name="SENHA" type="string"></param>    
    ///   <param name="SEXO" type="string"></param>    
    ///   <param name="TIME" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.CEPFAVORITO = CEPFAVORITO || null;
    self.COD = COD || null;
    self.COZINHAUSUARIO = COZINHAUSUARIO || null;
    self.CPF = CPF || null;
    self.DTNASCAA = DTNASCAA || null;
    self.DTNASCDD = DTNASCDD || null;
    self.DTNASCMM = DTNASCMM || null;
    self.EMAIL = EMAIL || null;
    self.EMAILNVREST = EMAILNVREST || null;
    self.EMAILPROMO = EMAILPROMO || null;
    self.ENDERECO = ENDERECO || null;
    self.ENVIAPESQUISA = ENVIAPESQUISA || null;
    self.EXIBEPREF = EXIBEPREF || null;
    self.EXIBERES = EXIBERES || null;
    self.LSENHA = LSENHA || null;
    self.NOME = NOME || null;
    self.SENHA = SENHA || null;
    self.SEXO = SEXO || null;
    self.TIME = TIME || null;
};

DadosUsuarioEndereco = function (BAIRRO, CADASTRO, CEP, CIDADE, COMPL, DDD1, DDD2, ENDERECO, ESTADO, INDFAV, NUMERO, TELEFONE1, TELEFONE2, TIPENDER) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="BAIRRO" type="string"></param>    
    ///   <param name="CADASTRO" type="string"></param>    
    ///   <param name="CEP" type="string"></param>    
    ///   <param name="CIDADE" type="string"></param>    
    ///   <param name="COMPL" type="string"></param>    
    ///   <param name="DDD1" type="string"></param>    
    ///   <param name="DDD2" type="string"></param>    
    ///   <param name="ENDERECO" type="string"></param>    
    ///   <param name="ESTADO" type="string"></param>    
    ///   <param name="INDFAV" type="int"></param>    
    ///   <param name="NUMERO" type="string"></param>    
    ///   <param name="TELEFONE1" type="string"></param>    
    ///   <param name="TELEFONE2" type="string"></param>    
    ///   <param name="TIPENDER" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.BAIRRO = BAIRRO || null;
    self.CADASTRO = CADASTRO || null;
    self.CEP = CEP || null;
    self.CIDADE = CIDADE || null;
    self.COMPL = COMPL || null;
    self.DDD1 = DDD1 || null;
    self.DDD2 = DDD2 || null;
    self.ENDERECO = ENDERECO || null;
    self.ESTADO = ESTADO || null;
    self.INDFAV = INDFAV || null;
    self.NUMERO = NUMERO || null;
    self.TELEFONE1 = TELEFONE1 || null;
    self.TELEFONE2 = TELEFONE2 || null;
    self.TIPENDER = TIPENDER || null;
};

EnderecoSelecao = function (BAIRRO, CEP, CIDADE, ENDERECO, ESTADO, SELECTED, TITLE) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="BAIRRO" type="string"></param>    
    ///   <param name="CEP" type="string"></param>    
    ///   <param name="CIDADE" type="string"></param>    
    ///   <param name="ENDERECO" type="string"></param>    
    ///   <param name="ESTADO" type="string"></param>    
    ///   <param name="SELECTED" type="string"></param>    
    ///   <param name="TITLE" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.BAIRRO = BAIRRO || null;
    self.CEP = CEP || null;
    self.CIDADE = CIDADE || null;
    self.ENDERECO = ENDERECO || null;
    self.ESTADO = ESTADO || null;
    self.SELECTED = SELECTED || null;
    self.TITLE = TITLE || null;
};

ErrorModel = function (ERRO, ERROMENSAGEM) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="ERRO" type="boolean"></param>    
    ///   <param name="ERROMENSAGEM" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.ERRO = ERRO || null;
    self.ERROMENSAGEM = ERROMENSAGEM || null;
};

FinalizaPedidoRestauranteModelAgendamento = function (AGENDAMENTO, DATAPEDIDO, HORAPEDIDO, INDSELECIONADO, TIPOPEDIDO, TIPOPROGRAMACAO) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="AGENDAMENTO" type="ArrayOfHorarioAgendamento"></param>    
    ///   <param name="DATAPEDIDO" type="string"></param>    
    ///   <param name="HORAPEDIDO" type="string"></param>    
    ///   <param name="INDSELECIONADO" type="string"></param>    
    ///   <param name="TIPOPEDIDO" type="string"></param>    
    ///   <param name="TIPOPROGRAMACAO" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.AGENDAMENTO = AGENDAMENTO || null;
    self.DATAPEDIDO = DATAPEDIDO || null;
    self.HORAPEDIDO = HORAPEDIDO || null;
    self.INDSELECIONADO = INDSELECIONADO || null;
    self.TIPOPEDIDO = TIPOPEDIDO || null;
    self.TIPOPROGRAMACAO = TIPOPROGRAMACAO || null;
};

FinalizaPedidoRestauranteModelAtendeCEP = function (ATENDE, CODFILIAL, TAXACEP) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="ATENDE" type="boolean"></param>    
    ///   <param name="CODFILIAL" type="short"></param>    
    ///   <param name="TAXACEP" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.ATENDE = ATENDE || null;
    self.CODFILIAL = CODFILIAL || null;
    self.TAXACEP = TAXACEP || null;
};

FinalizaPedidoRestauranteModelDadosEntrega = function (BAIRRO, CEP, CIDADE, COMBOEND, COMPLEMENTO, DDD1, DDD2, ENDERECO, ESTADO, NOME, NUMERO, OBSCLIENTE, OPEREND, SELECAOEND, TELEFONE1, TELEFONE2, TIPO) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="BAIRRO" type="string"></param>    
    ///   <param name="CEP" type="string"></param>    
    ///   <param name="CIDADE" type="string"></param>    
    ///   <param name="COMBOEND" type="boolean"></param>    
    ///   <param name="COMPLEMENTO" type="string"></param>    
    ///   <param name="DDD1" type="string"></param>    
    ///   <param name="DDD2" type="string"></param>    
    ///   <param name="ENDERECO" type="string"></param>    
    ///   <param name="ESTADO" type="string"></param>    
    ///   <param name="NOME" type="string"></param>    
    ///   <param name="NUMERO" type="string"></param>    
    ///   <param name="OBSCLIENTE" type="string"></param>    
    ///   <param name="OPEREND" type="string"></param>    
    ///   <param name="SELECAOEND" type="ArrayOfFinalizaPedidoRestauranteModelDadosEntregaSelecaoEndereco"></param>    
    ///   <param name="TELEFONE1" type="string"></param>    
    ///   <param name="TELEFONE2" type="string"></param>    
    ///   <param name="TIPO" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.BAIRRO = BAIRRO || null;
    self.CEP = CEP || null;
    self.CIDADE = CIDADE || null;
    self.COMBOEND = COMBOEND || null;
    self.COMPLEMENTO = COMPLEMENTO || null;
    self.DDD1 = DDD1 || null;
    self.DDD2 = DDD2 || null;
    self.ENDERECO = ENDERECO || null;
    self.ESTADO = ESTADO || null;
    self.NOME = NOME || null;
    self.NUMERO = NUMERO || null;
    self.OBSCLIENTE = OBSCLIENTE || null;
    self.OPEREND = OPEREND || null;
    self.SELECAOEND = SELECAOEND || null;
    self.TELEFONE1 = TELEFONE1 || null;
    self.TELEFONE2 = TELEFONE2 || null;
    self.TIPO = TIPO || null;
};

FinalizaPedidoRestauranteModelDadosEntregaSelecaoEndereco = function (BAIRRO, CEP, CIDADE, COMPLEMENTO, DDD1, DDD2, ENDERECO, ESTADO, NUMERO, OPER, TELEFONE1, TELEFONE2, TIPO) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="BAIRRO" type="string"></param>    
    ///   <param name="CEP" type="string"></param>    
    ///   <param name="CIDADE" type="string"></param>    
    ///   <param name="COMPLEMENTO" type="string"></param>    
    ///   <param name="DDD1" type="string"></param>    
    ///   <param name="DDD2" type="string"></param>    
    ///   <param name="ENDERECO" type="string"></param>    
    ///   <param name="ESTADO" type="string"></param>    
    ///   <param name="NUMERO" type="string"></param>    
    ///   <param name="OPER" type="string"></param>    
    ///   <param name="TELEFONE1" type="string"></param>    
    ///   <param name="TELEFONE2" type="string"></param>    
    ///   <param name="TIPO" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.BAIRRO = BAIRRO || null;
    self.CEP = CEP || null;
    self.CIDADE = CIDADE || null;
    self.COMPLEMENTO = COMPLEMENTO || null;
    self.DDD1 = DDD1 || null;
    self.DDD2 = DDD2 || null;
    self.ENDERECO = ENDERECO || null;
    self.ESTADO = ESTADO || null;
    self.NUMERO = NUMERO || null;
    self.OPER = OPER || null;
    self.TELEFONE1 = TELEFONE1 || null;
    self.TELEFONE2 = TELEFONE2 || null;
    self.TIPO = TIPO || null;
};

FinalizaPedidoRestauranteModelDadosPagamento = function (CARTAO, PAGTOS, TEMCARTAO) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="CARTAO" type="ArrayOfFinalizaPedidoRestauranteModelDadosPagamentoData"></param>    
    ///   <param name="PAGTOS" type="ArrayOfFinalizaPedidoRestauranteModelDadosPagamentoData"></param>    
    ///   <param name="TEMCARTAO" type="boolean"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.CARTAO = CARTAO || null;
    self.PAGTOS = PAGTOS || null;
    self.TEMCARTAO = TEMCARTAO || null;
};

FinalizaPedidoRestauranteModelDadosPagamentoData = function (CODIGOPAGTO, DESCRICAOPAGTO, ICONEPAGTO, ImagemFormatada) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="CODIGOPAGTO" type="string"></param>    
    ///   <param name="DESCRICAOPAGTO" type="string"></param>    
    ///   <param name="ICONEPAGTO" type="string"></param>    
    ///   <param name="ImagemFormatada" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.CODIGOPAGTO = CODIGOPAGTO || null;
    self.DESCRICAOPAGTO = DESCRICAOPAGTO || null;
    self.ICONEPAGTO = ICONEPAGTO || null;
    self.ImagemFormatada = ImagemFormatada || null;
};

FinalizaPedidoRestauranteModelPrato = function (NOMEPRATO, OBSPRATO, QTDEPRATO, TEXTOOPCOES, TextoOpcoesFormatado, VALORPRATO, VALORUNITARIO) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="NOMEPRATO" type="string"></param>    
    ///   <param name="OBSPRATO" type="string"></param>    
    ///   <param name="QTDEPRATO" type="string"></param>    
    ///   <param name="TEXTOOPCOES" type="string"></param>    
    ///   <param name="TextoOpcoesFormatado" type="string"></param>    
    ///   <param name="VALORPRATO" type="string"></param>    
    ///   <param name="VALORUNITARIO" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.NOMEPRATO = NOMEPRATO || null;
    self.OBSPRATO = OBSPRATO || null;
    self.QTDEPRATO = QTDEPRATO || null;
    self.TEXTOOPCOES = TEXTOOPCOES || null;
    self.TextoOpcoesFormatado = TextoOpcoesFormatado || null;
    self.VALORPRATO = VALORPRATO || null;
    self.VALORUNITARIO = VALORUNITARIO || null;
};

FinalizaPedidoRestauranteModelVoucher = function (LITERAL, PEDEVOUCHER, VOUCHER) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="LITERAL" type="string"></param>    
    ///   <param name="PEDEVOUCHER" type="string"></param>    
    ///   <param name="VOUCHER" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.LITERAL = LITERAL || null;
    self.PEDEVOUCHER = PEDEVOUCHER || null;
    self.VOUCHER = VOUCHER || null;
};

GetApplicationVersionResult = function (ForcedVersion, RecommendedVersion) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="ForcedVersion" type="string"></param>    
    ///   <param name="RecommendedVersion" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.ForcedVersion = ForcedVersion || null;
    self.RecommendedVersion = RecommendedVersion || null;
};

GetOrderOnlineStateResult = function (Value, Value, Value) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="Value" type="int"></param>    
    ///   <param name="Value" type="int"></param>    
    ///   <param name="Value" type="int"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.Value = Value || null;
    self.Value = Value || null;
    self.Value = Value || null;
};

GrupoPrato = function (CODIGOGRUPO, DESCRICAOGRUPO, GRUPOATIVO, TEXTOCOMPL) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="CODIGOGRUPO" type="int"></param>    
    ///   <param name="DESCRICAOGRUPO" type="string"></param>    
    ///   <param name="GRUPOATIVO" type="boolean"></param>    
    ///   <param name="TEXTOCOMPL" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.CODIGOGRUPO = CODIGOGRUPO || null;
    self.DESCRICAOGRUPO = DESCRICAOGRUPO || null;
    self.GRUPOATIVO = GRUPOATIVO || null;
    self.TEXTOCOMPL = TEXTOCOMPL || null;
};

HomeModel = function (CEP, COOKIECEP, COOKIEEMAIL, CSS, DADOSUSUARIO, EMAILNEWS, ENDERECOSELECAO, ERRO, ERROMENSAGEM, LISTARESTAURANTES, LOGADO, LOGOPARCEIRO, NOMENEWS, PEDIDOSREFAZER, RESTAURANTESAVALIACAO, RESTAURANTESPROMOCAO, SELECT, TEMCOOKIECEP, TEMCOOKIEEMAIL, TITLEPRINCIPAL, URLPARCEIRO) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="CEP" type="string"></param>    
    ///   <param name="COOKIECEP" type="string"></param>    
    ///   <param name="COOKIEEMAIL" type="string"></param>    
    ///   <param name="CSS" type="string"></param>    
    ///   <param name="DADOSUSUARIO" type="HomeModelDadosUsuario"></param>    
    ///   <param name="EMAILNEWS" type="string"></param>    
    ///   <param name="ENDERECOSELECAO" type="ArrayOfEnderecoSelecao"></param>    
    ///   <param name="ERRO" type="boolean"></param>    
    ///   <param name="ERROMENSAGEM" type="string"></param>    
    ///   <param name="LISTARESTAURANTES" type="string"></param>    
    ///   <param name="LOGADO" type="string"></param>    
    ///   <param name="LOGOPARCEIRO" type="string"></param>    
    ///   <param name="NOMENEWS" type="string"></param>    
    ///   <param name="PEDIDOSREFAZER" type="HomeModelPedidosRefazer"></param>    
    ///   <param name="RESTAURANTESAVALIACAO" type="ArrayOfHomeModelRestaurantes"></param>    
    ///   <param name="RESTAURANTESPROMOCAO" type="ArrayOfHomeModelRestaurantes"></param>    
    ///   <param name="SELECT" type="boolean"></param>    
    ///   <param name="TEMCOOKIECEP" type="boolean"></param>    
    ///   <param name="TEMCOOKIEEMAIL" type="boolean"></param>    
    ///   <param name="TITLEPRINCIPAL" type="string"></param>    
    ///   <param name="URLPARCEIRO" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.CEP = CEP || null;
    self.COOKIECEP = COOKIECEP || null;
    self.COOKIEEMAIL = COOKIEEMAIL || null;
    self.CSS = CSS || null;
    self.DADOSUSUARIO = DADOSUSUARIO || null;
    self.EMAILNEWS = EMAILNEWS || null;
    self.ENDERECOSELECAO = ENDERECOSELECAO || null;
    self.ERRO = ERRO || null;
    self.ERROMENSAGEM = ERROMENSAGEM || null;
    self.LISTARESTAURANTES = LISTARESTAURANTES || null;
    self.LOGADO = LOGADO || null;
    self.LOGOPARCEIRO = LOGOPARCEIRO || null;
    self.NOMENEWS = NOMENEWS || null;
    self.PEDIDOSREFAZER = PEDIDOSREFAZER || null;
    self.RESTAURANTESAVALIACAO = RESTAURANTESAVALIACAO || null;
    self.RESTAURANTESPROMOCAO = RESTAURANTESPROMOCAO || null;
    self.SELECT = SELECT || null;
    self.TEMCOOKIECEP = TEMCOOKIECEP || null;
    self.TEMCOOKIEEMAIL = TEMCOOKIEEMAIL || null;
    self.TITLEPRINCIPAL = TITLEPRINCIPAL || null;
    self.URLPARCEIRO = URLPARCEIRO || null;
};

HomeModelDadosUsuario = function (CEPFAVORITO, CEPFAVORITO, CEPFAVORITO, CEPFAVORITO) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="CEPFAVORITO" type="string"></param>    
    ///   <param name="CEPFAVORITO" type="string"></param>    
    ///   <param name="CEPFAVORITO" type="string"></param>    
    ///   <param name="CEPFAVORITO" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.CEPFAVORITO = CEPFAVORITO || null;
    self.CEPFAVORITO = CEPFAVORITO || null;
    self.CEPFAVORITO = CEPFAVORITO || null;
    self.CEPFAVORITO = CEPFAVORITO || null;
};

HomeModelPedidosRefazer = function (PEDIDOS, TEMPEDIDOSREFAZER) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="PEDIDOS" type="ArrayOfHomeModelPedidosRefazerItem"></param>    
    ///   <param name="TEMPEDIDOSREFAZER" type="boolean"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.PEDIDOS = PEDIDOS || null;
    self.TEMPEDIDOSREFAZER = TEMPEDIDOSREFAZER || null;
};

HomeModelPedidosRefazerItem = function (DATAPEDIDO, DESCRPAGTOPEDIDO, DataPedidoFormatada, HORAPEDIDO, ICONEPAGTOPEDIDO, ITENS, ImagemRestauranteFormatada, LOGORESTAURANTE, NOMERESTAURANTE, NROPEDIDO, PTOSFIDELIDADE, TAXAENTREGA, TITLERESTAURANTE, TaxaEntregaFormatado, VLDESCONTO, VLSUBTOTAL, VLTOTALPEDIDO, ValorDescontoFormatado, ValorSubTotalFormatado, ValorTotalFormatado) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="DATAPEDIDO" type="string"></param>    
    ///   <param name="DESCRPAGTOPEDIDO" type="string"></param>    
    ///   <param name="DataPedidoFormatada" type="string"></param>    
    ///   <param name="HORAPEDIDO" type="string"></param>    
    ///   <param name="ICONEPAGTOPEDIDO" type="string"></param>    
    ///   <param name="ITENS" type="ArrayOfHomeModelPedidosRefazerItemOpcao"></param>    
    ///   <param name="ImagemRestauranteFormatada" type="string"></param>    
    ///   <param name="LOGORESTAURANTE" type="string"></param>    
    ///   <param name="NOMERESTAURANTE" type="string"></param>    
    ///   <param name="NROPEDIDO" type="int"></param>    
    ///   <param name="PTOSFIDELIDADE" type="string"></param>    
    ///   <param name="TAXAENTREGA" type="string"></param>    
    ///   <param name="TITLERESTAURANTE" type="string"></param>    
    ///   <param name="TaxaEntregaFormatado" type="string"></param>    
    ///   <param name="VLDESCONTO" type="string"></param>    
    ///   <param name="VLSUBTOTAL" type="string"></param>    
    ///   <param name="VLTOTALPEDIDO" type="string"></param>    
    ///   <param name="ValorDescontoFormatado" type="string"></param>    
    ///   <param name="ValorSubTotalFormatado" type="string"></param>    
    ///   <param name="ValorTotalFormatado" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.DATAPEDIDO = DATAPEDIDO || null;
    self.DESCRPAGTOPEDIDO = DESCRPAGTOPEDIDO || null;
    self.DataPedidoFormatada = DataPedidoFormatada || null;
    self.HORAPEDIDO = HORAPEDIDO || null;
    self.ICONEPAGTOPEDIDO = ICONEPAGTOPEDIDO || null;
    self.ITENS = ITENS || null;
    self.ImagemRestauranteFormatada = ImagemRestauranteFormatada || null;
    self.LOGORESTAURANTE = LOGORESTAURANTE || null;
    self.NOMERESTAURANTE = NOMERESTAURANTE || null;
    self.NROPEDIDO = NROPEDIDO || null;
    self.PTOSFIDELIDADE = PTOSFIDELIDADE || null;
    self.TAXAENTREGA = TAXAENTREGA || null;
    self.TITLERESTAURANTE = TITLERESTAURANTE || null;
    self.TaxaEntregaFormatado = TaxaEntregaFormatado || null;
    self.VLDESCONTO = VLDESCONTO || null;
    self.VLSUBTOTAL = VLSUBTOTAL || null;
    self.VLTOTALPEDIDO = VLTOTALPEDIDO || null;
    self.ValorDescontoFormatado = ValorDescontoFormatado || null;
    self.ValorSubTotalFormatado = ValorSubTotalFormatado || null;
    self.ValorTotalFormatado = ValorTotalFormatado || null;
};

HomeModelPedidosRefazerItemOpcao = function (NOMEPRATO, OBSPRATO, QTDEPRATO, TEXTOOPCOES, VALORPRATO, VALORUNITARIO) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="NOMEPRATO" type="string"></param>    
    ///   <param name="OBSPRATO" type="string"></param>    
    ///   <param name="QTDEPRATO" type="string"></param>    
    ///   <param name="TEXTOOPCOES" type="string"></param>    
    ///   <param name="VALORPRATO" type="string"></param>    
    ///   <param name="VALORUNITARIO" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.NOMEPRATO = NOMEPRATO || null;
    self.OBSPRATO = OBSPRATO || null;
    self.QTDEPRATO = QTDEPRATO || null;
    self.TEXTOOPCOES = TEXTOOPCOES || null;
    self.VALORPRATO = VALORPRATO || null;
    self.VALORUNITARIO = VALORUNITARIO || null;
};

HomeModelRestaurantes = function (CIDADE, CODIGO, DESCPROMO, ENDERECO, ESTADO, FILIAL, FOTO, IMAGEMESTRELAS, MEDIAGERAL, NOME, NUMEROESTRELAS, TAXAPROMO, TIPOSCOZINHA) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="CIDADE" type="string"></param>    
    ///   <param name="CODIGO" type="short"></param>    
    ///   <param name="DESCPROMO" type="string"></param>    
    ///   <param name="ENDERECO" type="string"></param>    
    ///   <param name="ESTADO" type="string"></param>    
    ///   <param name="FILIAL" type="short"></param>    
    ///   <param name="FOTO" type="string"></param>    
    ///   <param name="IMAGEMESTRELAS" type="string"></param>    
    ///   <param name="MEDIAGERAL" type="decimal"></param>    
    ///   <param name="NOME" type="string"></param>    
    ///   <param name="NUMEROESTRELAS" type="string"></param>    
    ///   <param name="TAXAPROMO" type="string"></param>    
    ///   <param name="TIPOSCOZINHA" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.CIDADE = CIDADE || null;
    self.CODIGO = CODIGO || null;
    self.DESCPROMO = DESCPROMO || null;
    self.ENDERECO = ENDERECO || null;
    self.ESTADO = ESTADO || null;
    self.FILIAL = FILIAL || null;
    self.FOTO = FOTO || null;
    self.IMAGEMESTRELAS = IMAGEMESTRELAS || null;
    self.MEDIAGERAL = MEDIAGERAL || null;
    self.NOME = NOME || null;
    self.NUMEROESTRELAS = NUMEROESTRELAS || null;
    self.TAXAPROMO = TAXAPROMO || null;
    self.TIPOSCOZINHA = TIPOSCOZINHA || null;
};

HomeModelSearch = function (CEP, CodigoRede, IdKitchenType) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="CEP" type="string"></param>    
    ///   <param name="CodigoRede" type="short"></param>    
    ///   <param name="IdKitchenType" type="int"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.CEP = CEP || null;
    self.CodigoRede = CodigoRede || null;
    self.IdKitchenType = IdKitchenType || null;
};

Horario = function (AGENDAMENTO, INDSELECIONADO) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="AGENDAMENTO" type="ArrayOfHorarioAgendamento"></param>    
    ///   <param name="INDSELECIONADO" type="int"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.AGENDAMENTO = AGENDAMENTO || null;
    self.INDSELECIONADO = INDSELECIONADO || null;
};

HorarioAgendamento = function (DIA, HORARIOS, SELECTED, VALUE) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="DIA" type="string"></param>    
    ///   <param name="HORARIOS" type="ArrayOfstring"></param>    
    ///   <param name="SELECTED" type="string"></param>    
    ///   <param name="VALUE" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.DIA = DIA || null;
    self.HORARIOS = HORARIOS || null;
    self.SELECTED = SELECTED || null;
    self.VALUE = VALUE || null;
};

LogInModel = function (DADOSUSUARIO, LOGADO, SessionId) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="DADOSUSUARIO" type="LogInModelDadosUsuario"></param>    
    ///   <param name="LOGADO" type="boolean"></param>    
    ///   <param name="SessionId" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.DADOSUSUARIO = DADOSUSUARIO || null;
    self.LOGADO = LOGADO || null;
    self.SessionId = SessionId || null;
};

LogInModelDadosUsuario = function (CEPFAVORITO, COD, COZINHAUSUARIO, CPF, DTNASCAA, DTNASCDD, DTNASCMM, EMAIL, EMAILNVREST, EMAILPROMO, ENDERECO, ENVIAPESQUISA, EXIBEPREF, EXIBERES, LSENHA, NOME, SENHA, SEXO, TIME) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="CEPFAVORITO" type="string"></param>    
    ///   <param name="COD" type="string"></param>    
    ///   <param name="COZINHAUSUARIO" type="string"></param>    
    ///   <param name="CPF" type="string"></param>    
    ///   <param name="DTNASCAA" type="string"></param>    
    ///   <param name="DTNASCDD" type="string"></param>    
    ///   <param name="DTNASCMM" type="string"></param>    
    ///   <param name="EMAIL" type="string"></param>    
    ///   <param name="EMAILNVREST" type="string"></param>    
    ///   <param name="EMAILPROMO" type="string"></param>    
    ///   <param name="ENDERECO" type="ArrayOfLogInModelEndereco"></param>    
    ///   <param name="ENVIAPESQUISA" type="string"></param>    
    ///   <param name="EXIBEPREF" type="string"></param>    
    ///   <param name="EXIBERES" type="string"></param>    
    ///   <param name="LSENHA" type="string"></param>    
    ///   <param name="NOME" type="string"></param>    
    ///   <param name="SENHA" type="string"></param>    
    ///   <param name="SEXO" type="string"></param>    
    ///   <param name="TIME" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.CEPFAVORITO = CEPFAVORITO || null;
    self.COD = COD || null;
    self.COZINHAUSUARIO = COZINHAUSUARIO || null;
    self.CPF = CPF || null;
    self.DTNASCAA = DTNASCAA || null;
    self.DTNASCDD = DTNASCDD || null;
    self.DTNASCMM = DTNASCMM || null;
    self.EMAIL = EMAIL || null;
    self.EMAILNVREST = EMAILNVREST || null;
    self.EMAILPROMO = EMAILPROMO || null;
    self.ENDERECO = ENDERECO || null;
    self.ENVIAPESQUISA = ENVIAPESQUISA || null;
    self.EXIBEPREF = EXIBEPREF || null;
    self.EXIBERES = EXIBERES || null;
    self.LSENHA = LSENHA || null;
    self.NOME = NOME || null;
    self.SENHA = SENHA || null;
    self.SEXO = SEXO || null;
    self.TIME = TIME || null;
};

LogInModelEndereco = function (BAIRRO, CADASTRO, CEP, CIDADE, COMPL, DDD1, DDD2, ENDERECO, ESTADO, INDFAV, NUMERO, TELEFONE1, TELEFONE2, TIPENDER) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="BAIRRO" type="string"></param>    
    ///   <param name="CADASTRO" type="string"></param>    
    ///   <param name="CEP" type="string"></param>    
    ///   <param name="CIDADE" type="string"></param>    
    ///   <param name="COMPL" type="string"></param>    
    ///   <param name="DDD1" type="string"></param>    
    ///   <param name="DDD2" type="string"></param>    
    ///   <param name="ENDERECO" type="string"></param>    
    ///   <param name="ESTADO" type="string"></param>    
    ///   <param name="INDFAV" type="string"></param>    
    ///   <param name="NUMERO" type="string"></param>    
    ///   <param name="TELEFONE1" type="string"></param>    
    ///   <param name="TELEFONE2" type="string"></param>    
    ///   <param name="TIPENDER" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.BAIRRO = BAIRRO || null;
    self.CADASTRO = CADASTRO || null;
    self.CEP = CEP || null;
    self.CIDADE = CIDADE || null;
    self.COMPL = COMPL || null;
    self.DDD1 = DDD1 || null;
    self.DDD2 = DDD2 || null;
    self.ENDERECO = ENDERECO || null;
    self.ESTADO = ESTADO || null;
    self.INDFAV = INDFAV || null;
    self.NUMERO = NUMERO || null;
    self.TELEFONE1 = TELEFONE1 || null;
    self.TELEFONE2 = TELEFONE2 || null;
    self.TIPENDER = TIPENDER || null;
};

MenuDetailModel = function (BAIRRORESTAURANTE, BOTAO1, BOTAO2, CBASE, CEP, CIDADERESTAURANTE, CLASSAGENDAMENTO, CODFILIAL, CODRESTAURANTE, DATAPESQUISA, ENDERECORESTAURANTE, ENDERECOSELECAO, ERRO, ERROMENSAGEM, ESTRELASAVALIACAO, EXIBE, EXIBEPASSOS, FAVORITO, FAVORITOS, FOTOGRDE, FOTOPRATO, GRUPOS, GRUPOSELECIONADO, HORAPESQUISA, HORARIO, HORARIOS, ICONESMEDIAGERAL, LINKINFORESTAURANTE, LISTA, LITAGENDAMENTO1, LITAGENDAMENTO2, LOGIN, LOGORESTAURANTE, NOMERESTAURANTE, ORIGEM, PAGBLANK, PAGINA, PRATO, PROMOCAO, TEMFOTO, TEMPOENTREGA, TEXTOAVISO, TEXTOLIVRE, TEXTOPROMO, TIPO, TIPOSCOZINHA, TIPOSPAGTO, TITLEAGENDAMENTO, TITLEBOTAO1, TITLEBOTAO2, TITLEPRINCIPAL, TOTALAVALIACOES, URLRESTAURANTE, VALORPEDIDOMIN, VERMENOS) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="BAIRRORESTAURANTE" type="string"></param>    
    ///   <param name="BOTAO1" type="string"></param>    
    ///   <param name="BOTAO2" type="string"></param>    
    ///   <param name="CBASE" type="string"></param>    
    ///   <param name="CEP" type="string"></param>    
    ///   <param name="CIDADERESTAURANTE" type="string"></param>    
    ///   <param name="CLASSAGENDAMENTO" type="string"></param>    
    ///   <param name="CODFILIAL" type="short"></param>    
    ///   <param name="CODRESTAURANTE" type="short"></param>    
    ///   <param name="DATAPESQUISA" type="string"></param>    
    ///   <param name="ENDERECORESTAURANTE" type="string"></param>    
    ///   <param name="ENDERECOSELECAO" type="ArrayOfEnderecoSelecao"></param>    
    ///   <param name="ERRO" type="boolean"></param>    
    ///   <param name="ERROMENSAGEM" type="string"></param>    
    ///   <param name="ESTRELASAVALIACAO" type="int"></param>    
    ///   <param name="EXIBE" type="string"></param>    
    ///   <param name="EXIBEPASSOS" type="boolean"></param>    
    ///   <param name="FAVORITO" type="boolean"></param>    
    ///   <param name="FAVORITOS" type="string"></param>    
    ///   <param name="FOTOGRDE" type="string"></param>    
    ///   <param name="FOTOPRATO" type="string"></param>    
    ///   <param name="GRUPOS" type="ArrayOfGrupoPrato"></param>    
    ///   <param name="GRUPOSELECIONADO" type="string"></param>    
    ///   <param name="HORAPESQUISA" type="string"></param>    
    ///   <param name="HORARIO" type="string"></param>    
    ///   <param name="HORARIOS" type="ArrayOfHorario"></param>    
    ///   <param name="ICONESMEDIAGERAL" type="string"></param>    
    ///   <param name="LINKINFORESTAURANTE" type="string"></param>    
    ///   <param name="LISTA" type="string"></param>    
    ///   <param name="LITAGENDAMENTO1" type="string"></param>    
    ///   <param name="LITAGENDAMENTO2" type="string"></param>    
    ///   <param name="LOGIN" type="boolean"></param>    
    ///   <param name="LOGORESTAURANTE" type="string"></param>    
    ///   <param name="NOMERESTAURANTE" type="string"></param>    
    ///   <param name="ORIGEM" type="string"></param>    
    ///   <param name="PAGBLANK" type="string"></param>    
    ///   <param name="PAGINA" type="int"></param>    
    ///   <param name="PRATO" type="Prato"></param>    
    ///   <param name="PROMOCAO" type="string"></param>    
    ///   <param name="TEMFOTO" type="boolean"></param>    
    ///   <param name="TEMPOENTREGA" type="string"></param>    
    ///   <param name="TEXTOAVISO" type="string"></param>    
    ///   <param name="TEXTOLIVRE" type="string"></param>    
    ///   <param name="TEXTOPROMO" type="string"></param>    
    ///   <param name="TIPO" type="string"></param>    
    ///   <param name="TIPOSCOZINHA" type="string"></param>    
    ///   <param name="TIPOSPAGTO" type="string"></param>    
    ///   <param name="TITLEAGENDAMENTO" type="string"></param>    
    ///   <param name="TITLEBOTAO1" type="string"></param>    
    ///   <param name="TITLEBOTAO2" type="string"></param>    
    ///   <param name="TITLEPRINCIPAL" type="string"></param>    
    ///   <param name="TOTALAVALIACOES" type="int"></param>    
    ///   <param name="URLRESTAURANTE" type="string"></param>    
    ///   <param name="VALORPEDIDOMIN" type="string"></param>    
    ///   <param name="VERMENOS" type="boolean"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.BAIRRORESTAURANTE = BAIRRORESTAURANTE || null;
    self.BOTAO1 = BOTAO1 || null;
    self.BOTAO2 = BOTAO2 || null;
    self.CBASE = CBASE || null;
    self.CEP = CEP || null;
    self.CIDADERESTAURANTE = CIDADERESTAURANTE || null;
    self.CLASSAGENDAMENTO = CLASSAGENDAMENTO || null;
    self.CODFILIAL = CODFILIAL || null;
    self.CODRESTAURANTE = CODRESTAURANTE || null;
    self.DATAPESQUISA = DATAPESQUISA || null;
    self.ENDERECORESTAURANTE = ENDERECORESTAURANTE || null;
    self.ENDERECOSELECAO = ENDERECOSELECAO || null;
    self.ERRO = ERRO || null;
    self.ERROMENSAGEM = ERROMENSAGEM || null;
    self.ESTRELASAVALIACAO = ESTRELASAVALIACAO || null;
    self.EXIBE = EXIBE || null;
    self.EXIBEPASSOS = EXIBEPASSOS || null;
    self.FAVORITO = FAVORITO || null;
    self.FAVORITOS = FAVORITOS || null;
    self.FOTOGRDE = FOTOGRDE || null;
    self.FOTOPRATO = FOTOPRATO || null;
    self.GRUPOS = GRUPOS || null;
    self.GRUPOSELECIONADO = GRUPOSELECIONADO || null;
    self.HORAPESQUISA = HORAPESQUISA || null;
    self.HORARIO = HORARIO || null;
    self.HORARIOS = HORARIOS || null;
    self.ICONESMEDIAGERAL = ICONESMEDIAGERAL || null;
    self.LINKINFORESTAURANTE = LINKINFORESTAURANTE || null;
    self.LISTA = LISTA || null;
    self.LITAGENDAMENTO1 = LITAGENDAMENTO1 || null;
    self.LITAGENDAMENTO2 = LITAGENDAMENTO2 || null;
    self.LOGIN = LOGIN || null;
    self.LOGORESTAURANTE = LOGORESTAURANTE || null;
    self.NOMERESTAURANTE = NOMERESTAURANTE || null;
    self.ORIGEM = ORIGEM || null;
    self.PAGBLANK = PAGBLANK || null;
    self.PAGINA = PAGINA || null;
    self.PRATO = PRATO || null;
    self.PROMOCAO = PROMOCAO || null;
    self.TEMFOTO = TEMFOTO || null;
    self.TEMPOENTREGA = TEMPOENTREGA || null;
    self.TEXTOAVISO = TEXTOAVISO || null;
    self.TEXTOLIVRE = TEXTOLIVRE || null;
    self.TEXTOPROMO = TEXTOPROMO || null;
    self.TIPO = TIPO || null;
    self.TIPOSCOZINHA = TIPOSCOZINHA || null;
    self.TIPOSPAGTO = TIPOSPAGTO || null;
    self.TITLEAGENDAMENTO = TITLEAGENDAMENTO || null;
    self.TITLEBOTAO1 = TITLEBOTAO1 || null;
    self.TITLEBOTAO2 = TITLEBOTAO2 || null;
    self.TITLEPRINCIPAL = TITLEPRINCIPAL || null;
    self.TOTALAVALIACOES = TOTALAVALIACOES || null;
    self.URLRESTAURANTE = URLRESTAURANTE || null;
    self.VALORPEDIDOMIN = VALORPEDIDOMIN || null;
    self.VERMENOS = VERMENOS || null;
};

MenuDetailSearchModel = function (CodigoPrato, DadosCardapio) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="CodigoPrato" type="int"></param>    
    ///   <param name="DadosCardapio" type="RestaurantMenuSearchModel"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.CodigoPrato = CodigoPrato || null;
    self.DadosCardapio = DadosCardapio || null;
};

MeusPedidos = function (DATAFIM, DATAFINAL, DATAINICIAL, DATAINICIO, DESCRORIGEM, EXIBELAPELA, EXIBELISTA, EXIBEPASSOS, LAPELAORIGEM, LISTAPEDIDOS, PGORIGEM, TITULO, URLREFERER) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="DATAFIM" type="string"></param>    
    ///   <param name="DATAFINAL" type="string"></param>    
    ///   <param name="DATAINICIAL" type="string"></param>    
    ///   <param name="DATAINICIO" type="string"></param>    
    ///   <param name="DESCRORIGEM" type="string"></param>    
    ///   <param name="EXIBELAPELA" type="boolean"></param>    
    ///   <param name="EXIBELISTA" type="boolean"></param>    
    ///   <param name="EXIBEPASSOS" type="boolean"></param>    
    ///   <param name="LAPELAORIGEM" type="string"></param>    
    ///   <param name="LISTAPEDIDOS" type="ArrayOfMeusPedidosPedido"></param>    
    ///   <param name="PGORIGEM" type="string"></param>    
    ///   <param name="TITULO" type="string"></param>    
    ///   <param name="URLREFERER" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.DATAFIM = DATAFIM || null;
    self.DATAFINAL = DATAFINAL || null;
    self.DATAINICIAL = DATAINICIAL || null;
    self.DATAINICIO = DATAINICIO || null;
    self.DESCRORIGEM = DESCRORIGEM || null;
    self.EXIBELAPELA = EXIBELAPELA || null;
    self.EXIBELISTA = EXIBELISTA || null;
    self.EXIBEPASSOS = EXIBEPASSOS || null;
    self.LAPELAORIGEM = LAPELAORIGEM || null;
    self.LISTAPEDIDOS = LISTAPEDIDOS || null;
    self.PGORIGEM = PGORIGEM || null;
    self.TITULO = TITULO || null;
    self.URLREFERER = URLREFERER || null;
};

MeusPedidosPedido = function (BOTAO2LITERAL, BOTAO2TITLE, CLASSSTATUS, DATAHORACONFPEDIDO, DATAPEDIDO, DESCRPAGTOPEDIDO, DataConfPedidoFormatada, DataPedidoFormatada, HORAPEDIDO, ICONEPAGTOPEDIDO, ITENS, ImagemRestauranteFormatada, ImagemStatusPedido, LITSTATUS, LOGORESTAURANTE, MSGPROGRAMADO, NOMERESTAURANTE, NROPEDIDO, PTOSFIDELIDADE, TAXAENTREGA, TELEFONERESTAURANTE, TITLERESTAURANTE, TITLESTATUS, TaxaEntregaFormatado, VLDESCONTO, VLSUBTOTAL, VLTOTALPEDIDO, ValorDescontoFormatado, ValorSubTotalFormatado, ValorTotalFormatado) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="BOTAO2LITERAL" type="string"></param>    
    ///   <param name="BOTAO2TITLE" type="string"></param>    
    ///   <param name="CLASSSTATUS" type="string"></param>    
    ///   <param name="DATAHORACONFPEDIDO" type="string"></param>    
    ///   <param name="DATAPEDIDO" type="string"></param>    
    ///   <param name="DESCRPAGTOPEDIDO" type="string"></param>    
    ///   <param name="DataConfPedidoFormatada" type="string"></param>    
    ///   <param name="DataPedidoFormatada" type="string"></param>    
    ///   <param name="HORAPEDIDO" type="string"></param>    
    ///   <param name="ICONEPAGTOPEDIDO" type="string"></param>    
    ///   <param name="ITENS" type="ArrayOfMeusPedidosPedidoItem"></param>    
    ///   <param name="ImagemRestauranteFormatada" type="string"></param>    
    ///   <param name="ImagemStatusPedido" type="string"></param>    
    ///   <param name="LITSTATUS" type="string"></param>    
    ///   <param name="LOGORESTAURANTE" type="string"></param>    
    ///   <param name="MSGPROGRAMADO" type="string"></param>    
    ///   <param name="NOMERESTAURANTE" type="string"></param>    
    ///   <param name="NROPEDIDO" type="string"></param>    
    ///   <param name="PTOSFIDELIDADE" type="string"></param>    
    ///   <param name="TAXAENTREGA" type="string"></param>    
    ///   <param name="TELEFONERESTAURANTE" type="string"></param>    
    ///   <param name="TITLERESTAURANTE" type="string"></param>    
    ///   <param name="TITLESTATUS" type="string"></param>    
    ///   <param name="TaxaEntregaFormatado" type="string"></param>    
    ///   <param name="VLDESCONTO" type="string"></param>    
    ///   <param name="VLSUBTOTAL" type="string"></param>    
    ///   <param name="VLTOTALPEDIDO" type="string"></param>    
    ///   <param name="ValorDescontoFormatado" type="string"></param>    
    ///   <param name="ValorSubTotalFormatado" type="string"></param>    
    ///   <param name="ValorTotalFormatado" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.BOTAO2LITERAL = BOTAO2LITERAL || null;
    self.BOTAO2TITLE = BOTAO2TITLE || null;
    self.CLASSSTATUS = CLASSSTATUS || null;
    self.DATAHORACONFPEDIDO = DATAHORACONFPEDIDO || null;
    self.DATAPEDIDO = DATAPEDIDO || null;
    self.DESCRPAGTOPEDIDO = DESCRPAGTOPEDIDO || null;
    self.DataConfPedidoFormatada = DataConfPedidoFormatada || null;
    self.DataPedidoFormatada = DataPedidoFormatada || null;
    self.HORAPEDIDO = HORAPEDIDO || null;
    self.ICONEPAGTOPEDIDO = ICONEPAGTOPEDIDO || null;
    self.ITENS = ITENS || null;
    self.ImagemRestauranteFormatada = ImagemRestauranteFormatada || null;
    self.ImagemStatusPedido = ImagemStatusPedido || null;
    self.LITSTATUS = LITSTATUS || null;
    self.LOGORESTAURANTE = LOGORESTAURANTE || null;
    self.MSGPROGRAMADO = MSGPROGRAMADO || null;
    self.NOMERESTAURANTE = NOMERESTAURANTE || null;
    self.NROPEDIDO = NROPEDIDO || null;
    self.PTOSFIDELIDADE = PTOSFIDELIDADE || null;
    self.TAXAENTREGA = TAXAENTREGA || null;
    self.TELEFONERESTAURANTE = TELEFONERESTAURANTE || null;
    self.TITLERESTAURANTE = TITLERESTAURANTE || null;
    self.TITLESTATUS = TITLESTATUS || null;
    self.TaxaEntregaFormatado = TaxaEntregaFormatado || null;
    self.VLDESCONTO = VLDESCONTO || null;
    self.VLSUBTOTAL = VLSUBTOTAL || null;
    self.VLTOTALPEDIDO = VLTOTALPEDIDO || null;
    self.ValorDescontoFormatado = ValorDescontoFormatado || null;
    self.ValorSubTotalFormatado = ValorSubTotalFormatado || null;
    self.ValorTotalFormatado = ValorTotalFormatado || null;
};

MeusPedidosPedidoItem = function (NOMEPRATO, OBSPRATO, QTDEPRATO, TEXTOOPCOES, VALORPRATO, VALORUNITARIO, ValorPratoFormatado, ValorUnitarioFormatado) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="NOMEPRATO" type="string"></param>    
    ///   <param name="OBSPRATO" type="string"></param>    
    ///   <param name="QTDEPRATO" type="string"></param>    
    ///   <param name="TEXTOOPCOES" type="string"></param>    
    ///   <param name="VALORPRATO" type="string"></param>    
    ///   <param name="VALORUNITARIO" type="string"></param>    
    ///   <param name="ValorPratoFormatado" type="string"></param>    
    ///   <param name="ValorUnitarioFormatado" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.NOMEPRATO = NOMEPRATO || null;
    self.OBSPRATO = OBSPRATO || null;
    self.QTDEPRATO = QTDEPRATO || null;
    self.TEXTOOPCOES = TEXTOOPCOES || null;
    self.VALORPRATO = VALORPRATO || null;
    self.VALORUNITARIO = VALORUNITARIO || null;
    self.ValorPratoFormatado = ValorPratoFormatado || null;
    self.ValorUnitarioFormatado = ValorUnitarioFormatado || null;
};

MinhaContaModelData = function (BOTAOCADASTRO, BOTAOENDERECO, BOTAOSENHA, CADASTRO, CLASSCADASTRO, CLASSDADOSPESSOAISLI, CLASSEMAILCADASTROLI, CLASSENDERECO, CLASSENDERECOSSALVOSLI, CLASSINDICARAMIGOSLI, CLASSMEUSPEDIDOSLI, CLASSPREFERENCIASLI, CLASSPROGRAMAFIDELIDADELI, CLASSSENHA, CLASSSENHAACESSOLI, DESCRORIGEM, DIVDADOSPESSOAISSTYLE, DIVEMAILCADASTROSTYLE, DIVENDERECOSSALVOSSTYLE, DIVINDICARAMIGOSSTYLE, DIVMEUSPEDIDOSSTYLE, DIVPREFERENCIASSTYLE, DIVPROGRAMAFIDELIDADESTYLE, DIVSENHAACESSOSTYLE, EMAILNRESTUSUARIONAO, EMAILNRESTUSUARIOSIM, EMAILPROMOUSUARIONAO, EMAILPROMOUSUARIOSIM, ENDERECOS, ENVIAPESQUISANAO, ENVIAPESQUISASIM, ERRO, ERROCPO, ERROFORM, ERROMENS, EXIBELAPELA, EXIBEPASSOS, EXIBEPREFUSUARIONAO, EXIBEPREFUSUARIOSIM, EXIBERESTRESUSUARIONAO, EXIBERESTRESUSUARIOSIM, INDENDERECOEDICAO, LAPELAORIGEM, LEMBRETE, LISTACOZINHA, NOVOEND, PGORIGEM, PONTOSFIDELIDADE, URLREFERER) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="BOTAOCADASTRO" type="string"></param>    
    ///   <param name="BOTAOENDERECO" type="string"></param>    
    ///   <param name="BOTAOSENHA" type="string"></param>    
    ///   <param name="CADASTRO" type="MinhaContaModelDataCadastro"></param>    
    ///   <param name="CLASSCADASTRO" type="string"></param>    
    ///   <param name="CLASSDADOSPESSOAISLI" type="string"></param>    
    ///   <param name="CLASSEMAILCADASTROLI" type="string"></param>    
    ///   <param name="CLASSENDERECO" type="string"></param>    
    ///   <param name="CLASSENDERECOSSALVOSLI" type="string"></param>    
    ///   <param name="CLASSINDICARAMIGOSLI" type="string"></param>    
    ///   <param name="CLASSMEUSPEDIDOSLI" type="string"></param>    
    ///   <param name="CLASSPREFERENCIASLI" type="string"></param>    
    ///   <param name="CLASSPROGRAMAFIDELIDADELI" type="string"></param>    
    ///   <param name="CLASSSENHA" type="string"></param>    
    ///   <param name="CLASSSENHAACESSOLI" type="string"></param>    
    ///   <param name="DESCRORIGEM" type="string"></param>    
    ///   <param name="DIVDADOSPESSOAISSTYLE" type="string"></param>    
    ///   <param name="DIVEMAILCADASTROSTYLE" type="string"></param>    
    ///   <param name="DIVENDERECOSSALVOSSTYLE" type="string"></param>    
    ///   <param name="DIVINDICARAMIGOSSTYLE" type="string"></param>    
    ///   <param name="DIVMEUSPEDIDOSSTYLE" type="string"></param>    
    ///   <param name="DIVPREFERENCIASSTYLE" type="string"></param>    
    ///   <param name="DIVPROGRAMAFIDELIDADESTYLE" type="string"></param>    
    ///   <param name="DIVSENHAACESSOSTYLE" type="string"></param>    
    ///   <param name="EMAILNRESTUSUARIONAO" type="string"></param>    
    ///   <param name="EMAILNRESTUSUARIOSIM" type="string"></param>    
    ///   <param name="EMAILPROMOUSUARIONAO" type="string"></param>    
    ///   <param name="EMAILPROMOUSUARIOSIM" type="string"></param>    
    ///   <param name="ENDERECOS" type="ArrayOfMinhaContaModelDataEndereco"></param>    
    ///   <param name="ENVIAPESQUISANAO" type="string"></param>    
    ///   <param name="ENVIAPESQUISASIM" type="string"></param>    
    ///   <param name="ERRO" type="string"></param>    
    ///   <param name="ERROCPO" type="string"></param>    
    ///   <param name="ERROFORM" type="string"></param>    
    ///   <param name="ERROMENS" type="string"></param>    
    ///   <param name="EXIBELAPELA" type="boolean"></param>    
    ///   <param name="EXIBEPASSOS" type="boolean"></param>    
    ///   <param name="EXIBEPREFUSUARIONAO" type="string"></param>    
    ///   <param name="EXIBEPREFUSUARIOSIM" type="string"></param>    
    ///   <param name="EXIBERESTRESUSUARIONAO" type="string"></param>    
    ///   <param name="EXIBERESTRESUSUARIOSIM" type="string"></param>    
    ///   <param name="INDENDERECOEDICAO" type="int"></param>    
    ///   <param name="LAPELAORIGEM" type="string"></param>    
    ///   <param name="LEMBRETE" type="string"></param>    
    ///   <param name="LISTACOZINHA" type="ArrayOfMinhaContaModelDataCheck"></param>    
    ///   <param name="NOVOEND" type="MyAccountNewAddressModel"></param>    
    ///   <param name="PGORIGEM" type="string"></param>    
    ///   <param name="PONTOSFIDELIDADE" type="string"></param>    
    ///   <param name="URLREFERER" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.BOTAOCADASTRO = BOTAOCADASTRO || null;
    self.BOTAOENDERECO = BOTAOENDERECO || null;
    self.BOTAOSENHA = BOTAOSENHA || null;
    self.CADASTRO = CADASTRO || null;
    self.CLASSCADASTRO = CLASSCADASTRO || null;
    self.CLASSDADOSPESSOAISLI = CLASSDADOSPESSOAISLI || null;
    self.CLASSEMAILCADASTROLI = CLASSEMAILCADASTROLI || null;
    self.CLASSENDERECO = CLASSENDERECO || null;
    self.CLASSENDERECOSSALVOSLI = CLASSENDERECOSSALVOSLI || null;
    self.CLASSINDICARAMIGOSLI = CLASSINDICARAMIGOSLI || null;
    self.CLASSMEUSPEDIDOSLI = CLASSMEUSPEDIDOSLI || null;
    self.CLASSPREFERENCIASLI = CLASSPREFERENCIASLI || null;
    self.CLASSPROGRAMAFIDELIDADELI = CLASSPROGRAMAFIDELIDADELI || null;
    self.CLASSSENHA = CLASSSENHA || null;
    self.CLASSSENHAACESSOLI = CLASSSENHAACESSOLI || null;
    self.DESCRORIGEM = DESCRORIGEM || null;
    self.DIVDADOSPESSOAISSTYLE = DIVDADOSPESSOAISSTYLE || null;
    self.DIVEMAILCADASTROSTYLE = DIVEMAILCADASTROSTYLE || null;
    self.DIVENDERECOSSALVOSSTYLE = DIVENDERECOSSALVOSSTYLE || null;
    self.DIVINDICARAMIGOSSTYLE = DIVINDICARAMIGOSSTYLE || null;
    self.DIVMEUSPEDIDOSSTYLE = DIVMEUSPEDIDOSSTYLE || null;
    self.DIVPREFERENCIASSTYLE = DIVPREFERENCIASSTYLE || null;
    self.DIVPROGRAMAFIDELIDADESTYLE = DIVPROGRAMAFIDELIDADESTYLE || null;
    self.DIVSENHAACESSOSTYLE = DIVSENHAACESSOSTYLE || null;
    self.EMAILNRESTUSUARIONAO = EMAILNRESTUSUARIONAO || null;
    self.EMAILNRESTUSUARIOSIM = EMAILNRESTUSUARIOSIM || null;
    self.EMAILPROMOUSUARIONAO = EMAILPROMOUSUARIONAO || null;
    self.EMAILPROMOUSUARIOSIM = EMAILPROMOUSUARIOSIM || null;
    self.ENDERECOS = ENDERECOS || null;
    self.ENVIAPESQUISANAO = ENVIAPESQUISANAO || null;
    self.ENVIAPESQUISASIM = ENVIAPESQUISASIM || null;
    self.ERRO = ERRO || null;
    self.ERROCPO = ERROCPO || null;
    self.ERROFORM = ERROFORM || null;
    self.ERROMENS = ERROMENS || null;
    self.EXIBELAPELA = EXIBELAPELA || null;
    self.EXIBEPASSOS = EXIBEPASSOS || null;
    self.EXIBEPREFUSUARIONAO = EXIBEPREFUSUARIONAO || null;
    self.EXIBEPREFUSUARIOSIM = EXIBEPREFUSUARIOSIM || null;
    self.EXIBERESTRESUSUARIONAO = EXIBERESTRESUSUARIONAO || null;
    self.EXIBERESTRESUSUARIOSIM = EXIBERESTRESUSUARIOSIM || null;
    self.INDENDERECOEDICAO = INDENDERECOEDICAO || null;
    self.LAPELAORIGEM = LAPELAORIGEM || null;
    self.LEMBRETE = LEMBRETE || null;
    self.LISTACOZINHA = LISTACOZINHA || null;
    self.NOVOEND = NOVOEND || null;
    self.PGORIGEM = PGORIGEM || null;
    self.PONTOSFIDELIDADE = PONTOSFIDELIDADE || null;
    self.URLREFERER = URLREFERER || null;
};

MinhaContaModelDataCadastro = function (ANOS, COD, CPF, DIAS, DTNASCAA, DTNASCDD, DTNASCMM, EMAIL, MESES, NOME, SELECTEDF, SELECTEDM, SELTIMES, TIME, TIMES) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="ANOS" type="ArrayOfMinhaContaModelDataSelect"></param>    
    ///   <param name="COD" type="string"></param>    
    ///   <param name="CPF" type="string"></param>    
    ///   <param name="DIAS" type="ArrayOfMinhaContaModelDataSelect"></param>    
    ///   <param name="DTNASCAA" type="string"></param>    
    ///   <param name="DTNASCDD" type="string"></param>    
    ///   <param name="DTNASCMM" type="string"></param>    
    ///   <param name="EMAIL" type="string"></param>    
    ///   <param name="MESES" type="ArrayOfMinhaContaModelDataSelect"></param>    
    ///   <param name="NOME" type="string"></param>    
    ///   <param name="SELECTEDF" type="string"></param>    
    ///   <param name="SELECTEDM" type="string"></param>    
    ///   <param name="SELTIMES" type="ArrayOfstring"></param>    
    ///   <param name="TIME" type="string"></param>    
    ///   <param name="TIMES" type="ArrayOfstring"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.ANOS = ANOS || null;
    self.COD = COD || null;
    self.CPF = CPF || null;
    self.DIAS = DIAS || null;
    self.DTNASCAA = DTNASCAA || null;
    self.DTNASCDD = DTNASCDD || null;
    self.DTNASCMM = DTNASCMM || null;
    self.EMAIL = EMAIL || null;
    self.MESES = MESES || null;
    self.NOME = NOME || null;
    self.SELECTEDF = SELECTEDF || null;
    self.SELECTEDM = SELECTEDM || null;
    self.SELTIMES = SELTIMES || null;
    self.TIME = TIME || null;
    self.TIMES = TIMES || null;
};

MinhaContaModelDataCheck = function (CHECKED, COD, NOME) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="CHECKED" type="string"></param>    
    ///   <param name="COD" type="string"></param>    
    ///   <param name="NOME" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.CHECKED = CHECKED || null;
    self.COD = COD || null;
    self.NOME = NOME || null;
};

MinhaContaModelDataEndereco = function (BAIRRO, CEP, CIDADE, COMPLEMENTO, DDD1, DDD2, ENDERECO, ESTADO, FAVORITO, NUMERO, RADIO, TELEFONE1, TELEFONE2) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="BAIRRO" type="string"></param>    
    ///   <param name="CEP" type="string"></param>    
    ///   <param name="CIDADE" type="string"></param>    
    ///   <param name="COMPLEMENTO" type="string"></param>    
    ///   <param name="DDD1" type="string"></param>    
    ///   <param name="DDD2" type="string"></param>    
    ///   <param name="ENDERECO" type="string"></param>    
    ///   <param name="ESTADO" type="string"></param>    
    ///   <param name="FAVORITO" type="string"></param>    
    ///   <param name="NUMERO" type="string"></param>    
    ///   <param name="RADIO" type="string"></param>    
    ///   <param name="TELEFONE1" type="string"></param>    
    ///   <param name="TELEFONE2" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.BAIRRO = BAIRRO || null;
    self.CEP = CEP || null;
    self.CIDADE = CIDADE || null;
    self.COMPLEMENTO = COMPLEMENTO || null;
    self.DDD1 = DDD1 || null;
    self.DDD2 = DDD2 || null;
    self.ENDERECO = ENDERECO || null;
    self.ESTADO = ESTADO || null;
    self.FAVORITO = FAVORITO || null;
    self.NUMERO = NUMERO || null;
    self.RADIO = RADIO || null;
    self.TELEFONE1 = TELEFONE1 || null;
    self.TELEFONE2 = TELEFONE2 || null;
};

MinhaContaModelDataSelect = function (SELECTED, TEXT, VALUE) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="SELECTED" type="string"></param>    
    ///   <param name="TEXT" type="string"></param>    
    ///   <param name="VALUE" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.SELECTED = SELECTED || null;
    self.TEXT = TEXT || null;
    self.VALUE = VALUE || null;
};

MyAccountModel = function (MeusPedidos, MinhaConta) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="MeusPedidos" type="MeusPedidos"></param>    
    ///   <param name="MinhaConta" type="MinhaContaModelData"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.MeusPedidos = MeusPedidos || null;
    self.MinhaConta = MinhaConta || null;
};

MyAccountNewAddressModel = function (BAIRRO, CEP, CIDADE, COMPLEMENTO, DDD1, DDD2, ENDERECO, ESTADO, EnderecoSelecionado, INCLUIRENDERECO, NUMERO, RADIO, STATUS, TELEFONE1, TELEFONE2) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="BAIRRO" type="string"></param>    
    ///   <param name="CEP" type="string"></param>    
    ///   <param name="CIDADE" type="string"></param>    
    ///   <param name="COMPLEMENTO" type="string"></param>    
    ///   <param name="DDD1" type="string"></param>    
    ///   <param name="DDD2" type="string"></param>    
    ///   <param name="ENDERECO" type="string"></param>    
    ///   <param name="ESTADO" type="string"></param>    
    ///   <param name="EnderecoSelecionado" type="int"></param>    
    ///   <param name="INCLUIRENDERECO" type="boolean"></param>    
    ///   <param name="NUMERO" type="string"></param>    
    ///   <param name="RADIO" type="string"></param>    
    ///   <param name="STATUS" type="string"></param>    
    ///   <param name="TELEFONE1" type="string"></param>    
    ///   <param name="TELEFONE2" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.BAIRRO = BAIRRO || null;
    self.CEP = CEP || null;
    self.CIDADE = CIDADE || null;
    self.COMPLEMENTO = COMPLEMENTO || null;
    self.DDD1 = DDD1 || null;
    self.DDD2 = DDD2 || null;
    self.ENDERECO = ENDERECO || null;
    self.ESTADO = ESTADO || null;
    self.EnderecoSelecionado = EnderecoSelecionado || null;
    self.INCLUIRENDERECO = INCLUIRENDERECO || null;
    self.NUMERO = NUMERO || null;
    self.RADIO = RADIO || null;
    self.STATUS = STATUS || null;
    self.TELEFONE1 = TELEFONE1 || null;
    self.TELEFONE2 = TELEFONE2 || null;
};

MyAccountSaveAddressModel = function (Address, CEP, City, Complement, DDD1, DDD2, Neighborhood, Number, Phone1, Phone2, State) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="Address" type="string"></param>    
    ///   <param name="CEP" type="string"></param>    
    ///   <param name="City" type="string"></param>    
    ///   <param name="Complement" type="string"></param>    
    ///   <param name="DDD1" type="string"></param>    
    ///   <param name="DDD2" type="string"></param>    
    ///   <param name="Neighborhood" type="string"></param>    
    ///   <param name="Number" type="string"></param>    
    ///   <param name="Phone1" type="string"></param>    
    ///   <param name="Phone2" type="string"></param>    
    ///   <param name="State" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.Address = Address || null;
    self.CEP = CEP || null;
    self.City = City || null;
    self.Complement = Complement || null;
    self.DDD1 = DDD1 || null;
    self.DDD2 = DDD2 || null;
    self.Neighborhood = Neighborhood || null;
    self.Number = Number || null;
    self.Phone1 = Phone1 || null;
    self.Phone2 = Phone2 || null;
    self.State = State || null;
};

OpcaoAssociada = function (Value, Value, Value, Value) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="Value" type="string"></param>    
    ///   <param name="Value" type="string"></param>    
    ///   <param name="Value" type="string"></param>    
    ///   <param name="Value" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.Value = Value || null;
    self.Value = Value || null;
    self.Value = Value || null;
    self.Value = Value || null;
};

OpcaoItemPreco = function (Value, Value, Value, Value) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="Value" type="string"></param>    
    ///   <param name="Value" type="string"></param>    
    ///   <param name="Value" type="string"></param>    
    ///   <param name="Value" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.Value = Value || null;
    self.Value = Value || null;
    self.Value = Value || null;
    self.Value = Value || null;
};

OperationResponseModel = function (ResponseMessage, Success) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="ResponseMessage" type="string"></param>    
    ///   <param name="Success" type="boolean"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.ResponseMessage = ResponseMessage || null;
    self.Success = Success || null;
};

OrderConfirmationMessage = function (Value, Value, Value, Value) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="Value" type="string"></param>    
    ///   <param name="Value" type="string"></param>    
    ///   <param name="Value" type="string"></param>    
    ///   <param name="Value" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.Value = Value || null;
    self.Value = Value || null;
    self.Value = Value || null;
    self.Value = Value || null;
};

OrderCorfirmationModel = function (ANTECEDENCIA, DADOSCONFIRMACAO, DATAPEDIDO, DESTAQUE, ERRO, HORAPEDIDO, Imediato, LITENVIO, MENSAGENS, MensagensFormatadas, NOMECLIENTE, NOMERESTAURANTE, NOMESTATUS, NUMEROPEDIDO, PTOSCOMPRA, PTOSTOTAL, STATUS, TAXAENTRPEDANALITICS, TELEFONERESTAURANTE, TEMPOESTIMADO, TextoEnvio, TextoPedido, TextoStatus, VALORPEDIDO) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="ANTECEDENCIA" type="string"></param>    
    ///   <param name="DADOSCONFIRMACAO" type="string"></param>    
    ///   <param name="DATAPEDIDO" type="string"></param>    
    ///   <param name="DESTAQUE" type="string"></param>    
    ///   <param name="ERRO" type="string"></param>    
    ///   <param name="HORAPEDIDO" type="string"></param>    
    ///   <param name="Imediato" type="boolean"></param>    
    ///   <param name="LITENVIO" type="string"></param>    
    ///   <param name="MENSAGENS" type="ArrayOfstring"></param>    
    ///   <param name="MensagensFormatadas" type="ArrayOfOrderConfirmationMessage"></param>    
    ///   <param name="NOMECLIENTE" type="string"></param>    
    ///   <param name="NOMERESTAURANTE" type="string"></param>    
    ///   <param name="NOMESTATUS" type="string"></param>    
    ///   <param name="NUMEROPEDIDO" type="int"></param>    
    ///   <param name="PTOSCOMPRA" type="string"></param>    
    ///   <param name="PTOSTOTAL" type="string"></param>    
    ///   <param name="STATUS" type="string"></param>    
    ///   <param name="TAXAENTRPEDANALITICS" type="string"></param>    
    ///   <param name="TELEFONERESTAURANTE" type="string"></param>    
    ///   <param name="TEMPOESTIMADO" type="string"></param>    
    ///   <param name="TextoEnvio" type="string"></param>    
    ///   <param name="TextoPedido" type="string"></param>    
    ///   <param name="TextoStatus" type="string"></param>    
    ///   <param name="VALORPEDIDO" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.ANTECEDENCIA = ANTECEDENCIA || null;
    self.DADOSCONFIRMACAO = DADOSCONFIRMACAO || null;
    self.DATAPEDIDO = DATAPEDIDO || null;
    self.DESTAQUE = DESTAQUE || null;
    self.ERRO = ERRO || null;
    self.HORAPEDIDO = HORAPEDIDO || null;
    self.Imediato = Imediato || null;
    self.LITENVIO = LITENVIO || null;
    self.MENSAGENS = MENSAGENS || null;
    self.MensagensFormatadas = MensagensFormatadas || null;
    self.NOMECLIENTE = NOMECLIENTE || null;
    self.NOMERESTAURANTE = NOMERESTAURANTE || null;
    self.NOMESTATUS = NOMESTATUS || null;
    self.NUMEROPEDIDO = NUMEROPEDIDO || null;
    self.PTOSCOMPRA = PTOSCOMPRA || null;
    self.PTOSTOTAL = PTOSTOTAL || null;
    self.STATUS = STATUS || null;
    self.TAXAENTRPEDANALITICS = TAXAENTRPEDANALITICS || null;
    self.TELEFONERESTAURANTE = TELEFONERESTAURANTE || null;
    self.TEMPOESTIMADO = TEMPOESTIMADO || null;
    self.TextoEnvio = TextoEnvio || null;
    self.TextoPedido = TextoPedido || null;
    self.TextoStatus = TextoStatus || null;
    self.VALORPEDIDO = VALORPEDIDO || null;
};

OrderEndData = function (BAIRRO, CEP, CIDADE, COMPLEMENTO, CPF, CUPOM, DATA, DDD1, DDD2, ENDERECO, ESTADO, HORA, MEIOPAGAMENTO, NUMERO, OBSERVACOES, OPERACAO, TELEFONE1, TELEFONE2, TIPO, VALORPAGO, VALORPEDIDO) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="BAIRRO" type="string"></param>    
    ///   <param name="CEP" type="string"></param>    
    ///   <param name="CIDADE" type="string"></param>    
    ///   <param name="COMPLEMENTO" type="string"></param>    
    ///   <param name="CPF" type="string"></param>    
    ///   <param name="CUPOM" type="string"></param>    
    ///   <param name="DATA" type="string"></param>    
    ///   <param name="DDD1" type="string"></param>    
    ///   <param name="DDD2" type="string"></param>    
    ///   <param name="ENDERECO" type="string"></param>    
    ///   <param name="ESTADO" type="string"></param>    
    ///   <param name="HORA" type="string"></param>    
    ///   <param name="MEIOPAGAMENTO" type="string"></param>    
    ///   <param name="NUMERO" type="string"></param>    
    ///   <param name="OBSERVACOES" type="string"></param>    
    ///   <param name="OPERACAO" type="string"></param>    
    ///   <param name="TELEFONE1" type="string"></param>    
    ///   <param name="TELEFONE2" type="string"></param>    
    ///   <param name="TIPO" type="string"></param>    
    ///   <param name="VALORPAGO" type="string"></param>    
    ///   <param name="VALORPEDIDO" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.BAIRRO = BAIRRO || null;
    self.CEP = CEP || null;
    self.CIDADE = CIDADE || null;
    self.COMPLEMENTO = COMPLEMENTO || null;
    self.CPF = CPF || null;
    self.CUPOM = CUPOM || null;
    self.DATA = DATA || null;
    self.DDD1 = DDD1 || null;
    self.DDD2 = DDD2 || null;
    self.ENDERECO = ENDERECO || null;
    self.ESTADO = ESTADO || null;
    self.HORA = HORA || null;
    self.MEIOPAGAMENTO = MEIOPAGAMENTO || null;
    self.NUMERO = NUMERO || null;
    self.OBSERVACOES = OBSERVACOES || null;
    self.OPERACAO = OPERACAO || null;
    self.TELEFONE1 = TELEFONE1 || null;
    self.TELEFONE2 = TELEFONE2 || null;
    self.TIPO = TIPO || null;
    self.VALORPAGO = VALORPAGO || null;
    self.VALORPEDIDO = VALORPEDIDO || null;
};

OrderEndModel = function (AGENDAMENTO, ATENDECEP, BAIRRORESTAURANTE, CEP, CODFILIAL, CODIGOPEDIDO, CODRESTAURANTE, DADOSENTREGA, DADOSPAGTO, ENDERECORESTAURANTE, ERRO, ERROMENSAGEM, ITENS, ImagemFormatada, LOGORESTAURANTE, NOMERESTAURANTE, PEDMINIMO, PodePedirNotaPaulista, TEMPOENTREGARESTAURANTE, TEXTOAVISO, UFRESTAURANTE, URL, VLDESCONTO, VLSUBTOTAL, VLTAXA, VLTOTAL, VOUCHER) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="AGENDAMENTO" type="FinalizaPedidoRestauranteModelAgendamento"></param>    
    ///   <param name="ATENDECEP" type="FinalizaPedidoRestauranteModelAtendeCEP"></param>    
    ///   <param name="BAIRRORESTAURANTE" type="string"></param>    
    ///   <param name="CEP" type="string"></param>    
    ///   <param name="CODFILIAL" type="short"></param>    
    ///   <param name="CODIGOPEDIDO" type="int"></param>    
    ///   <param name="CODRESTAURANTE" type="short"></param>    
    ///   <param name="DADOSENTREGA" type="FinalizaPedidoRestauranteModelDadosEntrega"></param>    
    ///   <param name="DADOSPAGTO" type="FinalizaPedidoRestauranteModelDadosPagamento"></param>    
    ///   <param name="ENDERECORESTAURANTE" type="string"></param>    
    ///   <param name="ERRO" type="boolean"></param>    
    ///   <param name="ERROMENSAGEM" type="string"></param>    
    ///   <param name="ITENS" type="ArrayOfFinalizaPedidoRestauranteModelPrato"></param>    
    ///   <param name="ImagemFormatada" type="string"></param>    
    ///   <param name="LOGORESTAURANTE" type="string"></param>    
    ///   <param name="NOMERESTAURANTE" type="string"></param>    
    ///   <param name="PEDMINIMO" type="decimal"></param>    
    ///   <param name="PodePedirNotaPaulista" type="boolean"></param>    
    ///   <param name="TEMPOENTREGARESTAURANTE" type="string"></param>    
    ///   <param name="TEXTOAVISO" type="string"></param>    
    ///   <param name="UFRESTAURANTE" type="string"></param>    
    ///   <param name="URL" type="string"></param>    
    ///   <param name="VLDESCONTO" type="string"></param>    
    ///   <param name="VLSUBTOTAL" type="string"></param>    
    ///   <param name="VLTAXA" type="string"></param>    
    ///   <param name="VLTOTAL" type="string"></param>    
    ///   <param name="VOUCHER" type="FinalizaPedidoRestauranteModelVoucher"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.AGENDAMENTO = AGENDAMENTO || null;
    self.ATENDECEP = ATENDECEP || null;
    self.BAIRRORESTAURANTE = BAIRRORESTAURANTE || null;
    self.CEP = CEP || null;
    self.CODFILIAL = CODFILIAL || null;
    self.CODIGOPEDIDO = CODIGOPEDIDO || null;
    self.CODRESTAURANTE = CODRESTAURANTE || null;
    self.DADOSENTREGA = DADOSENTREGA || null;
    self.DADOSPAGTO = DADOSPAGTO || null;
    self.ENDERECORESTAURANTE = ENDERECORESTAURANTE || null;
    self.ERRO = ERRO || null;
    self.ERROMENSAGEM = ERROMENSAGEM || null;
    self.ITENS = ITENS || null;
    self.ImagemFormatada = ImagemFormatada || null;
    self.LOGORESTAURANTE = LOGORESTAURANTE || null;
    self.NOMERESTAURANTE = NOMERESTAURANTE || null;
    self.PEDMINIMO = PEDMINIMO || null;
    self.PodePedirNotaPaulista = PodePedirNotaPaulista || null;
    self.TEMPOENTREGARESTAURANTE = TEMPOENTREGARESTAURANTE || null;
    self.TEXTOAVISO = TEXTOAVISO || null;
    self.UFRESTAURANTE = UFRESTAURANTE || null;
    self.URL = URL || null;
    self.VLDESCONTO = VLDESCONTO || null;
    self.VLSUBTOTAL = VLSUBTOTAL || null;
    self.VLTAXA = VLTAXA || null;
    self.VLTOTAL = VLTOTAL || null;
    self.VOUCHER = VOUCHER || null;
};

OrderItemModel = function (CODIGO, NOME, OBSERVACOES, QUANTIDADE, VALORTOTAL, VALORUNITARIO) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="CODIGO" type="int"></param>    
    ///   <param name="NOME" type="string"></param>    
    ///   <param name="OBSERVACOES" type="string"></param>    
    ///   <param name="QUANTIDADE" type="int"></param>    
    ///   <param name="VALORTOTAL" type="decimal"></param>    
    ///   <param name="VALORUNITARIO" type="decimal"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.CODIGO = CODIGO || null;
    self.NOME = NOME || null;
    self.OBSERVACOES = OBSERVACOES || null;
    self.QUANTIDADE = QUANTIDADE || null;
    self.VALORTOTAL = VALORTOTAL || null;
    self.VALORUNITARIO = VALORUNITARIO || null;
};

OrderModel = function (DESCONTO, ITENS, PORCENTAGEMDESCONTO, SUBTOTAL, TAXAENTREGA, TOTAL, VALORMINIMO) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="DESCONTO" type="decimal"></param>    
    ///   <param name="ITENS" type="ArrayOfOrderItemModel"></param>    
    ///   <param name="PORCENTAGEMDESCONTO" type="decimal"></param>    
    ///   <param name="SUBTOTAL" type="decimal"></param>    
    ///   <param name="TAXAENTREGA" type="decimal"></param>    
    ///   <param name="TOTAL" type="decimal"></param>    
    ///   <param name="VALORMINIMO" type="decimal"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.DESCONTO = DESCONTO || null;
    self.ITENS = ITENS || null;
    self.PORCENTAGEMDESCONTO = PORCENTAGEMDESCONTO || null;
    self.SUBTOTAL = SUBTOTAL || null;
    self.TAXAENTREGA = TAXAENTREGA || null;
    self.TOTAL = TOTAL || null;
    self.VALORMINIMO = VALORMINIMO || null;
};

Prato = function (CODINTEGRACAOPRATO, CODPRATOINTEGOPCAO, DESCRDETPRATO, DESCRICAOPRATO, ERROCONSISTENCIA, ERROOPCAO, EXIBEVALOR, FOTOPRATO, FRMOBS, FRMQTDE, GRUPOPRATO, INDICAPIZZAMEIO, INDICAPIZZATAM, ITEMATIVOOPCAO, ITEMDETOPCAO, LISTAOPCOES, OPCAOATIVA, OPCAOPRECO, OPCOES, OPCOESPIZZA, PIZZAMEIO, QTDEDETOPCAO, QTDEMAXOPCAO, QTDEMINOPCAO, QTDEOPCOESPRATO, RADIOOPCAO, SITUACAOPRATO, TEMOPCOES, TEXTODETALHEOPCAO, TEXTODETOPCAO, TEXTOOPCAO, TIPOOPCAO, TIPOPRATO, VALORPRATO, VLADICDETOPCAO, VLADICDETOPCAOPRECO, VLEDITADO) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="CODINTEGRACAOPRATO" type="string"></param>    
    ///   <param name="CODPRATOINTEGOPCAO" type="string"></param>    
    ///   <param name="DESCRDETPRATO" type="string"></param>    
    ///   <param name="DESCRICAOPRATO" type="string"></param>    
    ///   <param name="ERROCONSISTENCIA" type="string"></param>    
    ///   <param name="ERROOPCAO" type="string"></param>    
    ///   <param name="EXIBEVALOR" type="boolean"></param>    
    ///   <param name="FOTOPRATO" type="string"></param>    
    ///   <param name="FRMOBS" type="string"></param>    
    ///   <param name="FRMQTDE" type="int"></param>    
    ///   <param name="GRUPOPRATO" type="string"></param>    
    ///   <param name="INDICAPIZZAMEIO" type="string"></param>    
    ///   <param name="INDICAPIZZATAM" type="string"></param>    
    ///   <param name="ITEMATIVOOPCAO" type="string"></param>    
    ///   <param name="ITEMDETOPCAO" type="string"></param>    
    ///   <param name="LISTAOPCOES" type="string"></param>    
    ///   <param name="OPCAOATIVA" type="string"></param>    
    ///   <param name="OPCAOPRECO" type="string"></param>    
    ///   <param name="OPCOES" type="ArrayOfPratoOpcoes"></param>    
    ///   <param name="OPCOESPIZZA" type="ArrayOfPratoOpcoesPizza"></param>    
    ///   <param name="PIZZAMEIO" type="boolean"></param>    
    ///   <param name="QTDEDETOPCAO" type="string"></param>    
    ///   <param name="QTDEMAXOPCAO" type="string"></param>    
    ///   <param name="QTDEMINOPCAO" type="string"></param>    
    ///   <param name="QTDEOPCOESPRATO" type="string"></param>    
    ///   <param name="RADIOOPCAO" type="string"></param>    
    ///   <param name="SITUACAOPRATO" type="string"></param>    
    ///   <param name="TEMOPCOES" type="boolean"></param>    
    ///   <param name="TEXTODETALHEOPCAO" type="string"></param>    
    ///   <param name="TEXTODETOPCAO" type="string"></param>    
    ///   <param name="TEXTOOPCAO" type="string"></param>    
    ///   <param name="TIPOOPCAO" type="string"></param>    
    ///   <param name="TIPOPRATO" type="string"></param>    
    ///   <param name="VALORPRATO" type="string"></param>    
    ///   <param name="VLADICDETOPCAO" type="string"></param>    
    ///   <param name="VLADICDETOPCAOPRECO" type="string"></param>    
    ///   <param name="VLEDITADO" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.CODINTEGRACAOPRATO = CODINTEGRACAOPRATO || null;
    self.CODPRATOINTEGOPCAO = CODPRATOINTEGOPCAO || null;
    self.DESCRDETPRATO = DESCRDETPRATO || null;
    self.DESCRICAOPRATO = DESCRICAOPRATO || null;
    self.ERROCONSISTENCIA = ERROCONSISTENCIA || null;
    self.ERROOPCAO = ERROOPCAO || null;
    self.EXIBEVALOR = EXIBEVALOR || null;
    self.FOTOPRATO = FOTOPRATO || null;
    self.FRMOBS = FRMOBS || null;
    self.FRMQTDE = FRMQTDE || null;
    self.GRUPOPRATO = GRUPOPRATO || null;
    self.INDICAPIZZAMEIO = INDICAPIZZAMEIO || null;
    self.INDICAPIZZATAM = INDICAPIZZATAM || null;
    self.ITEMATIVOOPCAO = ITEMATIVOOPCAO || null;
    self.ITEMDETOPCAO = ITEMDETOPCAO || null;
    self.LISTAOPCOES = LISTAOPCOES || null;
    self.OPCAOATIVA = OPCAOATIVA || null;
    self.OPCAOPRECO = OPCAOPRECO || null;
    self.OPCOES = OPCOES || null;
    self.OPCOESPIZZA = OPCOESPIZZA || null;
    self.PIZZAMEIO = PIZZAMEIO || null;
    self.QTDEDETOPCAO = QTDEDETOPCAO || null;
    self.QTDEMAXOPCAO = QTDEMAXOPCAO || null;
    self.QTDEMINOPCAO = QTDEMINOPCAO || null;
    self.QTDEOPCOESPRATO = QTDEOPCOESPRATO || null;
    self.RADIOOPCAO = RADIOOPCAO || null;
    self.SITUACAOPRATO = SITUACAOPRATO || null;
    self.TEMOPCOES = TEMOPCOES || null;
    self.TEXTODETALHEOPCAO = TEXTODETALHEOPCAO || null;
    self.TEXTODETOPCAO = TEXTODETOPCAO || null;
    self.TEXTOOPCAO = TEXTOOPCAO || null;
    self.TIPOOPCAO = TIPOOPCAO || null;
    self.TIPOPRATO = TIPOPRATO || null;
    self.VALORPRATO = VALORPRATO || null;
    self.VLADICDETOPCAO = VLADICDETOPCAO || null;
    self.VLADICDETOPCAOPRECO = VLADICDETOPCAOPRECO || null;
    self.VLEDITADO = VLEDITADO || null;
};

PratoOpcoes = function (CLASSPRECO, ITENS, TEMPRECO, TITULO) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="CLASSPRECO" type="string"></param>    
    ///   <param name="ITENS" type="ArrayOfPratoOpcoesItem"></param>    
    ///   <param name="TEMPRECO" type="boolean"></param>    
    ///   <param name="TITULO" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.CLASSPRECO = CLASSPRECO || null;
    self.ITENS = ITENS || null;
    self.TEMPRECO = TEMPRECO || null;
    self.TITULO = TITULO || null;
};

PratoOpcoesItem = function (CHECK, NOMEVARIAVEL, TEXTOVALOR, TIPOVARIAVEL, TITULODETALHE, TITULOVARIAVEL, VALOR, VALORVARIAVEL, VALUEVARIAVEL) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="CHECK" type="string"></param>    
    ///   <param name="NOMEVARIAVEL" type="string"></param>    
    ///   <param name="TEXTOVALOR" type="string"></param>    
    ///   <param name="TIPOVARIAVEL" type="string"></param>    
    ///   <param name="TITULODETALHE" type="string"></param>    
    ///   <param name="TITULOVARIAVEL" type="string"></param>    
    ///   <param name="VALOR" type="string"></param>    
    ///   <param name="VALORVARIAVEL" type="string"></param>    
    ///   <param name="VALUEVARIAVEL" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.CHECK = CHECK || null;
    self.NOMEVARIAVEL = NOMEVARIAVEL || null;
    self.TEXTOVALOR = TEXTOVALOR || null;
    self.TIPOVARIAVEL = TIPOVARIAVEL || null;
    self.TITULODETALHE = TITULODETALHE || null;
    self.TITULOVARIAVEL = TITULOVARIAVEL || null;
    self.VALOR = VALOR || null;
    self.VALORVARIAVEL = VALORVARIAVEL || null;
    self.VALUEVARIAVEL = VALUEVARIAVEL || null;
};

PratoOpcoesPizza = function (CODIGO, DESCRICAO, DESCRICAODET, SELECTED1, SELECTED2) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="CODIGO" type="string"></param>    
    ///   <param name="DESCRICAO" type="string"></param>    
    ///   <param name="DESCRICAODET" type="string"></param>    
    ///   <param name="SELECTED1" type="string"></param>    
    ///   <param name="SELECTED2" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.CODIGO = CODIGO || null;
    self.DESCRICAO = DESCRICAO || null;
    self.DESCRICAODET = DESCRICAODET || null;
    self.SELECTED1 = SELECTED1 || null;
    self.SELECTED2 = SELECTED2 || null;
};

PushCampaignModel = function (Image, RetinaImage) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="Image" type="string"></param>    
    ///   <param name="RetinaImage" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.Image = Image || null;
    self.RetinaImage = RetinaImage || null;
};

RecoverPasswordModel = function (ERRO, ERROMENSAGEM) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="ERRO" type="boolean"></param>    
    ///   <param name="ERROMENSAGEM" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.ERRO = ERRO || null;
    self.ERROMENSAGEM = ERROMENSAGEM || null;
};

RegisterUserModel = function (EMAIL, ERROCADASTRO, ERROCAMPO, ERROMENS, NOME) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="EMAIL" type="string"></param>    
    ///   <param name="ERROCADASTRO" type="boolean"></param>    
    ///   <param name="ERROCAMPO" type="string"></param>    
    ///   <param name="ERROMENS" type="string"></param>    
    ///   <param name="NOME" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.EMAIL = EMAIL || null;
    self.ERROCADASTRO = ERROCADASTRO || null;
    self.ERROCAMPO = ERROCAMPO || null;
    self.ERROMENS = ERROMENS || null;
    self.NOME = NOME || null;
};

ReorderModel = function (CEP, CODIGOFILIAL, CODIGORESTAURANTE, MESSAGE, Restaurant, SUCCESS) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="CEP" type="string"></param>    
    ///   <param name="CODIGOFILIAL" type="short"></param>    
    ///   <param name="CODIGORESTAURANTE" type="short"></param>    
    ///   <param name="MESSAGE" type="string"></param>    
    ///   <param name="Restaurant" type="SERPModelDadosRestaurante"></param>    
    ///   <param name="SUCCESS" type="boolean"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.CEP = CEP || null;
    self.CODIGOFILIAL = CODIGOFILIAL || null;
    self.CODIGORESTAURANTE = CODIGORESTAURANTE || null;
    self.MESSAGE = MESSAGE || null;
    self.Restaurant = Restaurant || null;
    self.SUCCESS = SUCCESS || null;
};

RestaurantGrupo = function (CODIGO, DESCRICAO, PRATOS, TEXTOCOMPLEMENTO) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="CODIGO" type="int"></param>    
    ///   <param name="DESCRICAO" type="string"></param>    
    ///   <param name="PRATOS" type="ArrayOfRestaurantPrato"></param>    
    ///   <param name="TEXTOCOMPLEMENTO" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.CODIGO = CODIGO || null;
    self.DESCRICAO = DESCRICAO || null;
    self.PRATOS = PRATOS || null;
    self.TEXTOCOMPLEMENTO = TEXTOCOMPLEMENTO || null;
};

RestaurantMenuModel = function (AGENDA, ATENDECEP, BAIRRORESTAURANTE, CBASE, CEP, CIDADERESTAURANTE, CLASSAGENDAMENTO, CODFILIAL, CODRESTAURANTE, DATAPESQUISA, DIVSITUACAO, DIVSITUACAOLITERAL, DIVSITUACAOTITLE, ENDERECORESTAURANTE, ENDERECOSELECAO, ESTRELASAVALIACAO, EXIBE, EXIBEPASSOS, FAVORITO, FAVORITOS, GRUPOS, GRUPOSELECIONADO, HORAPESQUISA, HORARIO, HORARIOS, ICONESMEDIAGERAL, LINKINFORESTAURANTE, LISTA, LITAGENDAMENTO1, LITAGENDAMENTO2, LOGIN, LOGORESTAURANTE, MENSAGEM, NOMERESTAURANTE, ORIGEM, PAGBLANK, PAGINA, PRATOS, PROMOCAO, SITUACAO, TEMPOENTREGA, TEXTOAVISO, TEXTOLIVRE, TEXTOPROMO, TIPO, TIPOSCOZINHA, TIPOSPAGTO, TITLEAGENDAMENTO, TITLEPRINCIPAL, TOTALAVALIACOES, URLRESTAURANTE, VALORPEDIDOMIN, VERMENOS) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="AGENDA" type="string"></param>    
    ///   <param name="ATENDECEP" type="boolean"></param>    
    ///   <param name="BAIRRORESTAURANTE" type="string"></param>    
    ///   <param name="CBASE" type="string"></param>    
    ///   <param name="CEP" type="string"></param>    
    ///   <param name="CIDADERESTAURANTE" type="string"></param>    
    ///   <param name="CLASSAGENDAMENTO" type="string"></param>    
    ///   <param name="CODFILIAL" type="short"></param>    
    ///   <param name="CODRESTAURANTE" type="short"></param>    
    ///   <param name="DATAPESQUISA" type="string"></param>    
    ///   <param name="DIVSITUACAO" type="string"></param>    
    ///   <param name="DIVSITUACAOLITERAL" type="string"></param>    
    ///   <param name="DIVSITUACAOTITLE" type="string"></param>    
    ///   <param name="ENDERECORESTAURANTE" type="string"></param>    
    ///   <param name="ENDERECOSELECAO" type="ArrayOfEnderecoSelecao"></param>    
    ///   <param name="ESTRELASAVALIACAO" type="int"></param>    
    ///   <param name="EXIBE" type="string"></param>    
    ///   <param name="EXIBEPASSOS" type="boolean"></param>    
    ///   <param name="FAVORITO" type="boolean"></param>    
    ///   <param name="FAVORITOS" type="string"></param>    
    ///   <param name="GRUPOS" type="ArrayOfGrupoPrato"></param>    
    ///   <param name="GRUPOSELECIONADO" type="int"></param>    
    ///   <param name="HORAPESQUISA" type="string"></param>    
    ///   <param name="HORARIO" type="string"></param>    
    ///   <param name="HORARIOS" type="Horario"></param>    
    ///   <param name="ICONESMEDIAGERAL" type="string"></param>    
    ///   <param name="LINKINFORESTAURANTE" type="string"></param>    
    ///   <param name="LISTA" type="string"></param>    
    ///   <param name="LITAGENDAMENTO1" type="string"></param>    
    ///   <param name="LITAGENDAMENTO2" type="string"></param>    
    ///   <param name="LOGIN" type="boolean"></param>    
    ///   <param name="LOGORESTAURANTE" type="string"></param>    
    ///   <param name="MENSAGEM" type="string"></param>    
    ///   <param name="NOMERESTAURANTE" type="string"></param>    
    ///   <param name="ORIGEM" type="string"></param>    
    ///   <param name="PAGBLANK" type="string"></param>    
    ///   <param name="PAGINA" type="int"></param>    
    ///   <param name="PRATOS" type="ArrayOfCardapioRestauranteModelPrato"></param>    
    ///   <param name="PROMOCAO" type="string"></param>    
    ///   <param name="SITUACAO" type="string"></param>    
    ///   <param name="TEMPOENTREGA" type="string"></param>    
    ///   <param name="TEXTOAVISO" type="string"></param>    
    ///   <param name="TEXTOLIVRE" type="string"></param>    
    ///   <param name="TEXTOPROMO" type="string"></param>    
    ///   <param name="TIPO" type="string"></param>    
    ///   <param name="TIPOSCOZINHA" type="string"></param>    
    ///   <param name="TIPOSPAGTO" type="string"></param>    
    ///   <param name="TITLEAGENDAMENTO" type="string"></param>    
    ///   <param name="TITLEPRINCIPAL" type="string"></param>    
    ///   <param name="TOTALAVALIACOES" type="string"></param>    
    ///   <param name="URLRESTAURANTE" type="string"></param>    
    ///   <param name="VALORPEDIDOMIN" type="string"></param>    
    ///   <param name="VERMENOS" type="boolean"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.AGENDA = AGENDA || null;
    self.ATENDECEP = ATENDECEP || null;
    self.BAIRRORESTAURANTE = BAIRRORESTAURANTE || null;
    self.CBASE = CBASE || null;
    self.CEP = CEP || null;
    self.CIDADERESTAURANTE = CIDADERESTAURANTE || null;
    self.CLASSAGENDAMENTO = CLASSAGENDAMENTO || null;
    self.CODFILIAL = CODFILIAL || null;
    self.CODRESTAURANTE = CODRESTAURANTE || null;
    self.DATAPESQUISA = DATAPESQUISA || null;
    self.DIVSITUACAO = DIVSITUACAO || null;
    self.DIVSITUACAOLITERAL = DIVSITUACAOLITERAL || null;
    self.DIVSITUACAOTITLE = DIVSITUACAOTITLE || null;
    self.ENDERECORESTAURANTE = ENDERECORESTAURANTE || null;
    self.ENDERECOSELECAO = ENDERECOSELECAO || null;
    self.ESTRELASAVALIACAO = ESTRELASAVALIACAO || null;
    self.EXIBE = EXIBE || null;
    self.EXIBEPASSOS = EXIBEPASSOS || null;
    self.FAVORITO = FAVORITO || null;
    self.FAVORITOS = FAVORITOS || null;
    self.GRUPOS = GRUPOS || null;
    self.GRUPOSELECIONADO = GRUPOSELECIONADO || null;
    self.HORAPESQUISA = HORAPESQUISA || null;
    self.HORARIO = HORARIO || null;
    self.HORARIOS = HORARIOS || null;
    self.ICONESMEDIAGERAL = ICONESMEDIAGERAL || null;
    self.LINKINFORESTAURANTE = LINKINFORESTAURANTE || null;
    self.LISTA = LISTA || null;
    self.LITAGENDAMENTO1 = LITAGENDAMENTO1 || null;
    self.LITAGENDAMENTO2 = LITAGENDAMENTO2 || null;
    self.LOGIN = LOGIN || null;
    self.LOGORESTAURANTE = LOGORESTAURANTE || null;
    self.MENSAGEM = MENSAGEM || null;
    self.NOMERESTAURANTE = NOMERESTAURANTE || null;
    self.ORIGEM = ORIGEM || null;
    self.PAGBLANK = PAGBLANK || null;
    self.PAGINA = PAGINA || null;
    self.PRATOS = PRATOS || null;
    self.PROMOCAO = PROMOCAO || null;
    self.SITUACAO = SITUACAO || null;
    self.TEMPOENTREGA = TEMPOENTREGA || null;
    self.TEXTOAVISO = TEXTOAVISO || null;
    self.TEXTOLIVRE = TEXTOLIVRE || null;
    self.TEXTOPROMO = TEXTOPROMO || null;
    self.TIPO = TIPO || null;
    self.TIPOSCOZINHA = TIPOSCOZINHA || null;
    self.TIPOSPAGTO = TIPOSPAGTO || null;
    self.TITLEAGENDAMENTO = TITLEAGENDAMENTO || null;
    self.TITLEPRINCIPAL = TITLEPRINCIPAL || null;
    self.TOTALAVALIACOES = TOTALAVALIACOES || null;
    self.URLRESTAURANTE = URLRESTAURANTE || null;
    self.VALORPEDIDOMIN = VALORPEDIDOMIN || null;
    self.VERMENOS = VERMENOS || null;
};

RestaurantMenuSearchModel = function (CEP, CodigoFilial, CodigoGrupo, CodigoRestaurante) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="CEP" type="string"></param>    
    ///   <param name="CodigoFilial" type="short"></param>    
    ///   <param name="CodigoGrupo" type="int"></param>    
    ///   <param name="CodigoRestaurante" type="short"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.CEP = CEP || null;
    self.CodigoFilial = CodigoFilial || null;
    self.CodigoGrupo = CodigoGrupo || null;
    self.CodigoRestaurante = CodigoRestaurante || null;
};

RestaurantModel = function (ATENDECEP, Aberto, BAIRRORESTAURANTE, CEP, CIDADERESTAURANTE, CODFILIAL, CODRESTAURANTE, DATAPESQUISA, ENDERECORESTAURANTE, ENDERECOSELECAO, ESTRELASAVALIACAO, FAVORITO, FILIALCARDAPIO, GRUPOSCOMPLETOS, HORAPESQUISA, HORARIO, HORARIOS, ImagemFormatada, ImagensIconePagamento, LINKINFORESTAURANTE, LOGORESTAURANTE, NOMERESTAURANTE, PEDECEP, SITUACAO, SUGESTAO, TAXAENTREGA, TEMPOENTREGA, TEXTOAVISO, TEXTOLIVRE, TEXTOPROMO, TIPOSCOZINHA, TIPOSPAGTO, TOTALAVALIACOES, URLRESTAURANTE, VALORMINIMOPEDIDO) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="ATENDECEP" type="boolean"></param>    
    ///   <param name="Aberto" type="boolean"></param>    
    ///   <param name="BAIRRORESTAURANTE" type="string"></param>    
    ///   <param name="CEP" type="string"></param>    
    ///   <param name="CIDADERESTAURANTE" type="string"></param>    
    ///   <param name="CODFILIAL" type="short"></param>    
    ///   <param name="CODRESTAURANTE" type="short"></param>    
    ///   <param name="DATAPESQUISA" type="string"></param>    
    ///   <param name="ENDERECORESTAURANTE" type="string"></param>    
    ///   <param name="ENDERECOSELECAO" type="ArrayOfEnderecoSelecao"></param>    
    ///   <param name="ESTRELASAVALIACAO" type="int"></param>    
    ///   <param name="FAVORITO" type="boolean"></param>    
    ///   <param name="FILIALCARDAPIO" type="short"></param>    
    ///   <param name="GRUPOSCOMPLETOS" type="ArrayOfRestaurantGrupo"></param>    
    ///   <param name="HORAPESQUISA" type="string"></param>    
    ///   <param name="HORARIO" type="string"></param>    
    ///   <param name="HORARIOS" type="Horario"></param>    
    ///   <param name="ImagemFormatada" type="string"></param>    
    ///   <param name="ImagensIconePagamento" type="ArrayOfstring"></param>    
    ///   <param name="LINKINFORESTAURANTE" type="string"></param>    
    ///   <param name="LOGORESTAURANTE" type="string"></param>    
    ///   <param name="NOMERESTAURANTE" type="string"></param>    
    ///   <param name="PEDECEP" type="boolean"></param>    
    ///   <param name="SITUACAO" type="string"></param>    
    ///   <param name="SUGESTAO" type="CapaRestauranteModelSugestao"></param>    
    ///   <param name="TAXAENTREGA" type="string"></param>    
    ///   <param name="TEMPOENTREGA" type="string"></param>    
    ///   <param name="TEXTOAVISO" type="string"></param>    
    ///   <param name="TEXTOLIVRE" type="string"></param>    
    ///   <param name="TEXTOPROMO" type="string"></param>    
    ///   <param name="TIPOSCOZINHA" type="string"></param>    
    ///   <param name="TIPOSPAGTO" type="CapaRestauranteModelPagamento"></param>    
    ///   <param name="TOTALAVALIACOES" type="int"></param>    
    ///   <param name="URLRESTAURANTE" type="string"></param>    
    ///   <param name="VALORMINIMOPEDIDO" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.ATENDECEP = ATENDECEP || null;
    self.Aberto = Aberto || null;
    self.BAIRRORESTAURANTE = BAIRRORESTAURANTE || null;
    self.CEP = CEP || null;
    self.CIDADERESTAURANTE = CIDADERESTAURANTE || null;
    self.CODFILIAL = CODFILIAL || null;
    self.CODRESTAURANTE = CODRESTAURANTE || null;
    self.DATAPESQUISA = DATAPESQUISA || null;
    self.ENDERECORESTAURANTE = ENDERECORESTAURANTE || null;
    self.ENDERECOSELECAO = ENDERECOSELECAO || null;
    self.ESTRELASAVALIACAO = ESTRELASAVALIACAO || null;
    self.FAVORITO = FAVORITO || null;
    self.FILIALCARDAPIO = FILIALCARDAPIO || null;
    self.GRUPOSCOMPLETOS = GRUPOSCOMPLETOS || null;
    self.HORAPESQUISA = HORAPESQUISA || null;
    self.HORARIO = HORARIO || null;
    self.HORARIOS = HORARIOS || null;
    self.ImagemFormatada = ImagemFormatada || null;
    self.ImagensIconePagamento = ImagensIconePagamento || null;
    self.LINKINFORESTAURANTE = LINKINFORESTAURANTE || null;
    self.LOGORESTAURANTE = LOGORESTAURANTE || null;
    self.NOMERESTAURANTE = NOMERESTAURANTE || null;
    self.PEDECEP = PEDECEP || null;
    self.SITUACAO = SITUACAO || null;
    self.SUGESTAO = SUGESTAO || null;
    self.TAXAENTREGA = TAXAENTREGA || null;
    self.TEMPOENTREGA = TEMPOENTREGA || null;
    self.TEXTOAVISO = TEXTOAVISO || null;
    self.TEXTOLIVRE = TEXTOLIVRE || null;
    self.TEXTOPROMO = TEXTOPROMO || null;
    self.TIPOSCOZINHA = TIPOSCOZINHA || null;
    self.TIPOSPAGTO = TIPOSPAGTO || null;
    self.TOTALAVALIACOES = TOTALAVALIACOES || null;
    self.URLRESTAURANTE = URLRESTAURANTE || null;
    self.VALORMINIMOPEDIDO = VALORMINIMOPEDIDO || null;
};

RestaurantPrato = function (CODIGOPRATO, DESCRDETPRATO, DESCRICAOPRATO, DescricaoDetalhadaFormatada, FOTOPRATO, GRUPOPRATO, ImagemFormatada, OPCOES, OPCOESPIZZA, PIZZAMEIO, PRECOPRATO, SITUACAOPRATO, TemOpcoes, VALORPRATO) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="CODIGOPRATO" type="string"></param>    
    ///   <param name="DESCRDETPRATO" type="string"></param>    
    ///   <param name="DESCRICAOPRATO" type="string"></param>    
    ///   <param name="DescricaoDetalhadaFormatada" type="string"></param>    
    ///   <param name="FOTOPRATO" type="string"></param>    
    ///   <param name="GRUPOPRATO" type="string"></param>    
    ///   <param name="ImagemFormatada" type="string"></param>    
    ///   <param name="OPCOES" type="ArrayOfRestaurantPratoOpcoes"></param>    
    ///   <param name="OPCOESPIZZA" type="ArrayOfRestaurantPratoOpcoesPizza"></param>    
    ///   <param name="PIZZAMEIO" type="boolean"></param>    
    ///   <param name="PRECOPRATO" type="string"></param>    
    ///   <param name="SITUACAOPRATO" type="string"></param>    
    ///   <param name="TemOpcoes" type="boolean"></param>    
    ///   <param name="VALORPRATO" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.CODIGOPRATO = CODIGOPRATO || null;
    self.DESCRDETPRATO = DESCRDETPRATO || null;
    self.DESCRICAOPRATO = DESCRICAOPRATO || null;
    self.DescricaoDetalhadaFormatada = DescricaoDetalhadaFormatada || null;
    self.FOTOPRATO = FOTOPRATO || null;
    self.GRUPOPRATO = GRUPOPRATO || null;
    self.ImagemFormatada = ImagemFormatada || null;
    self.OPCOES = OPCOES || null;
    self.OPCOESPIZZA = OPCOESPIZZA || null;
    self.PIZZAMEIO = PIZZAMEIO || null;
    self.PRECOPRATO = PRECOPRATO || null;
    self.SITUACAOPRATO = SITUACAOPRATO || null;
    self.TemOpcoes = TemOpcoes || null;
    self.VALORPRATO = VALORPRATO || null;
};

RestaurantPratoOpcoes = function (ITENS, ListaTextosAssociados, MAXIMOITENS, MINIMOITENS, TEMPRECO, TITULO, TITULOSIMPLES) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="ITENS" type="ArrayOfRestaurantPratoOpcoesItem"></param>    
    ///   <param name="ListaTextosAssociados" type="ArrayOfOpcaoAssociada"></param>    
    ///   <param name="MAXIMOITENS" type="string"></param>    
    ///   <param name="MINIMOITENS" type="string"></param>    
    ///   <param name="TEMPRECO" type="boolean"></param>    
    ///   <param name="TITULO" type="string"></param>    
    ///   <param name="TITULOSIMPLES" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.ITENS = ITENS || null;
    self.ListaTextosAssociados = ListaTextosAssociados || null;
    self.MAXIMOITENS = MAXIMOITENS || null;
    self.MINIMOITENS = MINIMOITENS || null;
    self.TEMPRECO = TEMPRECO || null;
    self.TITULO = TITULO || null;
    self.TITULOSIMPLES = TITULOSIMPLES || null;
};

RestaurantPratoOpcoesItem = function (CHECK, DescricaoDetalhada, ListaPrecoFormatado, NOMEVARIAVEL, Selecionado, TEXTOVALOR, TIPOVARIAVEL, TITULODETALHE, TITULOVARIAVEL, VALOR, VALORVARIAVEL, VALUEVARIAVEL) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="CHECK" type="string"></param>    
    ///   <param name="DescricaoDetalhada" type="string"></param>    
    ///   <param name="ListaPrecoFormatado" type="ArrayOfOpcaoItemPreco"></param>    
    ///   <param name="NOMEVARIAVEL" type="string"></param>    
    ///   <param name="Selecionado" type="boolean"></param>    
    ///   <param name="TEXTOVALOR" type="string"></param>    
    ///   <param name="TIPOVARIAVEL" type="string"></param>    
    ///   <param name="TITULODETALHE" type="string"></param>    
    ///   <param name="TITULOVARIAVEL" type="string"></param>    
    ///   <param name="VALOR" type="string"></param>    
    ///   <param name="VALORVARIAVEL" type="string"></param>    
    ///   <param name="VALUEVARIAVEL" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.CHECK = CHECK || null;
    self.DescricaoDetalhada = DescricaoDetalhada || null;
    self.ListaPrecoFormatado = ListaPrecoFormatado || null;
    self.NOMEVARIAVEL = NOMEVARIAVEL || null;
    self.Selecionado = Selecionado || null;
    self.TEXTOVALOR = TEXTOVALOR || null;
    self.TIPOVARIAVEL = TIPOVARIAVEL || null;
    self.TITULODETALHE = TITULODETALHE || null;
    self.TITULOVARIAVEL = TITULOVARIAVEL || null;
    self.VALOR = VALOR || null;
    self.VALORVARIAVEL = VALORVARIAVEL || null;
    self.VALUEVARIAVEL = VALUEVARIAVEL || null;
};

RestaurantPratoOpcoesPizza = function (CODIGO, DESCRICAO, DESCRICAODET, SELECTED1, SELECTED2) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="CODIGO" type="string"></param>    
    ///   <param name="DESCRICAO" type="string"></param>    
    ///   <param name="DESCRICAODET" type="string"></param>    
    ///   <param name="SELECTED1" type="string"></param>    
    ///   <param name="SELECTED2" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.CODIGO = CODIGO || null;
    self.DESCRICAO = DESCRICAO || null;
    self.DESCRICAODET = DESCRICAODET || null;
    self.SELECTED1 = SELECTED1 || null;
    self.SELECTED2 = SELECTED2 || null;
};

SERPModel = function (APENASFAVORITOS, BAIRRO, CEPSELECIONADO, CIDADE, DADOSDATAHORACOMBO, DADOSRESTAURANTES, DADOSTIPOCOZINHA, DADOSTIPOCOZINHACOMBO, DADOSUSUARIO, ENDERECOSELECAO, ERRO, ERROMENSAGEM, FAVORITOS, LITRESTAURANTES, PAGINAS, QTDERESTAURANTES, QTDPAGINAS, RESTAURANTECOMPROMOCAO, RESTAURANTESEXIBE, SessionId, TITLEPRINCIPAL, TITLEPRINCIPALPAGE) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="APENASFAVORITOS" type="string"></param>    
    ///   <param name="BAIRRO" type="string"></param>    
    ///   <param name="CEPSELECIONADO" type="string"></param>    
    ///   <param name="CIDADE" type="string"></param>    
    ///   <param name="DADOSDATAHORACOMBO" type="SERPModelDataHoraCombo"></param>    
    ///   <param name="DADOSRESTAURANTES" type="ArrayOfSERPModelDadosRestaurante"></param>    
    ///   <param name="DADOSTIPOCOZINHA" type="ArrayOfSERPModelTipoCozinha"></param>    
    ///   <param name="DADOSTIPOCOZINHACOMBO" type="ArrayOfSERPModelTipoCozinhaCombo"></param>    
    ///   <param name="DADOSUSUARIO" type="DadosUsuario"></param>    
    ///   <param name="ENDERECOSELECAO" type="ArrayOfEnderecoSelecao"></param>    
    ///   <param name="ERRO" type="boolean"></param>    
    ///   <param name="ERROMENSAGEM" type="string"></param>    
    ///   <param name="FAVORITOS" type="string"></param>    
    ///   <param name="LITRESTAURANTES" type="string"></param>    
    ///   <param name="PAGINAS" type="ArrayOfint"></param>    
    ///   <param name="QTDERESTAURANTES" type="int"></param>    
    ///   <param name="QTDPAGINAS" type="int"></param>    
    ///   <param name="RESTAURANTECOMPROMOCAO" type="string"></param>    
    ///   <param name="RESTAURANTESEXIBE" type="string"></param>    
    ///   <param name="SessionId" type="string"></param>    
    ///   <param name="TITLEPRINCIPAL" type="string"></param>    
    ///   <param name="TITLEPRINCIPALPAGE" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.APENASFAVORITOS = APENASFAVORITOS || null;
    self.BAIRRO = BAIRRO || null;
    self.CEPSELECIONADO = CEPSELECIONADO || null;
    self.CIDADE = CIDADE || null;
    self.DADOSDATAHORACOMBO = DADOSDATAHORACOMBO || null;
    self.DADOSRESTAURANTES = DADOSRESTAURANTES || null;
    self.DADOSTIPOCOZINHA = DADOSTIPOCOZINHA || null;
    self.DADOSTIPOCOZINHACOMBO = DADOSTIPOCOZINHACOMBO || null;
    self.DADOSUSUARIO = DADOSUSUARIO || null;
    self.ENDERECOSELECAO = ENDERECOSELECAO || null;
    self.ERRO = ERRO || null;
    self.ERROMENSAGEM = ERROMENSAGEM || null;
    self.FAVORITOS = FAVORITOS || null;
    self.LITRESTAURANTES = LITRESTAURANTES || null;
    self.PAGINAS = PAGINAS || null;
    self.QTDERESTAURANTES = QTDERESTAURANTES || null;
    self.QTDPAGINAS = QTDPAGINAS || null;
    self.RESTAURANTECOMPROMOCAO = RESTAURANTECOMPROMOCAO || null;
    self.RESTAURANTESEXIBE = RESTAURANTESEXIBE || null;
    self.SessionId = SessionId || null;
    self.TITLEPRINCIPAL = TITLEPRINCIPAL || null;
    self.TITLEPRINCIPALPAGE = TITLEPRINCIPALPAGE || null;
};

SERPModelDadosRestaurante = function (CADASTRO, EstaAberto, FAVORITO, HORARIO, ICONESMEDIAGERAL, LINKINFORESTAURANTE, SITUACAO, TIPOSCOZINHA, TIPOSCOZINHACODIGOS, TIPOSCOZINHAFAVORITA, TIPOSCOZINHAFRONT, TITLENOME, TOTALAVALIACOES, TiposCozinhaList, TotalAvaliacoesFormatada) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="CADASTRO" type="SERPModelDadosRestauranteCadastro"></param>    
    ///   <param name="EstaAberto" type="boolean"></param>    
    ///   <param name="FAVORITO" type="boolean"></param>    
    ///   <param name="HORARIO" type="string"></param>    
    ///   <param name="ICONESMEDIAGERAL" type="string"></param>    
    ///   <param name="LINKINFORESTAURANTE" type="string"></param>    
    ///   <param name="SITUACAO" type="string"></param>    
    ///   <param name="TIPOSCOZINHA" type="string"></param>    
    ///   <param name="TIPOSCOZINHACODIGOS" type="string"></param>    
    ///   <param name="TIPOSCOZINHAFAVORITA" type="string"></param>    
    ///   <param name="TIPOSCOZINHAFRONT" type="string"></param>    
    ///   <param name="TITLENOME" type="string"></param>    
    ///   <param name="TOTALAVALIACOES" type="string"></param>    
    ///   <param name="TiposCozinhaList" type="ArrayOfSERPModelDadosRestauranteTipoCozinha"></param>    
    ///   <param name="TotalAvaliacoesFormatada" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.CADASTRO = CADASTRO || null;
    self.EstaAberto = EstaAberto || null;
    self.FAVORITO = FAVORITO || null;
    self.HORARIO = HORARIO || null;
    self.ICONESMEDIAGERAL = ICONESMEDIAGERAL || null;
    self.LINKINFORESTAURANTE = LINKINFORESTAURANTE || null;
    self.SITUACAO = SITUACAO || null;
    self.TIPOSCOZINHA = TIPOSCOZINHA || null;
    self.TIPOSCOZINHACODIGOS = TIPOSCOZINHACODIGOS || null;
    self.TIPOSCOZINHAFAVORITA = TIPOSCOZINHAFAVORITA || null;
    self.TIPOSCOZINHAFRONT = TIPOSCOZINHAFRONT || null;
    self.TITLENOME = TITLENOME || null;
    self.TOTALAVALIACOES = TOTALAVALIACOES || null;
    self.TiposCozinhaList = TiposCozinhaList || null;
    self.TotalAvaliacoesFormatada = TotalAvaliacoesFormatada || null;
};

SERPModelDadosRestauranteCadastro = function (BAIRRO, CIDADE, CODIGO, DESCONTO, ENDERECO, ESTADO, FIDELIDADE, FILIAL, FILIALCARDAPIO, FOTO, ImagemFormatada, MEDIAGERAL, MEDIAPRAZOENTREGA, MEDIAQUALIDADE, MINPEDIDOPROG, NOME, NumeroEstrelas, PEDMINIMO, PRIORIDADE, QTDDIASPROG, RETIRAR, SITUACAOATIVIDADE, TEMPOENTREGA, TEXTOAVISO, TEXTOCOMPLETO, TEXTOLIVRE, TOTALAVALIACOES, TemPromocao, URL, URLAMIGAVEL) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="BAIRRO" type="string"></param>    
    ///   <param name="CIDADE" type="string"></param>    
    ///   <param name="CODIGO" type="short"></param>    
    ///   <param name="DESCONTO" type="string"></param>    
    ///   <param name="ENDERECO" type="string"></param>    
    ///   <param name="ESTADO" type="string"></param>    
    ///   <param name="FIDELIDADE" type="string"></param>    
    ///   <param name="FILIAL" type="short"></param>    
    ///   <param name="FILIALCARDAPIO" type="short"></param>    
    ///   <param name="FOTO" type="string"></param>    
    ///   <param name="ImagemFormatada" type="string"></param>    
    ///   <param name="MEDIAGERAL" type="decimal"></param>    
    ///   <param name="MEDIAPRAZOENTREGA" type="decimal"></param>    
    ///   <param name="MEDIAQUALIDADE" type="decimal"></param>    
    ///   <param name="MINPEDIDOPROG" type="string"></param>    
    ///   <param name="NOME" type="string"></param>    
    ///   <param name="NumeroEstrelas" type="int"></param>    
    ///   <param name="PEDMINIMO" type="decimal"></param>    
    ///   <param name="PRIORIDADE" type="string"></param>    
    ///   <param name="QTDDIASPROG" type="string"></param>    
    ///   <param name="RETIRAR" type="string"></param>    
    ///   <param name="SITUACAOATIVIDADE" type="string"></param>    
    ///   <param name="TEMPOENTREGA" type="string"></param>    
    ///   <param name="TEXTOAVISO" type="string"></param>    
    ///   <param name="TEXTOCOMPLETO" type="string"></param>    
    ///   <param name="TEXTOLIVRE" type="string"></param>    
    ///   <param name="TOTALAVALIACOES" type="string"></param>    
    ///   <param name="TemPromocao" type="boolean"></param>    
    ///   <param name="URL" type="string"></param>    
    ///   <param name="URLAMIGAVEL" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.BAIRRO = BAIRRO || null;
    self.CIDADE = CIDADE || null;
    self.CODIGO = CODIGO || null;
    self.DESCONTO = DESCONTO || null;
    self.ENDERECO = ENDERECO || null;
    self.ESTADO = ESTADO || null;
    self.FIDELIDADE = FIDELIDADE || null;
    self.FILIAL = FILIAL || null;
    self.FILIALCARDAPIO = FILIALCARDAPIO || null;
    self.FOTO = FOTO || null;
    self.ImagemFormatada = ImagemFormatada || null;
    self.MEDIAGERAL = MEDIAGERAL || null;
    self.MEDIAPRAZOENTREGA = MEDIAPRAZOENTREGA || null;
    self.MEDIAQUALIDADE = MEDIAQUALIDADE || null;
    self.MINPEDIDOPROG = MINPEDIDOPROG || null;
    self.NOME = NOME || null;
    self.NumeroEstrelas = NumeroEstrelas || null;
    self.PEDMINIMO = PEDMINIMO || null;
    self.PRIORIDADE = PRIORIDADE || null;
    self.QTDDIASPROG = QTDDIASPROG || null;
    self.RETIRAR = RETIRAR || null;
    self.SITUACAOATIVIDADE = SITUACAOATIVIDADE || null;
    self.TEMPOENTREGA = TEMPOENTREGA || null;
    self.TEXTOAVISO = TEXTOAVISO || null;
    self.TEXTOCOMPLETO = TEXTOCOMPLETO || null;
    self.TEXTOLIVRE = TEXTOLIVRE || null;
    self.TOTALAVALIACOES = TOTALAVALIACOES || null;
    self.TemPromocao = TemPromocao || null;
    self.URL = URL || null;
    self.URLAMIGAVEL = URLAMIGAVEL || null;
};

SERPModelDadosRestauranteTipoCozinha = function (Codigo, Descricao) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="Codigo" type="int"></param>    
    ///   <param name="Descricao" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.Codigo = Codigo || null;
    self.Descricao = Descricao || null;
};

SERPModelDataCombo = function (DIA, SELECTED, VALUE) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="DIA" type="string"></param>    
    ///   <param name="SELECTED" type="string"></param>    
    ///   <param name="VALUE" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.DIA = DIA || null;
    self.SELECTED = SELECTED || null;
    self.VALUE = VALUE || null;
};

SERPModelDataHoraCombo = function (DATAS, HORARIOS) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="DATAS" type="ArrayOfSERPModelDataCombo"></param>    
    ///   <param name="HORARIOS" type="ArrayOfSERPModelHoraCombo"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.DATAS = DATAS || null;
    self.HORARIOS = HORARIOS || null;
};

SERPModelHoraCombo = function (HORA, SELECTED, VALUE) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="HORA" type="string"></param>    
    ///   <param name="SELECTED" type="string"></param>    
    ///   <param name="VALUE" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.HORA = HORA || null;
    self.SELECTED = SELECTED || null;
    self.VALUE = VALUE || null;
};

SERPModelTipoCozinha = function (CHECKED, CODIGO, DESCRICAO, DESCRICAOAUX, TOTAL) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="CHECKED" type="string"></param>    
    ///   <param name="CODIGO" type="int"></param>    
    ///   <param name="DESCRICAO" type="string"></param>    
    ///   <param name="DESCRICAOAUX" type="string"></param>    
    ///   <param name="TOTAL" type="int"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.CHECKED = CHECKED || null;
    self.CODIGO = CODIGO || null;
    self.DESCRICAO = DESCRICAO || null;
    self.DESCRICAOAUX = DESCRICAOAUX || null;
    self.TOTAL = TOTAL || null;
};

SERPModelTipoCozinhaCombo = function (CODIGO, DESCRICAO, DESCRICAOAUX, SELECTED, TOTAL) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="CODIGO" type="int"></param>    
    ///   <param name="DESCRICAO" type="string"></param>    
    ///   <param name="DESCRICAOAUX" type="string"></param>    
    ///   <param name="SELECTED" type="string"></param>    
    ///   <param name="TOTAL" type="int"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.CODIGO = CODIGO || null;
    self.DESCRICAO = DESCRICAO || null;
    self.DESCRICAOAUX = DESCRICAOAUX || null;
    self.SELECTED = SELECTED || null;
    self.TOTAL = TOTAL || null;
};

SERPSearchModel = function (Abertos, CEP, Data, Favoritos, Logado, Promocao) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="Abertos" type="boolean"></param>    
    ///   <param name="CEP" type="string"></param>    
    ///   <param name="Data" type="dateTime"></param>    
    ///   <param name="Favoritos" type="boolean"></param>    
    ///   <param name="Logado" type="boolean"></param>    
    ///   <param name="Promocao" type="boolean"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.Abertos = Abertos || null;
    self.CEP = CEP || null;
    self.Data = Data || null;
    self.Favoritos = Favoritos || null;
    self.Logado = Logado || null;
    self.Promocao = Promocao || null;
};

SecurityModel = function (Password, SessionId, Username) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="Password" type="string"></param>    
    ///   <param name="SessionId" type="string"></param>    
    ///   <param name="Username" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.Password = Password || null;
    self.SessionId = SessionId || null;
    self.Username = Username || null;
};

UserBasicAndOrderModel = function (IsUserLoggedIn, Message, Order, UserBasicData) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="IsUserLoggedIn" type="boolean"></param>    
    ///   <param name="Message" type="string"></param>    
    ///   <param name="Order" type="OrderModel"></param>    
    ///   <param name="UserBasicData" type="UserBasicDataModel"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.IsUserLoggedIn = IsUserLoggedIn || null;
    self.Message = Message || null;
    self.Order = Order || null;
    self.UserBasicData = UserBasicData || null;
};

UserBasicDataModel = function (CPF, Code, Email, Name) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="CPF" type="string"></param>    
    ///   <param name="Code" type="int"></param>    
    ///   <param name="Email" type="string"></param>    
    ///   <param name="Name" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.CPF = CPF || null;
    self.Code = Code || null;
    self.Email = Email || null;
    self.Name = Name || null;
};

UserModel = function (Address, AddressCity, AddressComplement, AddressNeighborhood, AddressNumber, AddressState, AddressZipCode, BirthDate, CPF, Email, Genre, Name, Password, Phone1DDD, Phone1Number, Phone2DDD, Phone2Number) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="Address" type="string"></param>    
    ///   <param name="AddressCity" type="string"></param>    
    ///   <param name="AddressComplement" type="string"></param>    
    ///   <param name="AddressNeighborhood" type="string"></param>    
    ///   <param name="AddressNumber" type="string"></param>    
    ///   <param name="AddressState" type="string"></param>    
    ///   <param name="AddressZipCode" type="string"></param>    
    ///   <param name="BirthDate" type="dateTime"></param>    
    ///   <param name="CPF" type="string"></param>    
    ///   <param name="Email" type="string"></param>    
    ///   <param name="Genre" type="GenreEnum"></param>    
    ///   <param name="Name" type="string"></param>    
    ///   <param name="Password" type="string"></param>    
    ///   <param name="Phone1DDD" type="string"></param>    
    ///   <param name="Phone1Number" type="string"></param>    
    ///   <param name="Phone2DDD" type="string"></param>    
    ///   <param name="Phone2Number" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.Address = Address || null;
    self.AddressCity = AddressCity || null;
    self.AddressComplement = AddressComplement || null;
    self.AddressNeighborhood = AddressNeighborhood || null;
    self.AddressNumber = AddressNumber || null;
    self.AddressState = AddressState || null;
    self.AddressZipCode = AddressZipCode || null;
    self.BirthDate = BirthDate || null;
    self.CPF = CPF || null;
    self.Email = Email || null;
    self.Genre = Genre || null;
    self.Name = Name || null;
    self.Password = Password || null;
    self.Phone1DDD = Phone1DDD || null;
    self.Phone1Number = Phone1Number || null;
    self.Phone2DDD = Phone2DDD || null;
    self.Phone2Number = Phone2Number || null;
};

WebSession = function (DADOSRESTAURANTE, DADOSRESTAURANTECAPA, DADOSUSUARIO, LOGADO, PEDIDO, URLTOKEN) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="DADOSRESTAURANTE" type="ArrayOfSERPModelDadosRestaurante"></param>    
    ///   <param name="DADOSRESTAURANTECAPA" type="WebSessionCapaRestaurante"></param>    
    ///   <param name="DADOSUSUARIO" type="DadosUsuario"></param>    
    ///   <param name="LOGADO" type="boolean"></param>    
    ///   <param name="PEDIDO" type="WebSessionPedido"></param>    
    ///   <param name="URLTOKEN" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.DADOSRESTAURANTE = DADOSRESTAURANTE || null;
    self.DADOSRESTAURANTECAPA = DADOSRESTAURANTECAPA || null;
    self.DADOSUSUARIO = DADOSUSUARIO || null;
    self.LOGADO = LOGADO || null;
    self.PEDIDO = PEDIDO || null;
    self.URLTOKEN = URLTOKEN || null;
};

WebSessionCapaRestaurante = function (ATENDECEP, AUTORIZAEMISSINT, AUTORIZAREDECARD, AUTORIZAVISANET, BAIRRO, CIDADE, CODCEP, CODFILIAL, CODRESTAURANTE, CODVOUCHERAUT, DDDFAXENVIO, DIASAGENDAMENTO, ENDERECO, ENVIAALERTAEMAIL, ESTADO, EXIBEPASSOS, FILIALCARDAPIO, FILIALENVIO, FLAGTAXAENTRAGA, HORARIO, LOGORESTAURANTE, MEDIAGERAL, MINPEDIDOPROG, NOME, NRODDDTRAVA, ORIGEM, PERCENTAXA, PERCENTPROMOCAO, PROMOCAODESCRICAO, PROMOCAOPARCEIRO, PROMOCAOPERCENTPROMOCAO, PROMOCAOSITUACAO, PROMOCAOTXTFAX, QTDDIASPROG, RESTENVIO, RISCOREST, SITUACAO, TAXACOMISSAO, TAXAENTREGA, TECNOLOGIAENVIO, TELEFONEFAXENVIO, TEMPOENTREGA, TEMPROMOCAO, TEMTAXAFIXA, TEXTOAVISO, TEXTOLIVRE, TEXTOPROMO, TIPOCALCPIZZA, TIPOSCOZINHA, TIPOSCOZINHATITLE, TIPOTAXA, TRAVADDD, URL, URLRESTAURANTE, VALORPEDIDOMIN, VLMAXTAXA, VLMINTAXA) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="ATENDECEP" type="boolean"></param>    
    ///   <param name="AUTORIZAEMISSINT" type="string"></param>    
    ///   <param name="AUTORIZAREDECARD" type="string"></param>    
    ///   <param name="AUTORIZAVISANET" type="string"></param>    
    ///   <param name="BAIRRO" type="string"></param>    
    ///   <param name="CIDADE" type="string"></param>    
    ///   <param name="CODCEP" type="string"></param>    
    ///   <param name="CODFILIAL" type="short"></param>    
    ///   <param name="CODRESTAURANTE" type="short"></param>    
    ///   <param name="CODVOUCHERAUT" type="string"></param>    
    ///   <param name="DDDFAXENVIO" type="string"></param>    
    ///   <param name="DIASAGENDAMENTO" type="string"></param>    
    ///   <param name="ENDERECO" type="string"></param>    
    ///   <param name="ENVIAALERTAEMAIL" type="string"></param>    
    ///   <param name="ESTADO" type="string"></param>    
    ///   <param name="EXIBEPASSOS" type="boolean"></param>    
    ///   <param name="FILIALCARDAPIO" type="short"></param>    
    ///   <param name="FILIALENVIO" type="string"></param>    
    ///   <param name="FLAGTAXAENTRAGA" type="string"></param>    
    ///   <param name="HORARIO" type="string"></param>    
    ///   <param name="LOGORESTAURANTE" type="string"></param>    
    ///   <param name="MEDIAGERAL" type="string"></param>    
    ///   <param name="MINPEDIDOPROG" type="string"></param>    
    ///   <param name="NOME" type="string"></param>    
    ///   <param name="NRODDDTRAVA" type="string"></param>    
    ///   <param name="ORIGEM" type="string"></param>    
    ///   <param name="PERCENTAXA" type="string"></param>    
    ///   <param name="PERCENTPROMOCAO" type="string"></param>    
    ///   <param name="PROMOCAODESCRICAO" type="string"></param>    
    ///   <param name="PROMOCAOPARCEIRO" type="string"></param>    
    ///   <param name="PROMOCAOPERCENTPROMOCAO" type="string"></param>    
    ///   <param name="PROMOCAOSITUACAO" type="string"></param>    
    ///   <param name="PROMOCAOTXTFAX" type="string"></param>    
    ///   <param name="QTDDIASPROG" type="string"></param>    
    ///   <param name="RESTENVIO" type="string"></param>    
    ///   <param name="RISCOREST" type="boolean"></param>    
    ///   <param name="SITUACAO" type="string"></param>    
    ///   <param name="TAXACOMISSAO" type="string"></param>    
    ///   <param name="TAXAENTREGA" type="string"></param>    
    ///   <param name="TECNOLOGIAENVIO" type="string"></param>    
    ///   <param name="TELEFONEFAXENVIO" type="string"></param>    
    ///   <param name="TEMPOENTREGA" type="string"></param>    
    ///   <param name="TEMPROMOCAO" type="string"></param>    
    ///   <param name="TEMTAXAFIXA" type="boolean"></param>    
    ///   <param name="TEXTOAVISO" type="string"></param>    
    ///   <param name="TEXTOLIVRE" type="string"></param>    
    ///   <param name="TEXTOPROMO" type="string"></param>    
    ///   <param name="TIPOCALCPIZZA" type="string"></param>    
    ///   <param name="TIPOSCOZINHA" type="string"></param>    
    ///   <param name="TIPOSCOZINHATITLE" type="string"></param>    
    ///   <param name="TIPOTAXA" type="string"></param>    
    ///   <param name="TRAVADDD" type="string"></param>    
    ///   <param name="URL" type="string"></param>    
    ///   <param name="URLRESTAURANTE" type="string"></param>    
    ///   <param name="VALORPEDIDOMIN" type="string"></param>    
    ///   <param name="VLMAXTAXA" type="string"></param>    
    ///   <param name="VLMINTAXA" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.ATENDECEP = ATENDECEP || null;
    self.AUTORIZAEMISSINT = AUTORIZAEMISSINT || null;
    self.AUTORIZAREDECARD = AUTORIZAREDECARD || null;
    self.AUTORIZAVISANET = AUTORIZAVISANET || null;
    self.BAIRRO = BAIRRO || null;
    self.CIDADE = CIDADE || null;
    self.CODCEP = CODCEP || null;
    self.CODFILIAL = CODFILIAL || null;
    self.CODRESTAURANTE = CODRESTAURANTE || null;
    self.CODVOUCHERAUT = CODVOUCHERAUT || null;
    self.DDDFAXENVIO = DDDFAXENVIO || null;
    self.DIASAGENDAMENTO = DIASAGENDAMENTO || null;
    self.ENDERECO = ENDERECO || null;
    self.ENVIAALERTAEMAIL = ENVIAALERTAEMAIL || null;
    self.ESTADO = ESTADO || null;
    self.EXIBEPASSOS = EXIBEPASSOS || null;
    self.FILIALCARDAPIO = FILIALCARDAPIO || null;
    self.FILIALENVIO = FILIALENVIO || null;
    self.FLAGTAXAENTRAGA = FLAGTAXAENTRAGA || null;
    self.HORARIO = HORARIO || null;
    self.LOGORESTAURANTE = LOGORESTAURANTE || null;
    self.MEDIAGERAL = MEDIAGERAL || null;
    self.MINPEDIDOPROG = MINPEDIDOPROG || null;
    self.NOME = NOME || null;
    self.NRODDDTRAVA = NRODDDTRAVA || null;
    self.ORIGEM = ORIGEM || null;
    self.PERCENTAXA = PERCENTAXA || null;
    self.PERCENTPROMOCAO = PERCENTPROMOCAO || null;
    self.PROMOCAODESCRICAO = PROMOCAODESCRICAO || null;
    self.PROMOCAOPARCEIRO = PROMOCAOPARCEIRO || null;
    self.PROMOCAOPERCENTPROMOCAO = PROMOCAOPERCENTPROMOCAO || null;
    self.PROMOCAOSITUACAO = PROMOCAOSITUACAO || null;
    self.PROMOCAOTXTFAX = PROMOCAOTXTFAX || null;
    self.QTDDIASPROG = QTDDIASPROG || null;
    self.RESTENVIO = RESTENVIO || null;
    self.RISCOREST = RISCOREST || null;
    self.SITUACAO = SITUACAO || null;
    self.TAXACOMISSAO = TAXACOMISSAO || null;
    self.TAXAENTREGA = TAXAENTREGA || null;
    self.TECNOLOGIAENVIO = TECNOLOGIAENVIO || null;
    self.TELEFONEFAXENVIO = TELEFONEFAXENVIO || null;
    self.TEMPOENTREGA = TEMPOENTREGA || null;
    self.TEMPROMOCAO = TEMPROMOCAO || null;
    self.TEMTAXAFIXA = TEMTAXAFIXA || null;
    self.TEXTOAVISO = TEXTOAVISO || null;
    self.TEXTOLIVRE = TEXTOLIVRE || null;
    self.TEXTOPROMO = TEXTOPROMO || null;
    self.TIPOCALCPIZZA = TIPOCALCPIZZA || null;
    self.TIPOSCOZINHA = TIPOSCOZINHA || null;
    self.TIPOSCOZINHATITLE = TIPOSCOZINHATITLE || null;
    self.TIPOTAXA = TIPOTAXA || null;
    self.TRAVADDD = TRAVADDD || null;
    self.URL = URL || null;
    self.URLRESTAURANTE = URLRESTAURANTE || null;
    self.VALORPEDIDOMIN = VALORPEDIDOMIN || null;
    self.VLMAXTAXA = VLMAXTAXA || null;
    self.VLMINTAXA = VLMINTAXA || null;
};

WebSessionPedido = function (TEMPEDIDO, listaNomPrato, listaObsPrato, listaOpcoes, listaPrato, listaQtdePrato, listaVlOpcoes, listaVlPrato, listapratoint, listapratoint1, listapratointOpc, opcoesPizza, temOpcoes, tipoOpcao, tipoPrato, vlTotPrato, vltotOpcoes) {
    /// <signature>
    ///   <summary></summary>    
    ///   <param name="TEMPEDIDO" type="boolean"></param>    
    ///   <param name="listaNomPrato" type="string"></param>    
    ///   <param name="listaObsPrato" type="string"></param>    
    ///   <param name="listaOpcoes" type="string"></param>    
    ///   <param name="listaPrato" type="string"></param>    
    ///   <param name="listaQtdePrato" type="string"></param>    
    ///   <param name="listaVlOpcoes" type="string"></param>    
    ///   <param name="listaVlPrato" type="string"></param>    
    ///   <param name="listapratoint" type="string"></param>    
    ///   <param name="listapratoint1" type="string"></param>    
    ///   <param name="listapratointOpc" type="string"></param>    
    ///   <param name="opcoesPizza" type="string"></param>    
    ///   <param name="temOpcoes" type="string"></param>    
    ///   <param name="tipoOpcao" type="string"></param>    
    ///   <param name="tipoPrato" type="string"></param>    
    ///   <param name="vlTotPrato" type="string"></param>    
    ///   <param name="vltotOpcoes" type="string"></param>
    ///   <returns type="String" />
    /// </signature>

    var self = this;
    self.TEMPEDIDO = TEMPEDIDO || null;
    self.listaNomPrato = listaNomPrato || null;
    self.listaObsPrato = listaObsPrato || null;
    self.listaOpcoes = listaOpcoes || null;
    self.listaPrato = listaPrato || null;
    self.listaQtdePrato = listaQtdePrato || null;
    self.listaVlOpcoes = listaVlOpcoes || null;
    self.listaVlPrato = listaVlPrato || null;
    self.listapratoint = listapratoint || null;
    self.listapratoint1 = listapratoint1 || null;
    self.listapratointOpc = listapratointOpc || null;
    self.opcoesPizza = opcoesPizza || null;
    self.temOpcoes = temOpcoes || null;
    self.tipoOpcao = tipoOpcao || null;
    self.tipoPrato = tipoPrato || null;
    self.vlTotPrato = vlTotPrato || null;
    self.vltotOpcoes = vltotOpcoes || null;
};