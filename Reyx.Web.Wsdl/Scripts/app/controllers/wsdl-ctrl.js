﻿if (typeof String.prototype.endsWith !== 'function') {
    String.prototype.endsWith = function (suffix) {
        return this.indexOf(suffix, this.length - suffix.length) !== -1;
    };
}

function WsdlCtrl($scope) {
    $scope.url = 'http://panasonic.restauranteweb.com.br/servicoapps/datalayerservice.svc';
    $scope.interface = 'IDataLayerService';
    $scope.fileName;
    $scope.xml;
    $scope.json;
    $scope.class;
    $scope.webService;

    $scope.wsdl;    
    $scope.enumerations = [];
    $scope.functions = [];
    $scope.imports = [];
    $scope.models = [];
    $scope.operations = [];
    $scope.requests = [];

    $scope.extractImports = function () {
        _.each($scope.imports, function (xsd) {
            switch (xsd.namespace) {
                case "http://tempuri.org/":
                    _.each(xsd.data.element, function (type) {
                        if (type.name.endsWith('Response')) {
                            _.find($scope.models, function (item) {
                                return item.name === type.name.replace('Response', '');
                            }).returns = type.complexType.sequence.element.type.split(':')[1];
                        } else {
                            var method = { name: type.name, properties: [] };

                            if (type.complexType && type.complexType.sequence) {
                                if ($.isArray(type.complexType.sequence.element)) {
                                    _.each(type.complexType.sequence.element, function (prop) {
                                        method.properties.push({ name: prop.name, type: prop.type.split(':')[1] });
                                    });
                                } else {
                                    method.properties.push({ name: type.complexType.sequence.element.name, type: type.complexType.sequence.element.type.split(':')[1] });
                                }
                            }

                            $scope.models.push(method);
                        }
                    });
                    break;
                case "http://schemas.microsoft.com/2003/10/Serialization/":
                    break;
                case "http://schemas.datacontract.org/2004/07/RWebApplication.Service.ServiceModels":
                case "http://schemas.datacontract.org/2004/07/RWebApplication.Service.Models":
                    if (xsd.data && xsd.data.complexType) {
                        _.each(xsd.data.complexType, function (type) {

                            var model = { name: type.name, properties: [] };

                            _.each(type.sequence.element, function (prop) {
                                if (type.name.indexOf('ArrayOf') === -1) {
                                    if (prop.name)
                                        model.properties.push({ name: prop.name, type: prop.type.split(':')[1] });
                                    else {
                                        model.properties.push({ name: type.sequence.element.name, type: type.sequence.element.type.split(':')[1] });
                                    }
                                }
                            });

                            $scope.functions.push(model);
                        });
                    }

                    if (xsd.data.simpleType && xsd.data.simpleType.restriction && xsd.data.simpleType.restriction.enumeration) {
                        var enumeration = { name: xsd.data.simpleType.name, properties: [] };
                        
                        _.each(xsd.data.simpleType.restriction.enumeration, function (prop) {
                            enumeration.properties.push({ name: prop.value, value: prop.annotation.appinfo.EnumerationValue });
                        });

                        $scope.enumerations.push(enumeration);
                    }
                    break;
                case "http://schemas.microsoft.com/2003/10/Serialization/Arrays":
                    if (xsd.data.element && xsd.data.element.complexType && xsd.element.complexType.sequence) {
                        _.each(xsd.data.element.complexType, function (type) {
                            var model = { name: type.name, properties: [] };

                            _.each(type.sequence.element, function (prop) {
                                model.properties.push({ name: prop.name, type: prop.type.split(':')[1] });
                            });

                            $scope.functions.push(model);
                        });
                    }
                    break;
            }
        });

        $scope.models = _.uniq($scope.models, function (item) {
            return item.name;
        })
        
        $scope.models = _.sortBy($scope.models, function (item) {
            return item.name;
        });

        $scope.functions = _.sortBy(_.uniq($scope.functions, function (item) {
            return item.name;
        }), function (item) {
            return item.name;
        });
    };

    $scope.save = function () {
        var blob = new Blob([$('#wsdl').text()], { type: "text/javascript;charset=utf-8" });
        saveAs(blob, $scope.fileName);
    }

    $scope.login = function () {
        var service = new DataLayerService();
        var securityData = new SecurityModel("panasonicpass", null, "panasonic");
        service.LogIn("regis.sabino@gmail.com", "123", securityData).done(function (data) {
            var logInModel = _.extend(new LogInModel(), data);
        });

        var pl = new SOAPClientParameters();

        pl.add("email", "regis.sabino@gmail.com");
        pl.add("password", "123");
        pl.add("securityData", {
            Password: "panasonicpass",
            Username: "panasonic",
            SessionId: null
        });

        SOAPClient.invoke("http://panasonic.restauranteweb.com.br/ServicoApps/DataLayerService.svc", "LogIn", pl, true, "IDataLayerService").done($scope.LoginResponse);
    };

    var LoginResponse = function (LogInResult) {
        debugger;
        $scope.$apply(function () {
            $scope.json = JSON.stringify(LogInResult, null, 4);
        });
    };

    $scope.generate = function () {
        if ($scope.xml) {
            $scope.wsdl = $.xml2json($scope.xml);
            $scope.json = JSON.stringify($scope.wsdl, null, 4);
        } else {
            $.get($scope.url.replace("?wsdl", "") + "?wsdl").done(function (response) {
                $scope.wsdl = $.xml2json(response);
                $scope.json = JSON.stringify($scope.wsdl, null, 4);

                $scope.fileName = $scope.wsdl.service.name + ".js";

                _.each($scope.wsdl.binding.operation, function (node) {
                    $scope.operations.push(node);
                });

                _.each($scope.wsdl.types.schema.import, function (node) {
                    $scope.imports.push({ namespace: node.namespace, url: node.schemaLocation });
                    $scope.requests.push($.get(node.schemaLocation));
                });

                $.when.apply(undefined, $scope.requests).then(function () {
                    _.each(arguments, function (argument, idx) {
                        $scope.$apply(function () {
                            var imp = $.xml2json(argument[0]);
                            _.each($scope.imports, function (item) {
                                if (item.url === argument[2].url) {
                                    item.data = imp;
                                    item.string = JSON.stringify(imp, null, 4);
                                }
                            });

                            $scope.extractImports();
                        });
                    });
                });
            });
        }
    }
};