﻿app.filter('prettyJson', function () {
    return function (model) {
        return JSON.stringify(model, null, 4);
    }
});
