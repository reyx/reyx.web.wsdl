﻿
var url = 'http://panasonic.restauranteweb.com.br/servicoapps/datalayerservice.svc?wsdl';

function formatXml(xml) {
    var formatted = '';
    var reg = /(>)(<)(\/*)/g;
    xml = xml.replace(reg, '$1\r\n$2$3');
    var pad = 0;

    $.each(xml.split('\r\n'), function (index, node) {
        var indent = 0;
        if (node.match(/.+<\/\w[^>]*>$/)) {
            indent = 0;
        }
        else if (node.match(/^<\/\w/)) {
            if (pad != 0) {
                pad -= 1;
            }
        }
        else if (node.match(/^<\w[^>]*[^\/]>.*$/)) {
            indent = 1;
        }
        else {
            indent = 0;
        }
        var padding = '';
        for (var i = 0; i < pad; i++) {
            padding += '  ';
        }
        formatted += padding + node + '\r\n';
        pad += indent;
    });

    return formatted;
}

$(function () {
    $.get(url).done(function () {

    });

    $('#test').click(function (e) {
        // stop the form to be submitted...
        e.preventDefault();
        var params = $('#params').val();
        if ($('#paramsType').val() == 'json') {
            params = eval("(" + params + ")");
        }
        var wss = {};
        if ($('#enableWSS').prop('checked')) {
            wss = {
                username: $('#wssUsername').val(),
                password: $('#wssPassword').val(),
                nonce: 'aepfhvaepifha3p4iruaq349fu34r9q',
                created: new Date().getTime()
            };
        }
        $.soap({
            url: $('#url').val(),
            method: $('#method').val(),

            appendMethodToURL: $('#appendMethodToURL').prop('checked'),
            SOAPAction: $('#SOAPAction').val(),
            soap12: $('#soap12').prop('checked'),

            params: params,
            wss: wss,

            namespaceQualifier: $('#namespaceQualifier').val(),
            namespaceURL: $('#namespaceURL').val(),
            noPrefix: $('#noPrefix').prop('checked'),
            elementName: $('#elementName').val(),

            enableLogging: $('#enableLogging').prop('checked'),

            request: function (SOAPRequest) {
                $('#request').text(formatXml(SOAPRequest.toString()) + '\n\n' + JSON.stringify($.xml2json(SOAPRequest.toString()), null, 4));
            },
            success: function (SOAPResponse) {
                $('#feedbackHeader').html('Success!');
                $('#feedback').text(JSON.stringify(SOAPResponse.toJSON(), null, 4));
            },
            error: function (SOAPResponse) {
                $('#feedbackHeader').html('Error!');
                $('#feedback').text(SOAPResponse.toString());
            }
        });
    });
});

// DEMO B
function GetData() {
    var pl = new SOAPClientParameters();
    pl.add("value", "550");
    SOAPClient.invoke(url, "GetData", pl, true, GetData_callBack);
}
function GetData_callBack(r) {
    alert(r);
}

// DEMO 0
function GetDataUsingDataContract() {
    var pl = new SOAPClientParameters();
    pl.add("composite", "0");
    SOAPClient.invoke(url, "GetDataUsingDataContract", pl, true, GetDataUsingDataContract_callBack);
}
function GetDataUsingDataContract_callBack(r) {
    alert(r);
}

// DEMO 1
function HelloWorld() {
    var pl = new SOAPClientParameters();
    SOAPClient.invoke(url, "HelloWorld", pl, true, HelloWorld_callBack);
}
function HelloWorld_callBack(r) {
    alert(r);
}

// DEMO 2
function HelloTo() {
    var pl = new SOAPClientParameters();
    pl.add("name", document.frmDemo.txtName.value);
    SOAPClient.invoke(url, "HelloTo", pl, true, HelloTo_callBack);
}
function HelloTo_callBack(r) {
    alert(r);
}

// DEMO 3
function ServerTime() {
    var pl = new SOAPClientParameters();
    SOAPClient.invoke(url, "ServerTime", pl, true, ServerTime_callBack);
}
function ServerTime_callBack(st) {
    var ct = new Date();
    alert("Server: " + st.toLocaleString() + "\r\n[Client: " + ct.toLocaleString() + "]");
}

// DEMO 4
function Wait() {
    var duration = parseInt(document.frmDemo.ddSleepDuration[document.frmDemo.ddSleepDuration.selectedIndex].value, 10);
    var pl = new SOAPClientParameters();
    pl.add("seconds", duration);
    var ph = document.getElementById("phWait");
    ph.style.display = "block";
    SOAPClient.invoke(url, "Wait", pl, true, Wait_callBack);
}
function Wait_callBack(r) {
    var img = document.getElementById("phWait");
    img.style.display = "none";
    alert("Call to \"Wait\" method completed");
}

// DEMO 5
function ThrowException() {
    try {
        var pl = new SOAPClientParameters();
        SOAPClient.invoke(url, "ThrowException", pl, false);
    }
    catch (e) {
        alert("An error has occured!");
    }
}

// DEMO 6
function SyncSample() {
    var pl = new SOAPClientParameters();
    pl.add("seconds", 5);
    var starttime = (new Date).toLocaleTimeString();
    var r = SOAPClient.invoke(url, "Wait", pl, false);
    alert("Operation start time: " + starttime + "\r\nOperation end time: " + (new Date).toLocaleTimeString());
}

// DEMO 7
function GetUser() {
    var username = document.frmDemo.txtUsername.value;
    var pl = new SOAPClientParameters();
    pl.add("username", username);
    SOAPClient.invoke(url, "GetUser", pl, true, GetUser_callBack);
}
function GetUser_callBack(u) {
    if (u == null)
        alert("No user found.\r\n\r\nEnter a username and repeat search.");
    else
        alert(
            "ID: " + u.Id + "\r\n" +
            "USERNAME: " + u.Username + "\r\n" +
            "PASSWORD: " + u.Password + "\r\n" +
            "EXPIRATION: " + u.ExpirationDate.toLocaleString());
}

// DEMO 8
function GetUsers() {
    var pl = new SOAPClientParameters();
    SOAPClient.invoke(url, "GetUsers", pl, true, GetUsers_callBack);
}
function GetUsers_callBack(ul) {
    alert(ul.length + " user(s) found:");
    for (var i = 0; i < ul.length; i++)
        alert(
            "User No. " + (i + 1) + "\r\n\r\n" +
            "ID: " + ul[i].Id + "\r\n" +
            "USERNAME: " + ul[i].Username + "\r\n" +
            "PASSWORD: " + ul[i].Password + "\r\n" +
            "EXPIRATION: " + ul[i].ExpirationDate.toLocaleString());
}

// DEMO 9
function GetUserList() {
    var pl = new SOAPClientParameters();
    SOAPClient.invoke(url, "GetUserList", pl, true, GetUserList_callBack);
}
function GetUserList_callBack(ul) {
    alert(ul.length + " user(s) found:");
    for (var i = 0; i < ul.length; i++)
        alert(
            "User No. " + (i + 1) + "\r\n\r\n" +
            "ID: " + ul[i].Id + "\r\n" +
            "USERNAME: " + ul[i].Username + "\r\n" +
            "PASSWORD: " + ul[i].Password + "\r\n" +
            "EXPIRATION: " + ul[i].ExpirationDate.toLocaleString());
}

// DEMO 10
function GetCars() {
    var cid = document.frmDemo.ddCompany[document.frmDemo.ddCompany.selectedIndex].value;
    if (cid != "") {
        // clear car list
        while (document.frmDemo.ddCar.options.length > 0)
            document.frmDemo.ddCar.remove(0);
        // add waiting element
        var o = document.createElement("OPTION");
        document.frmDemo.ddCar.options.add(o);
        o.value = "";
        o.innerHTML = "Loading...";
        // disable dropdown
        document.frmDemo.ddCar.disabled = true;
        // invoke
        var pl = new SOAPClientParameters();
        pl.add("companyid", cid);
        SOAPClient.invoke(url, "GetCars", pl, true, GetCars_callBack);
    }
}
function GetCars_callBack(cl) {
    // clear car list
    var c = document.frmDemo.ddCar.options.length;
    while (document.frmDemo.ddCar.options.length > 0)
        document.frmDemo.ddCar.remove(0);
    // add first (empty) element
    var o = document.createElement("OPTION");
    document.frmDemo.ddCar.options.add(o);
    o.value = "";
    o.innerHTML = "Please, select a model";
    // fill car list
    for (var i = 0; i < cl.length; i++) {
        var o = document.createElement("OPTION");
        document.frmDemo.ddCar.options.add(o);
        o.value = cl[i].Id.toString();
        o.innerHTML = cl[i].Label;
    }
    // enable dropdown
    document.frmDemo.ddCar.disabled = false;
}

// DEMO 11
function GetSoapResponse() {
    var pl = new SOAPClientParameters();
    SOAPClient.invoke(url, "HelloWorld", pl, true, GetSoapResponse_callBack);
}
function GetSoapResponse_callBack(r, soapResponse) {
    alert(soapResponse.xml);
}

// utils
function toggle(id) {
    var d = document.getElementById(id);
    if (d != null)
        d.className = (d.className == "h") ? "s" : "h";
}