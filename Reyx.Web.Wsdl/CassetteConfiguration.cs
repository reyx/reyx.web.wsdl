using Cassette;
using Cassette.Scripts;
using Cassette.Stylesheets;

namespace Reyx.Web.Wsdl
{
    /// <summary>
    /// Configures the Cassette asset bundles for the web application.
    /// </summary>
    public class CassetteBundleConfiguration : IConfiguration<BundleCollection>
    {
        public void Configure(BundleCollection bundles)
        {
            bundles.Add<StylesheetBundle>("~/content", new FileSearch() {
                Pattern = "*.css",
                SearchOption = System.IO.SearchOption.AllDirectories
            });

            bundles.Add<ScriptBundle>("~/scripts",
                "FileSaver.js",
                "Blob.js",
                "jquery-1.9.1.js",
                "bootstrap.js",
                "angular.js",
                "underscore.js",
                "soapclient.js",
                "jquery.xml2json.js",
                "DataLayerService.js",
                // "wsdl.js",
                "index.js",
                "app/app.js",
                "app/filters/pretty-json.js",
                "app/controllers/wsdl-ctrl.js",
                "home.js"
            );
        }
    }
}
