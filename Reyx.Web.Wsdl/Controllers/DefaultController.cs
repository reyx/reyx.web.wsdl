﻿using Reyx.Web.Wsdl.RestauranteWebService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Reyx.Web.Wsdl.Controllers
{
    public class DefaultController : Controller
    {
        private readonly DataLayerService service;
        private readonly SecurityModel securityModel;

        public DefaultController(DataLayerService service, SecurityModel securityModel)
        {
            this.service = service;
            this.securityModel = securityModel;
        }

        public DefaultController()
            : this(new DataLayerService(), new SecurityModel()
            {
                Password = "panasonicpass",
                Username = "panasonic"
            })
        {
        }

        public ActionResult Index()
        {
            // var cadastro = service.RegisterUser(new UserModel()
            // {
            //     Address = "Rua Curio",
            //     AddressCity = "Guarulhos",
            //     AddressNeighborhood = "Jd Valéria",
            //     AddressNumber = "220",
            //     AddressState = "SP",
            //     AddressZipCode = "07124660",
            //     BirthDate = new DateTime(1986, 2, 25),
            //     BirthDateSpecified = true,
            //     CPF = "32758221870",
            //     Email = "regis.sabino@gmail.com",
            //     Genre = GenreEnum.MALE,
            //     GenreSpecified = true,
            //     Name = "Regis Silva",
            //     Password = "123",
            //     Phone1DDD = "11",
            //     Phone1Number = "985459314"
            // }, securityModel);

            var model = service.LogIn("regis.sabino@gmail.com", "123", securityModel);
            securityModel.SessionId = model.SessionId;
            // var md = service.LogOut(securityModel);
            // var home = service.GetHomeData("01311000", securityModel);
            // var account = service.GetMyAccountData(securityModel);
            var restaurantes = service.GetSERPData(new HomeModelSearch() { CEP = "01311000" }, securityModel);

            ViewBag.Logado = model.LOGADO;

            return View();
        }

        public JsonResult LogIn(string email, string password)
        {
            //var model = service.LogIn("regis.sabino@gmail.com", "123", securityModel);
            var model = service.LogIn(email, password, securityModel);
            securityModel.SessionId = model.SessionId;

            var data = new
            {
                model.DADOSUSUARIO.COD,
                model.DADOSUSUARIO.NOME,
                model.DADOSUSUARIO.EMAIL,
                model.DADOSUSUARIO.CPF,
                model.SessionId
            };

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetUltimosPedidos()
        {
            var login = service.LogIn("regis.sabino@gmail.com", "123", securityModel);
            securityModel.SessionId = login.SessionId;

            var model = service.GetHomeData("01311000", securityModel);

            var data = model.PEDIDOSREFAZER.PEDIDOS
                .OrderBy(t => t.DATAPEDIDO)
                .ThenBy(t => t.HORAPEDIDO)
                .Select(t => new
                {
                    NROPEDIDO = t.NROPEDIDO,
                    NOMERESTAURANTE = t.NOMERESTAURANTE,
                    DATAPEDIDO = t.DATAPEDIDO,
                    HORAPEDIDO = t.HORAPEDIDO,
                    ITENS = t.ITENS.Take(6).ToArray(),
                    VLTOTALPEDIDO = t.VLTOTALPEDIDO,
                    TOTALITENS = t.ITENS.Length
                }).Take(2).ToArray();

            //var data = new { model.PEDIDOSREFAZER.PEDIDOS};

            return Json(data, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetEnderecosComidas(string cep = "")
        {
            var login = service.LogIn("regis.sabino@gmail.com", "123", securityModel);
            securityModel.SessionId = login.SessionId;

            var enderecos = service.LogIn("regis.sabino@gmail.com", "123", securityModel);

            if (string.IsNullOrWhiteSpace(cep)) cep = enderecos.DADOSUSUARIO.CEPFAVORITO;

            var tipocomidas = service.GetSERPData(new HomeModelSearch() { CEP = cep }, securityModel);

            var model = new
            {
                enderecos.DADOSUSUARIO.ENDERECO,
                tipocomidas.DADOSTIPOCOZINHA
            };

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public enum status
        {
            Todos = 0,
            Aberto = 1,
            ComPromocao = 2,
            ComEntregaGratis = 3
        }

        public JsonResult GetListaRestaurante(string cep, int[] cozinha, status status = status.Todos, string dia = "", string hora = "")
        {
            var login = service.LogIn("regis.sabino@gmail.com", "123", securityModel);
            securityModel.SessionId = login.SessionId;

            var restaurantes = service.GetSERPData(new HomeModelSearch() { CEP = cep }, securityModel);

            if (cozinha != null)
            {
                restaurantes.DADOSRESTAURANTES = restaurantes.DADOSRESTAURANTES.Where(t => t.TiposCozinhaList.Any(m => cozinha.Contains(m.Codigo))).ToArray();
            }

            if (!string.IsNullOrWhiteSpace(dia))
            {
                restaurantes.DADOSDATAHORACOMBO.DATAS = restaurantes.DADOSDATAHORACOMBO.DATAS.Where(t => t.DIA == dia).ToArray();
            }

            if (!string.IsNullOrWhiteSpace(dia))
            {
                restaurantes.DADOSDATAHORACOMBO.DATAS = restaurantes.DADOSDATAHORACOMBO.DATAS.Where(t => t.DIA == dia).ToArray();
            }

            if (status != status.Todos)
            {
                if (status == status.Aberto)
                    restaurantes.DADOSRESTAURANTES = restaurantes.DADOSRESTAURANTES.Where(t => t.EstaAberto == true).ToArray();
                if (status == status.ComPromocao)
                    restaurantes.DADOSRESTAURANTES = restaurantes.DADOSRESTAURANTES.Where(t => t.CADASTRO.TemPromocao == true).ToArray();
                if (status == status.ComEntregaGratis)
                    restaurantes.DADOSRESTAURANTES = restaurantes.DADOSRESTAURANTES.Where(t => t.TAXAENTREGA == "0.00").ToArray();
            }

            var data = new
            {
                restaurantes.DADOSDATAHORACOMBO.DATAS,
                restaurantes.DADOSDATAHORACOMBO.HORARIOS,
                DADOSRESTAURANTES = restaurantes.DADOSRESTAURANTES.Select(t => new
                {
                    t.CADASTRO.NOME,
                    t.CADASTRO.CODIGO,
                    t.CADASTRO.ENDERECO,
                    t.CADASTRO.ImagemFormatada,
                    t.CADASTRO.NumeroEstrelas,
                    t.CADASTRO.FILIAL,
                    t.CADASTRO.TemPromocao,
                    t.HORARIO
                }).ToArray()
            };

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRestaurante(short codRestaurante, string cep, short codFilial)
        {
            var login = service.LogIn("regis.sabino@gmail.com", "123", securityModel);
            securityModel.SessionId = login.SessionId;

            var model = service.GetRestaurantData(codRestaurante, true, codFilial, true, cep, securityModel);

            string urlBase = model.TIPOSPAGTO.LITERALPAGTOSSSL;
            urlBase = urlBase.Substring(urlBase.IndexOf("src=\""), urlBase.IndexOf("\" title") - urlBase.IndexOf("src=\""));
            urlBase = urlBase.Substring(0, urlBase.LastIndexOf("/") + 1).Substring(5);

            //model.GRUPOSCOMPLETOS = (from a in model.GRUPOSCOMPLETOS
            //                        select new RestaurantGrupo(){
            //                            CODIGO = a.CODIGO,
            //                            CODIGOSpecified = a.CODIGOSpecified,
            //                            DESCRICAO = a.DESCRICAO,
            //                            TEXTOCOMPLEMENTO = a.TEXTOCOMPLEMENTO,
            //                            PRATOS = null
            //                        }).ToArray();


            var GRUPOSCOMPLETOS = (from a in model.GRUPOSCOMPLETOS
                                   select new
                                   {
                                       CODIGO = a.CODIGO,
                                       CODIGOSpecified = a.CODIGOSpecified,
                                       DESCRICAO = a.DESCRICAO,
                                       TEXTOCOMPLEMENTO = "" // a.TEXTOCOMPLEMENTO,
                                   }).ToArray();

            model.TIPOSPAGTO.ICONE = (from q in model.TIPOSPAGTO.ICONE
                                      select string.Format("{0}{1}", urlBase, q)
                                    ).ToArray();

            var TIPOSPAGTO = new object[model.TIPOSPAGTO.CARTAO.Length];

            for (int i = 0; i < model.TIPOSPAGTO.CARTAO.Length; i++)
            {
                TIPOSPAGTO[i] = new {
                    CARTAO = model.TIPOSPAGTO.CARTAO[i],
                    CODIGO = model.TIPOSPAGTO.CODIGOS[i],
                    DESCRICAO = model.TIPOSPAGTO.DESCRICAO[i],
                    ICONE = model.TIPOSPAGTO.ICONE[i]
                };
            }

            var data = new
            {
                model.CODFILIAL,
                model.CODRESTAURANTE,
                model.ImagemFormatada,
                model.TEXTOLIVRE,
                model.TEXTOPROMO,
                model.NOMERESTAURANTE,
                model.HORARIO,
                model.TIPOSCOZINHA,
                model.ENDERECORESTAURANTE,
                model.ESTRELASAVALIACAO,
                model.VALORMINIMOPEDIDO,
                TIPOSPAGTO = TIPOSPAGTO,
                GRUPOSCOMPLETOS = GRUPOSCOMPLETOS,
                SUGESTAO = new
                {
                    model.SUGESTAO.FOTOPRATO,
                    model.SUGESTAO.CODIGOGRUPO,
                    model.SUGESTAO.CODIGOPRATO,
                    model.SUGESTAO.INDICAPIZZAMEIO,
                    model.SUGESTAO.PRATO.DESCRICAOPRATO,
                    model.SUGESTAO.PRATO.DESCRDETPRATO,
                    model.SUGESTAO.PRATO.VALORPRATO
                }
            };


            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetGrupoPratos(string cep, short codFilial, short codRestaurante, int codGrupo)
        {
            var login = service.LogIn("regis.sabino@gmail.com", "123", securityModel);
            securityModel.SessionId = login.SessionId;

            service.LogOut(securityModel);

            login = service.LogIn("regis.sabino@gmail.com", "123", securityModel);
            securityModel.SessionId = login.SessionId;


            var model = service.GetRestaurantData(codRestaurante, true, codFilial, true, cep, securityModel);

            var query = (from e in model.GRUPOSCOMPLETOS
                         where e.CODIGO == codGrupo
                         select e.PRATOS
                ).First();

            var data = (from a in query
                        select new
                        {
                            DESCRICAOPRATO = a.DESCRICAOPRATO,
                            DESCRDETPRATO = a.DESCRDETPRATO,
                            CODIGOPRATO = a.CODIGOPRATO,
                            ImagemFormatada = a.ImagemFormatada
                        });

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPrato(string cep, short codFilial, short codRestaurante, short codGrupo, string codPrato)
        {
            var login = service.LogIn("regis.sabino@gmail.com", "123", securityModel);
            securityModel.SessionId = login.SessionId;

            service.LogOut(securityModel);

            login = service.LogIn("regis.sabino@gmail.com", "123", securityModel);
            securityModel.SessionId = login.SessionId;

            MenuDetailSearchModel objMenu = new MenuDetailSearchModel();
            objMenu.CodigoPrato = Convert.ToInt32(codPrato);

            objMenu.DadosCardapio = new RestaurantMenuSearchModel();

            objMenu.DadosCardapio.CEP = cep;
            objMenu.DadosCardapio.CodigoFilial = codFilial;
            objMenu.DadosCardapio.CodigoGrupo = codGrupo;
            objMenu.DadosCardapio.CodigoRestaurante = codRestaurante;
            
            var model = service.GetRestaurantData(codRestaurante, true, codFilial, true, cep, securityModel);

            var query = model.GRUPOSCOMPLETOS.Where(t => t.CODIGO == codGrupo).Select(t => t.PRATOS).First();

            var data = query.Select(t => new
            {
                t.CODIGOPRATO,
                t.ImagemFormatada,
                t.DESCRICAOPRATO,
                t.DESCRDETPRATO,
                t.VALORPRATO,
                OPCOES = t.OPCOES.Select(o => new
                {
                    o.TITULO,
                    o.MAXIMOITENS,
                    o.MINIMOITENS,
                    ITENS = o.ITENS.Select(i => new
                    {
                        i.Selecionado,
                        i.TIPOVARIAVEL,
                        i.TITULOVARIAVEL,
                        i.VALORVARIAVEL
                    })
                })
            }).FirstOrDefault(t => t.CODIGOPRATO == codPrato);

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPedido(int codPedido)
        {
            var model = service.GetOrderConfirmationData(codPedido, true, securityModel);

            var data = new
            {
                model.NOMECLIENTE,
                model.NUMEROPEDIDO,
                model.STATUS,
                model.DATAPEDIDO,
                model.HORAPEDIDO,
                model.TEMPOESTIMADO,
                model.NOMERESTAURANTE,
                model.TextoStatus
            };


            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDetalhePedido(int codPedido)
        {
            //var model = service.GetOrderConfirmationData(codPedido, true, securityModel);
            var model = service.GetHomeData("01311000", securityModel).PEDIDOSREFAZER.PEDIDOS.Where(m => m.NROPEDIDO == codPedido).SingleOrDefault();

            var data = new
            {
                CODPEDIDOD = model.NROPEDIDO,
                TOTAL = model.VLTOTALPEDIDO,
                SUBTOTAL = model.VLSUBTOTAL,
                TAXAENTREGA = model.TAXAENTREGA,
                ITENS = model.ITENS
                //ITENS = model.ITENS.Select(t => new {
                //        t.NOMEPRATO,
                //        t.QTDEPRATO,
                //        t.VALORUNITARIO,
                //        t.VALORPRATO,
                //        t.TEXTOOPCOES
                //}).ToArray()
            };

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTermosUso()
        {
            var data = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam pulvinar consequat scelerisque. Vestibulum sit amet nibh sit amet eros facilisis tristique vitae sit amet arcu. Nam luctus, sem et sollicitudin molestie, elit arcu posuere felis, eu ullamcorper arcu massa non lectus. Phasellus luctus diam quam. Ut commodo arcu sit amet euismod tempus. Nam iaculis, mi at bibendum porta, augue tellus eleifend nisl, ut porta sapien est ac lorem. Aliquam in quam eu lorem bibendum suscipit aliquet sed massa. Praesent in orci vitae neque auctor adipiscing ut id sapien. Donec vulputate, nibh ut convallis lobortis, metus mi ultricies nibh, ac tincidunt risus purus ac arcu. Duis id molestie erat, congue tincidunt enim. Donec lacus nisl, dapibus et scelerisque ut, accumsan ut augue. Donec nec mi ut dui aliquam tincidunt. Nunc ultrices euismod diam, eget pretium dui aliquam a. Nullam quis vestibulum velit, at adipiscing quam. Nam sagittis sem at condimentum eleifend. Suspendisse tempor elit in dolor iaculis interdum.\n\n"
                        + "Donec accumsan mi at enim blandit pretium. Maecenas non orci in neque dapibus elementum. Ut at metus lacus. Morbi nec dui nec nisl volutpat tincidunt eget iaculis dolor. Vestibulum commodo tempus urna. Suspendisse potenti. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Praesent eros nisi, hendrerit in imperdiet in, euismod vel est.\n\n"
                        + "Sed gravida, velit ac posuere mattis, urna quam bibendum quam, ac fringilla justo quam ornare dolor. Donec suscipit scelerisque turpis, ac sagittis nunc. Aliquam nec rutrum tellus, ac consequat quam. Ut vehicula, dui sit amet pellentesque tempus, lectus ligula pulvinar lectus, et tincidunt purus orci et metus. Proin in est vitae purus pharetra tristique et nec sapien. Nullam tortor diam, lacinia vitae elit in, varius eleifend nisi. Morbi tempor ornare ligula, ut commodo est venenatis vitae. Cras malesuada felis eget ornare porttitor. Integer ultricies odio vitae velit blandit, at imperdiet ligula scelerisque. Maecenas at velit pulvinar purus accumsan fringilla. Vivamus sollicitudin id mauris a volutpat. Donec mattis mollis enim et luctus. Fusce ac nulla velit. Suspendisse rhoncus sapien nulla, in aliquet mauris convallis non. Nam ut odio pellentesque, sollicitudin nisl blandit, lobortis mi.\n\n"
                        + "Donec tincidunt erat ultrices turpis bibendum, eu iaculis lorem adipiscing. Aenean tristique sapien et dolor commodo, aliquet semper lorem porta. Maecenas a consectetur est. Integer non mauris sem. Sed at mattis neque, suscipit eleifend velit. Integer dolor neque, posuere in egestas eu, viverra eget tortor. Curabitur lobortis vestibulum consectetur. In lobortis gravida nunc vitae porta. Quisque congue, risus sit amet lobortis interdum, est orci fringilla urna, a dictum diam nisl vel lorem. Vivamus accumsan justo vel euismod fermentum. Aliquam in massa elementum, luctus justo non, aliquam sapien. Nunc ullamcorper purus id purus varius, in rhoncus dolor tempor.\n\n"
                        + "Praesent non bibendum nulla, venenatis volutpat leo. Ut in dui justo. Donec in metus pulvinar, eleifend nulla nec, molestie urna. Integer eu turpis semper, ullamcorper enim ac, porttitor sem. Proin a augue bibendum nulla convallis scelerisque et nec nibh. Etiam rhoncus sodales dolor, eget fermentum nisi aliquam ut. Cras sodales, nibh et adipiscing molestie, neque est ornare metus, vel laoreet nisl elit ut mi.\n\n";

            return Json(new { Termos = data }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPoliticaPrivacidade()
        {
            var data = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi erat nunc, tempor sit amet mi ut, fringilla facilisis ligula. Nullam luctus orci ac lectus ultrices ullamcorper. Pellentesque et erat vitae lorem venenatis interdum. Maecenas lacinia posuere nulla, non dapibus nulla varius eu. Proin in lacus pharetra, vestibulum dolor id, condimentum quam. Aenean luctus massa eu metus lobortis vulputate. Quisque eu pretium lacus. Nunc sed vulputate est, ut fermentum felis. Sed sed nisi non massa viverra molestie non at odio. Ut dapibus est nec enim interdum, vel interdum enim blandit. Nam sodales molestie arcu, a dignissim lacus volutpat quis.\n\n"
                        + "Curabitur varius urna sit amet luctus porttitor. Ut elit elit, lacinia non neque vel, adipiscing porta mi. Pellentesque sem risus, scelerisque tincidunt accumsan vitae, cursus vel sapien. Morbi libero leo, dignissim sit amet risus non, iaculis accumsan lectus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sollicitudin dapibus ante, mollis sagittis sem dignissim sit amet. Duis in egestas felis. Vestibulum dictum enim mollis risus iaculis, vel ornare ligula tristique. Duis nec dapibus neque. Vivamus felis mi, venenatis et tortor ut, vestibulum tempor mi. Nulla scelerisque nunc diam, sed aliquam purus pharetra et. Cras vitae ornare diam. Nam volutpat, nulla vitae accumsan cursus, felis turpis pharetra mauris, ac venenatis mauris ipsum eget sem. Nunc orci velit, hendrerit bibendum justo sit amet, tincidunt convallis dui. Ut suscipit laoreet odio ut adipiscing.\n\n"
                        + "Nulla facilisi. Donec eu metus sed lorem dapibus tempor. Nam eleifend odio quis enim imperdiet, id luctus lectus ultrices. Aliquam sit amet porta urna, a viverra sapien. Ut sit amet dui imperdiet, interdum felis at, blandit orci. Proin quis vulputate nunc. Morbi sed aliquam ligula, in consectetur odio. Pellentesque non blandit urna. Nulla pellentesque gravida augue vitae mattis. Integer gravida justo ac erat tempor ornare. Phasellus auctor felis nec felis bibendum, vel consectetur dui blandit. Aliquam pretium dignissim nisl, nec tincidunt enim accumsan eu.\n\n"
                        + "Ut quis congue velit. Maecenas et turpis ac purus euismod aliquam. Aenean massa risus, rutrum pretium eros at, fermentum gravida turpis. Integer hendrerit augue et eros cursus, a malesuada quam commodo. Donec dui odio, mattis accumsan leo ac, convallis ultrices enim. Aliquam dignissim volutpat ipsum, ac ullamcorper ipsum elementum sollicitudin. Aliquam condimentum dolor vitae neque posuere blandit. Cras dictum arcu eu nibh auctor, vel tempus augue vehicula. In in eros non ligula lacinia porttitor quis eget lacus. Sed et laoreet ipsum. Vivamus blandit velit vel purus euismod pharetra. Mauris consequat diam dolor. Duis cursus convallis tortor, eget ultrices ante tempor quis. Morbi sit amet augue interdum, imperdiet ipsum a, aliquam eros. Lorem ipsum dolor sit amet, consectetur adipiscing elit.\n\n"
                        + "Aliquam interdum arcu ut magna ultrices facilisis. Quisque fringilla egestas nibh, at lacinia purus sollicitudin imperdiet. Nullam ornare vulputate ligula ac pharetra. Aliquam erat volutpat. Donec leo eros, pulvinar ut venenatis at, mollis non quam. Maecenas ac quam in lacus sollicitudin laoreet. Vivamus sagittis eros aliquet ante pharetra volutpat. Praesent ac consectetur velit. Proin sed leo quis augue dictum molestie. Etiam nec est sapien. Nulla iaculis, quam sit amet condimentum viverra, magna mauris mollis mauris, pulvinar sagittis diam ipsum eu odio. Pellentesque dapibus erat elit, quis fringilla nisl lacinia vitae. Aliquam ultricies enim nisi, et tempus ligula pulvinar sed.\n\n"
                        + "Suspendisse non luctus augue. Phasellus tortor massa, congue vitae euismod et, sagittis eu velit. Donec turpis nisi, tristique id lacinia eget, facilisis nec mi. Pellentesque facilisis porttitor nunc nec dapibus. Quisque facilisis egestas dui, et placerat odio vehicula a. In dui justo, blandit vel tempor eu, semper sed nulla. Aenean venenatis quis enim sed imperdiet. Nam a dui a nunc euismod euismod. Nullam porttitor dui et metus vehicula scelerisque. Donec vitae elementum odio, ac ullamcorper nulla. Pellentesque auctor malesuada nulla, eget mollis tellus dapibus vel.\n\n"
                        + "Donec id turpis nec erat convallis rhoncus non quis eros. Mauris pulvinar orci mattis laoreet consequat. Curabitur tristique tempor interdum. Suspendisse id viverra turpis, et pulvinar orci. Donec consectetur lectus tellus, in rutrum justo adipiscing at. Donec non placerat ipsum. Sed vehicula mi ut leo tristique, sit amet imperdiet libero tempus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Quisque malesuada odio non eros iaculis, non sagittis nulla volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla molestie quam a aliquet bibendum. Suspendisse potenti. Mauris non diam et eros faucibus tristique non nec neque. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras in posuere libero.\n\n";

            return Json(new { Politica = data }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult teste()
        {
            var login = service.LogIn("regis.sabino@gmail.com", "123", securityModel);
            securityModel.SessionId = login.SessionId;

            service.LogOut(securityModel);

            login = service.LogIn("regis.sabino@gmail.com", "123", securityModel);
            securityModel.SessionId = login.SessionId;

            //var model = service.GetHomeData("01311000", securityModel);

            // var model = service.GetSERPData(new HomeModelSearch() { CEP = "01311000", }, securityModel);

            var model = service.GetRestaurantData(1598, true, 1, true, "01311000", securityModel);

            RestaurantMenuSearchModel obj = new RestaurantMenuSearchModel();
            obj.CEP = "01311000";
            obj.CodigoFilial = 1;
            obj.CodigoRestaurante = 1598;
            obj.CodigoGrupo = 1;
            obj.CodigoFilialSpecified = true;
            obj.CodigoGrupoSpecified = true;
            obj.CodigoRestauranteSpecified = true;


            //var model = service.GetRestaurantMenuData(obj, securityModel);

            //OrderEndData order = new OrderEndData();
            //var model = service.GetOrderConfirmationData(0,true,securityModel);

            AdicionarPratoModelFormData prato = new AdicionarPratoModelFormData();            
            prato.CodigoPizzaMetade1 = "Pizza Calabresa";
            prato.Quantidade = 1;
            prato.QuantidadeSpecified = true;

            AddMenuItemModel objMenu = new AddMenuItemModel();
            // objMenu.DadosForm = new AdicionarPratoModelFormData();
            objMenu.DadosForm = new AdicionarPratoModelFormData();
            //objMenu.DadosForm.CodigoPizzaMetade1 = "Pizza Calabresa";
            //objMenu.DadosForm.CodigoPizzaMetade2 = "Pizza Calabresa";
            //objMenu.DadosForm.Observacao = "teste";
            //objMenu.DadosForm.Quantidade = 1;
            //objMenu.DadosForm.QuantidadeSpecified = true;

            //AdicionarPratoModelFormDataSelectValue teste = new AdicionarPratoModelFormDataSelectValue();
            //teste.NomeInput = "";
            //teste.ValorInput = "";
            //objMenu.DadosForm.SelectedValues[0] = teste;

            objMenu.DadosUrl = new MenuDetailSearchModel();
            objMenu.DadosUrl.CodigoPrato = 2;
            objMenu.DadosUrl.CodigoPratoSpecified = true;
            objMenu.DadosUrl.DadosCardapio = new RestaurantMenuSearchModel();
            objMenu.DadosUrl.DadosCardapio = obj;

            //var model = service.AddMenuItem(objMenu, securityModel);

            var teste = service.GetOrderEndData(obj, securityModel);
            //var model = service.GetUserBasicAndOrder(securityModel);

            //var model = service.GetUserBasicAndOrder(0, true, securityModel);


            //var model = service.SendOrder(obj,data,"",securityModel);

            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}

